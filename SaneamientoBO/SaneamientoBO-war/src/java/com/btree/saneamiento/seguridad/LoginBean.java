/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;

import com.btree.saneamiento.bd.Menu;
import com.btree.saneamiento.bd.Permisos;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.MenuFacade;
import com.btree.saneamiento.ln.PermisosFacade;
import com.btree.saneamiento.ln.ProgramaFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

@Named(value = "login")
@SessionScoped
public class LoginBean implements Serializable {

    public static Logger log = Logger.getLogger(LoginBean.class);
    private String user;
    private String password;

  

    @EJB
    UsuarioFacade usuarioService;

    private Usuario usuario = null;
  

    @EJB
    PermisosFacade permisoService;
    @EJB
    ProgramaFacade programaService;
     @EJB
    private MenuFacade menuService;

    private HashMap<Menu, List<Programa>> menu;
    private Set<Menu> menuKeySet;

    public String login() {

        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        try {
            log.info("Validando usuario: " + user + " ip: " + remoteAddr);
            usuario = usuarioService.loginUser(user, password);

            //if (usuarioService.validateUser(user, password)) {
            log.info("Usuario valido: " + user);
            // if (/*usuarioSvc.isHabilitado(usuario)*/true) {
            password = "";
            //si es usuario conductor verificar si tiene un estado anterior a la membresia,
            //de ese modo dirigirlo a la interfaz de enrolamiento de conductor para que complete sus datos
            log.info("Iniciando armado de menu");
            List<Menu> menuList = getMenuList();
            menu = new HashMap<>();
            for (Menu menuItem : menuList) {
                List<Programa> items = getItems(menuItem);
               if (items != null && !items.isEmpty()) {
                        menu.put(menuItem, items);
                    }
            }
//            List list = new ArrayList(menu.keySet());
//           // Collections.sort(list, Collections.reverseOrder());
//            menuKeySet = new LinkedHashSet(list);
            log.info("Finalizando armado de menu");
            user=usuario.getUsuario();
            return "adm/dashboard.xhtml?faces-redirect=true";
            /*  } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario inhabilitado", "Usuario inhabilitado"));
            }
            } else {
                nroIntentosFallidos++;
                System.out.println("Intento fallido nro: " + nroIntentosFallidos + " usuario:" + user + " ip:" + remoteAddr);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario y/o Password Incorrecto", "Usuario y/o Password Incorrecto"));
            }
            if (nroIntentosFallidos == 3) {
                //usuarioSvc.inhabilitarUsuario(usuario);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario bloqueado por demasiados intentos fallidos", "Usuario bloqueado por demasiados intentos fallidos"));
                nroIntentosFallidos = 0;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
            //return "error.xhtml?faces-redirect=true";
        }
        return null;
    }

    public String registrar() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        session.invalidate();
        return "conductores.xhtml?faces-redirect=true";
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the usuario to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    

    private List<Menu> getMenuList() {
        return menuService.findAll();
    }

    private List<Programa> getItems(Menu menu) {
        try {
            List<Permisos> permisos = permisoService.listarPermisosMenu(getUsuario(), menu);

            List<Programa> programas = new ArrayList<>();
            for (Permisos permiso : permisos) {               
              //  Programa programa = programaService.find(permiso.getPrograma().getProgramaid());
                if (permiso.getPrograma().getVisible()) {
                    programas.add(permiso.getPrograma());
                }
            }
            return programas;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public HashMap<Menu, List<Programa>> getMenu() {
        return menu;
    }

    public void setMenu(HashMap<Menu, List<Programa>> menu) {
        this.menu = menu;
    }

    public Set<Menu> getMenuKeySet() {
        return menuKeySet;
    }

}
