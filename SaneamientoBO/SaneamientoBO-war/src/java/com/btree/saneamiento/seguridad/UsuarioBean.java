/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;


import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.PerfilFacade;
import com.btree.saneamiento.ln.PerfilprogramaFacade;
import com.btree.saneamiento.ln.PermisosFacade;
import com.btree.saneamiento.ln.ProgramaFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "usuarioBean")
@ViewScoped
public class UsuarioBean implements Serializable {

    public static Logger log = Logger.getLogger(UsuarioBean.class);
    private boolean dialogVisibleNuevoEditar;
    private boolean dialogVisiblePassword;
    private boolean editActive;
    private Usuario usuarioSelected;
    private Integer codigoEpsa;
    private boolean monitor;
    private List<Usuario> usuarios;
    private List<Programa> programas;

    private List<Perfil> perfiles;

    @EJB
    ProgramaFacade programaService;
    @EJB
    PerfilFacade perfilService;
    @EJB
    UsuarioFacade usuarioService;
    @EJB
    PerfilprogramaFacade perfilProgramaService;
    @EJB
    PermisosFacade permisoService;
     @EJB
    MonitorFacade monitorService;
    @EJB
    private Util util;
    
    @Inject
    private LoginBean login;

    @PostConstruct
    public void init() {
        dialogVisibleNuevoEditar = false;
        dialogVisiblePassword = false;
        usuarios = usuarioService.findAll();
        usuarioSelected = new Usuario();
       
        

        programas = new ArrayList<>();

        perfiles = perfilService.findAll();
        editActive = false;
    }

    

    public void save() {
        try {
            if (editActive) {
               usuarioSelected.setLdap(usuarioSelected.getLdap().substring(0,1));
                usuarioService.editar(usuarioSelected);
                util.showInfo("Usuario modificado satisfactoriamente");
            } else {
                usuarioSelected.setEstado(Util.HABILITADO);
                usuarioSelected.setLdap(monitor?"t":"f");
                usuarioService.crear(usuarioSelected);
                
                util.showInfo("Usuario creado satisfactoriamente");
            }
            editActive = false;
            dialogVisibleNuevoEditar = false;
            perfiles = perfilService.findAll();
            usuarios = usuarioService.findAll();
            usuarioSelected = new Usuario();
           
        } catch (Exception e) {
            log.error(e);
        }
    }

    public void create() {
        dialogVisibleNuevoEditar = true;
        usuarios = usuarioService.findAll();
        usuarioSelected = new Usuario();
        
        editActive = false;
        

        programas = new ArrayList<>();

        perfiles = perfilService.findAll();
    }

    public void edit(Usuario usuario) {

        try {
            dialogVisibleNuevoEditar = true;
            editActive = true;
            usuarioSelected = usuario;
            listPrograms();
        } catch (Exception e) {
            log.error(e);
        }
    }
    
    public String getPerfil(Short perfil){
        try{
            return   perfilService.find(perfil).getDescripcion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void cambiarPassword() {

        try {
            log.info(usuarioSelected.getContrasenia());
            usuarioService.editPassword(usuarioSelected);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro realizado exitosamente", ""));
            RequestContext.getCurrentInstance().execute("PF('changePass').hide()");
        } catch (Exception e) {
            log.error(e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), ""));

        }
    }

    public void delete(Usuario usuario) {
        if (Objects.equals(usuario.getUsuarioid(), Util.USUARIO_SYS)) {
            util.showInfo("El usuario principal del sistema no puede ser eliminado");
            return;
        }

        try {
            if(monitorService.existeUsuario(usuario)){
                util.showInfo(Util.MSG_DELETE_FAIL);
                return;
            }
            
            usuarioService.remove(usuario);
            log.error(": Se procede al eliminado fisico");
            util.showInfo("Eliminado satisfactoriamente");
            usuarios = usuarioService.findAll();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
           
            util.showError(Util.MSG_CRASH);
                
            
        }

    }

    public void listPrograms() {
        programas=new ArrayList<>();
        if (usuarioSelected.getPerfilid()!=null){
            log.info("*********************"+usuarioSelected.getPerfilid()+"**********************");
            programas = programaService.programaInPerfilById(usuarioSelected.getPerfilid());
        }
    }
    
    //Methods
    
    public boolean isDialogVisiblePassword() {
        return dialogVisiblePassword;
    }

    public void setDialogVisiblePassword(boolean dialogVisiblePassword) {
        this.dialogVisiblePassword = dialogVisiblePassword;
    }

    public boolean isDialogVisibleNuevoEditar() {
        return dialogVisibleNuevoEditar;
    }

    public void setDialogVisibleNuevoEditar(boolean dialogVisibleNuevoEditar) {
        this.dialogVisibleNuevoEditar = dialogVisibleNuevoEditar;
    }

    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }

    public Usuario getUsuarioSelected() {
        return usuarioSelected;
    }

    public void setUsuarioSelected(Usuario usuarioSelected) {
        log.info(usuarioSelected);
        this.usuarioSelected = usuarioSelected;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Programa> getProgramas() {
        
        return programas;
    }

    public void setProgramas(List<Programa> programas) {
        this.programas = programas;
    }

    public List<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public Integer getCodigoEpsa() {
        return codigoEpsa;
    }

    public void setCodigoEpsa(Integer codigoEpsa) {
        this.codigoEpsa = codigoEpsa;
    }

    public boolean isMonitor() {
        return monitor;
    }

    public void setMonitor(boolean monitor) {
        this.monitor = monitor;
    }
    
    
}
