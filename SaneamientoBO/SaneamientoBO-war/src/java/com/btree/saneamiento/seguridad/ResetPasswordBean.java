/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;


import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 *
 * @author Daniel Yugar
 */
@Named(value = "resetPasswordBean")
@ViewScoped
public class ResetPasswordBean implements Serializable {

    public static Logger log = Logger.getLogger(ResetPasswordBean.class);

    private boolean passwordResetSuccessful = false;

    private Usuario user;

    @EJB
    private UsuarioFacade usuarioService;
    @EJB
    private Util util;
    
    @Inject
    LoginBean login;

    public String resetPassword() {
        if (user != null) {
            try {
//                usuarioService.resetPassword(user);
//                passwordResetSuccessful = true;
//                util.showInfo("Cambio exitoso.");
//
//                login.setUser(user.getUsuariologin());
//                login.setPassword(user.getUsuariopassword());
                return login.login();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                util.showError("Could not find user.");
                passwordResetSuccessful = false;
            }
        } else {
            passwordResetSuccessful = false;
        }
        return "";
    }

    public void processActionToken() {
        if (!passwordResetSuccessful) {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            String actionToken = request.getParameter("uid");
            log.info(actionToken);
            try {
//                user = usuarioService.getUserByActionToken(actionToken);
//                log.info(user.getUsuariologin());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                util.showError("Action token not valid.");
                user = null;
            }
        }
    }

    public boolean isPasswordResetSuccessful() {
        return passwordResetSuccessful;
    }

    public void setPasswordResetSuccessful(boolean passwordResetSuccessful) {
        this.passwordResetSuccessful = passwordResetSuccessful;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

}
