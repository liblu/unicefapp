/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;


import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.ln.PerfilFacade;
import com.btree.saneamiento.ln.PerfilprogramaFacade;
import com.btree.saneamiento.ln.ProgramaFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Diana Mejía
 */
@Named(value = "perfilBean")
@ViewScoped
public class PerfilBean implements Serializable {

    public static Logger log = Logger.getLogger(PerfilBean.class);
    private boolean dialogVisibleNuevoEditar;
    private boolean editActive;
    private Perfil perfilSelected;
    private List<Perfil> perfiles;

    private DualListModel<Programa> programas;

    @EJB
    PerfilFacade perfilService;
    @EJB
    ProgramaFacade programaService;
    @EJB
    PerfilprogramaFacade perfilProgramaService;
    @EJB
    private UsuarioFacade usuarioService;

    @Inject
    private LoginBean login;
    @EJB
    private Util util;

    @PostConstruct
    public void init() {
        dialogVisibleNuevoEditar = false;
        perfiles = perfilService.findAll();
        perfilSelected = new Perfil();

        List<Programa> programasSource = null;
        try {
            programasSource = programaService.listarProgramas();
        } catch (Exception ex) {
            log.error(ex);
        }
        List<Programa> programasTarget = new ArrayList<>();

        programas = new DualListModel<>(programasSource, programasTarget);
    }

    public boolean isDialogVisibleNuevoEditar() {
        return dialogVisibleNuevoEditar;
    }

    public void setDialogVisibleNuevoEditar(boolean dialogVisibleNuevoEditar) {
        this.dialogVisibleNuevoEditar = dialogVisibleNuevoEditar;
    }

    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }

    public Perfil getPerfilSelected() {
        return perfilSelected;
    }

    public void setPerfilSelected(Perfil perfilSelected) {
        this.perfilSelected = perfilSelected;
    }

    public List<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public void save() {
        try {
            if (editActive) {
                perfilService.edit(perfilSelected, programas.getTarget());
            } else {
                perfilService.create(perfilSelected, programas.getTarget());
            }
            editActive = false;
            dialogVisibleNuevoEditar = false;
            util.showInfo("Registro realizado");
            perfiles = perfilService.findAll();
            perfilSelected = new Perfil();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
//
    public void edit(Perfil perfil) {
       
        editActive = true;
        perfilSelected = perfil;
        List<Programa> target = programaService.programaInPerfil(perfil); 
        List<Programa> programasSource =  programaService.programaNotInPerfil(perfil);      
        programas = new DualListModel<>(programasSource, target);

        dialogVisibleNuevoEditar = true;
    }

    public void delete(Perfil perfil) {
        try {
            if (!usuarioService.existePerfil(perfil)) {
                log.info("**********************del if");
                perfilService.remove(perfil);
                util.showInfo(Util.MSG_DELETE);
                perfiles = perfilService.findAll();
            } else {
               
                util.showInfo("perfil en uso");
                
            }

        } catch (Exception ex) {
            try {
                log.info("**********************eliminado logico");
                log.error(ex.getMessage() + ": Se procede al eliminado logico", ex);
                perfil.setEstado(Util.INHABILITADO);
                perfilService.edit(perfil);
                 util.showInfo("Eliminado lógico satisfactorio");
                perfiles = perfilService.findAll();
            } catch (Exception e) {
                log.error(e.getMessage(), e);

                util.showError("No se puede eliminar debido a que es utilizado por otros registros");
            }
        }
    }

    public void create() {
        perfilSelected = new Perfil();

        editActive = false;
        List<Programa> programasSource = null;
        try {
            programasSource = programaService.listarProgramas();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        List<Programa> programasTarget = new ArrayList<>();
        programas = new DualListModel<>(programasSource, programasTarget);
        dialogVisibleNuevoEditar = true;
       // RequestContext.getCurrentInstance().execute("PF('dlgNuevoEditarId').show()");
    }

    public DualListModel<Programa> getProgramas() {
        return programas;
    }

    public void setProgramas(DualListModel<Programa> programas) {
        this.programas = programas;
    }
}
