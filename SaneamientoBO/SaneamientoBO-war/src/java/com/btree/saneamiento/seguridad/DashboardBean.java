/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;


import com.btree.saneamiento.bd.Menu;
import com.btree.saneamiento.bd.Programa;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "dashboardBean")
@ViewScoped
public class DashboardBean implements Serializable {

    public static Logger log = Logger.getLogger(DashboardBean.class);
    
    @Inject
    LoginBean login;
    
    
    public Set<Menu> getMenu(){
        return login.getMenu().keySet();
    }
    
    public List<Programa> getProgramas(Menu menu){
        return login.getMenu().get(menu);
    }

    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        session.invalidate();
        return "/login.xhtml?faces-redirect=true";
    }
}
