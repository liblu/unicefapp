/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.seguridad;


import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.util.AppException;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 *
 * @author Daniel Yugar
 */
@Named(value = "forgotPasswordBean")
@ViewScoped
public class ForgotPasswordBean implements Serializable {

    public static Logger log = Logger.getLogger(ForgotPasswordBean.class);

    private String userIdentifier;

    @EJB
    private UsuarioFacade usuarioService;
    @EJB
    private Util util;


    public void resetPassword() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        String serverString = request.getServerName() + ":" + request.getServerPort();
        try {
//            Usuario user = usuarioService.findUserByUsernameOrEmail(userIdentifier);
//            if (user != null) {
//                usuarioService.initiateResetPassword(user);
//                usuarioService.sendPasswordResetMail(user, serverString);
//                util.showInfo("Se le enviará un mail con la información para recuperar su contraseña a la dirección email que proporcionó al crear su cuenta.");
//            } else {
//                throw new Exception("No se encontro el usuario");
//            }
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            util.showError("No se encontro un usuario con el mismo nombre de usuario o dirección email.");
        }
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

}
