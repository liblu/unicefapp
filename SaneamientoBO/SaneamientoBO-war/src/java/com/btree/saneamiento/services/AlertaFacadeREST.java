/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services;


import com.btree.saneamiento.services.request.AlertaRequest;
import com.btree.saneamiento.services.response.Response;
import com.btree.saneamiento.services.response.ResponseStatus;
import com.btree.saneamiento.util.Util;
import java.text.ParseException;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniel Yugar
 */
@Stateless
@Path("alerta")
public class AlertaFacadeREST {

    @Context
    private UriInfo context;

    
    @EJB
    private Util util;

    @POST
    @Path("save")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response save(AlertaRequest alertaRequest) {
        Response response = new Response();
        Date fechaHora;
        try {
            fechaHora = util.convertStringToDate("dd/MM/yyyy HH:mm:ss.SSS", alertaRequest.getFechaHora());
        } catch (ParseException p) {
            response.setStatus(ResponseStatus.ERROR.id());
            response.setMessage("El formato de fecha debe ser: dd/MM/yyyy HH:mm:ss.SSS");
            return response;
        }
//        Alerta alerta = new Alerta();
//        alerta.setCodigomodem(alertaRequest.getNroTelefono());
//        alerta.setFechahoraevento(fechaHora);
//        alerta.setMensaje(alertaRequest.getMensaje());
//        try {
//            alertaService.create(alerta);
//            response.setStatus(ResponseStatus.SUCCESSFULL.id());
//            response.setMessage("OK");
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.setStatus(ResponseStatus.ERROR.id());
//            response.setMessage("NOK");
//        }
        return response;
    }
}
