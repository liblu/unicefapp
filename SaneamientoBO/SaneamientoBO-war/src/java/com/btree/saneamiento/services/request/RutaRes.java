/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import com.btree.saneamiento.bd.Evaluacion;
import com.btree.saneamiento.bd.Rutadetalle;
import java.io.Serializable;

/**
 *
 * @author Diana Mejia
 */
public class RutaRes implements Serializable{

    private Long rutadetid;
    private Long rutaid;
    private String descripcion;
    private Integer orden;
    private Long beneficiarioid;
    private String beneficiarioDes;
    private Long evaluacionid;
    private String estado;
    private Long monitorid;
    
    public RutaRes(Rutadetalle ruta, Evaluacion eval){
        this.rutadetid=ruta.getRutadetid();
        this.rutaid=ruta.getRutaid().getRutaid();
        this.descripcion=ruta.getRutaid().getDescripcion();
        this.orden=ruta.getOrden();
        this.beneficiarioid=ruta.getBeneficiarioid().getBeneficiarioid();
        this.beneficiarioDes=ruta.getBeneficiarioid().getNombre();
        this.evaluacionid=eval.getEvaluacionid();
        this.estado=eval.getEstado();
        this.monitorid=eval.getRutadetid().getRutaid().getMonitorid().getMonitorid();
    }

    public Long getMonitorid() {
        return monitorid;
    }

    public void setMonitorid(Long monitorid) {
        this.monitorid = monitorid;
    }

   
    
    

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }
    
    

    public Long getRutadetid() {
        return rutadetid;
    }

    public void setRutadetid(Long rutadetid) {
        this.rutadetid = rutadetid;
    }

    public Long getRutaid() {
        return rutaid;
    }

    public void setRutaid(Long rutaid) {
        this.rutaid = rutaid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Long getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Long beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }

    public String getBeneficiarioDes() {
        return beneficiarioDes;
    }

    public void setBeneficiarioDes(String beneficiarioDes) {
        this.beneficiarioDes = beneficiarioDes;
    }

    
}
