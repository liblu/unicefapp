/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.response;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class MovilIdResponse extends Response{
    private Integer movilId; 

    public Integer getMovilId() {
        return movilId;
    }

    public void setMovilId(Integer movilId) {
        this.movilId = movilId;
    }
    
     
}
