/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

/**
 *
 * @author Diana Mejía
 */
public class ServicioPeticionRes {
    private Long servicioid;
    private Long beneficiarioid;
    private String observacion;
    private int cantidad;

    
    

    public Long getServicioid() {
        return servicioid;
    }

    public void setServicioid(Long servicioid) {
        this.servicioid = servicioid;
    }

    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Long beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

  
    
    
}
