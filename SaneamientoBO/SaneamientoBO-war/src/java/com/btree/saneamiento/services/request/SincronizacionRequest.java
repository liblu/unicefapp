/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

/**
 *
 * @author Diana Mejia
 */
public class SincronizacionRequest {
    private String dtLastSync;
    private Long monitorId;
    private String user;

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
    

    public String getDtLastSync() {
        return dtLastSync;
    }

    public void setDtLastSync(String dtLastSync) {
        this.dtLastSync = dtLastSync;
    }

    public Long getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Long monitorId) {
        this.monitorId = monitorId;
    }

    @Override
    public String toString() {
        return "dtLastSync " +dtLastSync+"  monitorId "+monitorId;
    }
    

    
   
}
