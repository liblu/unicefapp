package com.btree.saneamiento.services;


import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;


/**
 * Created by Diana Mejia on 01/08/2017.
 */

public class SendNotification implements Runnable {
    
   
    
    private final String keyServer = "AIzaSyABn9qYVrE7XBQvSo_8tkwkH99dVSFXoGM ";
    String uri = "https://fcm.googleapis.com/fcm/send";
    
    private   Integer componente;
    private Integer tipoComp;
    private Integer tipMed;
   private Double medida;
    private String mensaje;

    public SendNotification(Integer componente, Integer tipoComp, Integer tipMed, Double medida, String mensaje) {
        this.componente = componente;
        this.tipoComp = tipoComp;
        this.tipMed = tipMed;
        this.medida = medida;
        this.mensaje = mensaje;
    }
    
    

    @Override
    public void run() {
       try {

         //  String topic= cooperativaService.find(ReportsCooperativaFacade.ID).getCooNombreCorto();
            JsonObject json = new JsonObject();
            json.addProperty("componente", componente);
            json.addProperty("tipoComp", tipoComp);
            json.addProperty("mensaje", mensaje);
            json.addProperty("tipoMed", tipMed);
            json.addProperty("medida", medida);

            JsonObject jsonBody = new JsonObject();
            jsonBody.addProperty("to", "/topics/"+"" );
            jsonBody.add("data", json);

            String request = jsonBody.toString();

            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key=" + keyServer);

// For POST only - START
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(request);
            wr.flush();
            wr.close();

            // For POST only - END
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                System.out.println(response.toString());
               //  Logger.getLogger(RecorridoFacadeREST.class.getName()).log(Level.INFO, null, response.toString());
            } else {
                System.out.println("POST request not worked");
            }

        } catch (IOException ex) {
           // Logger.getLogger(RecorridoFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
