/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.response;

import com.btree.saneamiento.services.request.RutaRes;
import java.util.List;

/**
 *
 * @author Diana Mejia
 */
public class RutaResponse extends Response {

 
    
    List<RutaRes> rutas;
  
   

    public List<RutaRes> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaRes> rutas) {
        this.rutas = rutas;
    }

  

   

}
