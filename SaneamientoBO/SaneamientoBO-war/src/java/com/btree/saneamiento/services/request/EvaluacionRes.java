/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

/**
 *
 * @author Diana Mejía
 */
public class EvaluacionRes {
    private Long evaluacionid;
    private String descripcion;
    private String picture;
    private Long rutadetid;
    private Long planillaid;
    private String ulastupdate;
    private String estado;

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }
    
    

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }
    
    

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Long getRutadetid() {
        return rutadetid;
    }

    public void setRutadetid(Long rutadetid) {
        this.rutadetid = rutadetid;
    }

    public Long getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Long planillaid) {
        this.planillaid = planillaid;
    }
    
    
}

