/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import com.btree.saneamiento.bd.Grupo;

/**
 *
 * @author Diana Mejía
 */
public class GrupoRes {
    private Long grupoid;
    private String descripcion;
    
    public GrupoRes(Grupo item){
        this.grupoid=item.getGrupoid();
        this.descripcion=item.getDescripcion();
    }

    public Long getGrupoid() {
        return grupoid;
    }

    public void setGrupoid(Long grupoid) {
        this.grupoid = grupoid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
            
    
}
