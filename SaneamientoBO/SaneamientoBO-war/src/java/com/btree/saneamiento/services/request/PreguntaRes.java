/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import com.btree.saneamiento.bd.Pregunta;
import java.io.Serializable;

/**
 *
 * @author Diana Mejia
 */
public class PreguntaRes implements Serializable{

    private Long preguntaid;
    private String descripcion;
    private String tiporespuesta;
    private Long grupoid;
    private String grupoDes;
    private Long planillaid;
    private String planillaDes;
    
    public PreguntaRes(Pregunta item){
       this.preguntaid=item.getPreguntaid();
                    this.descripcion=item.getDescripcion();
                    this.tiporespuesta=item.getTiporespuesta();
                     this.grupoid=item.getGrupoid().getGrupoid();
                    this.grupoDes=item.getGrupoid().getDescripcion();
                    this.planillaid=item.getPlanillaid().getPlanillaid();
                    this.planillaDes=item.getPlanillaid().getDescripcion();
                   
                    
    }

    public Long getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Long preguntaid) {
        this.preguntaid = preguntaid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTiporespuesta() {
        return tiporespuesta;
    }

    public void setTiporespuesta(String tiporespuesta) {
        this.tiporespuesta = tiporespuesta;
    }

    public Long getGrupoid() {
        return grupoid;
    }

    public void setGrupoid(Long grupoid) {
        this.grupoid = grupoid;
    }

    public String getGrupoDes() {
        return grupoDes;
    }

    public void setGrupoDes(String grupoDes) {
        this.grupoDes = grupoDes;
    }

    public Long getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Long planillaid) {
        this.planillaid = planillaid;
    }

    public String getPlanillaDes() {
        return planillaDes;
    }

    public void setPlanillaDes(String planillaDes) {
        this.planillaDes = planillaDes;
    }

    

}
