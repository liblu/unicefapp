/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import java.util.List;

/**
 *
 * @author Diana Mejia
 */
public class RegisterRequest {
    private EvaluacionRes evaluacion;
    private List<RespuestaRes> respuestas;
    private List<ServicioPeticionRes> servicios;

    public EvaluacionRes getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(EvaluacionRes evaluacion) {
        this.evaluacion = evaluacion;
    }

    public List<RespuestaRes> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<RespuestaRes> respuestas) {
        this.respuestas = respuestas;
    }

    public List<ServicioPeticionRes> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioPeticionRes> servicios) {
        this.servicios = servicios;
    }

   

    @Override
    public String toString() {
        return "rutadetid " +evaluacion.getRutadetid()+"  respuestas "+respuestas.size() +" servicios: "+servicios.size();
    }
    

    
   
}
