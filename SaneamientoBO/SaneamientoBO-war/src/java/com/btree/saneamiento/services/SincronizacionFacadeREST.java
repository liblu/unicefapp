/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services;

import com.btree.saneamiento.adm.BeneficiarioBean;
import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Evaluacion;
import com.btree.saneamiento.bd.Grupo;
import com.btree.saneamiento.bd.Pregunta;
import com.btree.saneamiento.bd.Respuesta;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.bd.Rutadetalle;
import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.bd.Serviciopeticion;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.EvaluacionFacade;
import com.btree.saneamiento.ln.GrupoFacade;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.PlanillaFacade;
import com.btree.saneamiento.ln.PreguntaFacade;
import com.btree.saneamiento.ln.RespuestaFacade;
import com.btree.saneamiento.ln.RutaFacade;
import com.btree.saneamiento.ln.RutadetalleFacade;
import com.btree.saneamiento.ln.ServicioFacade;
import com.btree.saneamiento.ln.ServiciopeticionFacade;
import com.btree.saneamiento.services.request.BeneficiarioRes;
import com.btree.saneamiento.services.request.GrupoRes;
import com.btree.saneamiento.util.Util;
import com.btree.saneamiento.services.request.PreguntaRes;
import com.btree.saneamiento.services.request.RegisterRequest;
import com.btree.saneamiento.services.request.RespuestaRes;
import com.btree.saneamiento.services.request.RutaRes;
import com.btree.saneamiento.services.request.ServicioPeticionRes;
import com.btree.saneamiento.services.request.ServicioRes;
import com.btree.saneamiento.services.request.SincronizacionRequest;
import com.btree.saneamiento.services.response.Response;
import com.btree.saneamiento.services.response.ResponseStatus;
import com.btree.saneamiento.services.response.RutaResponse;
import com.btree.saneamiento.services.response.SincronizacionResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;

/**
 * REST Web Service
 *
 * @author Diana Mejía
 */
@Path("sincronizacion")
@RequestScoped
public class SincronizacionFacadeREST {
  public static Logger log = Logger.getLogger(BeneficiarioBean.class);
  
    @Context
    private UriInfo context;
    
    @EJB
    private MonitorFacade monitorService;
    
    @EJB
    private RutaFacade rutaService;
    @EJB
    private RutadetalleFacade rutadetService;
    
    @EJB
    private BeneficiarioFacade beneficiarioService;
    @EJB
    private PreguntaFacade preguntaService;
    @EJB
    private GrupoFacade grupoService;
    @EJB
    private PlanillaFacade planillaService;
     
    @EJB
    private ServicioFacade servService;
     @EJB
    private ServiciopeticionFacade servPeticionService;
    @EJB
    private EvaluacionFacade evaluacionService;
    @EJB
    private RespuestaFacade respuestaService;
     
    @EJB 
    private Util util;

    /**
     * Creates a new instance of SincronizacionFacadeRest
     */
    public SincronizacionFacadeREST() {
    }

    /**
     * Retrieves representation of an instance of com.btree.saneamiento.services.SincronizacionFacadeRest
     * @return an instance of java.lang.String
     */
    @POST
    @Path("sync")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public SincronizacionResponse startSync(SincronizacionRequest request) {
        SincronizacionResponse response=new SincronizacionResponse();;
        try {
             log.info("request: "+request);
            
            String today= util.convertDateToString(Util.DATE_FORMAT, new Date());
//            if(today.compareTo(request.getDtLastSync())==0){
//                response.setStatus(ResponseStatus.ERROR_LOGICO.id());
//                response.setMessage("Ya existe sincronizacion para hoy");
//                return response;
//            }
                Date date=util.convertStringToDate(Util.DATE_FORMAT, today);
                Calendar cal=Calendar.getInstance();
                cal.setTime(date);
                int day=cal.get(Calendar.DAY_OF_WEEK);
                log.info("******DAY "+day);
                
                List<RutaRes> rutaList=new ArrayList<>();
                try{
                  Ruta ruta= rutaService.findRuta(request.getMonitorId(),String.valueOf(day) );
                
               
                   if(ruta!=null){
                    log.info("************ "+ ruta.getNombre()+" ***********");
                        for(Rutadetalle item : ruta.getRutadetalleCollection()){

                            item.setUlastupdate(request.getUser());
                            Evaluacion evaluacion=evaluacionService.createByRutadetalle(item);
                            rutaList.add(new RutaRes(item,evaluacion));
                        }
                          log.info("************rutas obtenidas***********");
                   }
                }catch(Exception e){
                    log.error("ruta: "+e);
                }
                
                response.setRutas(rutaList);
                
              
                //obtener beneficiarios
                List<Beneficiario> beneficiarios= beneficiarioService.findAll();
                List<BeneficiarioRes> beneficiarioResList=new ArrayList<>();
                for(Beneficiario ben:beneficiarios){
                    beneficiarioResList.add(new BeneficiarioRes(ben));
                }
                response.setBeneficiarios(beneficiarioResList);
                log.info("************beneficiarios obtenidos***********");
                
                //preguntas
                List<Pregunta> preguntas = preguntaService.findByPlanilla(Util.PLANILLA_DEFAULT);
                List<PreguntaRes> preguntaRespList=new ArrayList<>();
               
                for(Pregunta item:preguntas){
                    preguntaRespList.add(new PreguntaRes(item));
                }
                response.setPreguntas(preguntaRespList);
                log.info("************preguntas obtenidas***********");
                
                
                //grupo 
                
                List<Grupo> grupos = grupoService.findAll();
                List<GrupoRes> grupoRespList=new ArrayList<>();
               
                for(Grupo item:grupos){
                    grupoRespList.add(new GrupoRes(item));
                }
                response.setGrupos(grupoRespList);
                log.info("************grupos obtenidos***********");
                
                List<Servicio> servicios= servService.findAll();
                List<ServicioRes> serviciosRest=new ArrayList<>();
                for(Servicio item:servicios){
                    serviciosRest.add(new ServicioRes(item));
                }
                response.setServicios(serviciosRest);
                log.info("************servicios obtenidos***********");
                
                response.setStatus(ResponseStatus.SUCCESSFULL.id());
                response.setMessage("OK");
            
            
           
        } catch (Exception ex) {
            log.error(ex);
            response.setStatus(ResponseStatus.ERROR.id());
            response.setMessage("Error inesperado");
        }
        return response;
    }
    
     @POST
    @Path("ruta")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public RutaResponse downloadRuta(SincronizacionRequest request){
        RutaResponse response=new RutaResponse();;
        try {
             log.info("request: "+request);
                Calendar cal=Calendar.getInstance();                
                int day=cal.get(Calendar.DAY_OF_WEEK);
                log.info("******DAY "+day);
                Ruta ruta= rutaService.findRuta(request.getMonitorId(),String.valueOf(day));
                
               
                List<RutaRes> rutaList=new ArrayList<>();
                if(ruta!=null&& ruta.getRutadetalleCollection()!=null&& ruta.getRutadetalleCollection().size()>0){
                   log.info("************ "+ ruta.getNombre()+" ***********");
                    for(Rutadetalle item : ruta.getRutadetalleCollection()){
                        item.setUlastupdate(request.getUser());
                        Evaluacion evaluacion=evaluacionService.createByRutadetalle(item);
                        rutaList.add(new RutaRes(item,evaluacion));
                    }
                    response.setStatus(ResponseStatus.SUCCESSFULL.id());
                    response.setMessage("OK");
                    response.setRutas(rutaList);
                }else{
                    response.setStatus(ResponseStatus.ERROR_LOGICO.id());
                    response.setMessage("No existe ruta para hoy");
                    response.setRutas(rutaList);
                }
                
        } catch (Exception ex) {
            log.error(ex);
            response.setStatus(ResponseStatus.ERROR.id());
            response.setMessage("Error inesperado");
        }
        return response;
    }
    
      @POST
    @Path("register")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response evaluacionRegister(RegisterRequest request){
        Response response= new Response();
        try{
             Evaluacion evaluacion=evaluacionService.find(request.getEvaluacion().getEvaluacionid());
            if(evaluacion==null){
            
                response.setStatus(ResponseStatus.ERROR_LOGICO.id());
                response.setMessage("No existe una evaluacion para el beneficiario");
                return response;
            }
                      
            evaluacion.setDescripcion(request.getEvaluacion().getDescripcion());
            evaluacion.setUlastupdate(request.getEvaluacion().getUlastupdate());
          //  evaluacion.setPicture(picture);//pendiente las imagenes            
            evaluacionService.edit(evaluacion);
            log.info("evaluacion created");
            Respuesta respuesta;
            Pregunta pregunta;
            if(request.getRespuestas()!=null){
                for(RespuestaRes item:request.getRespuestas()){
                    respuesta=respuestaService.findByPreguntaEvaluacion(evaluacion.getEvaluacionid(),item.getPreguntaid());

                    if(respuesta==null){
                        respuesta=new Respuesta();
                        pregunta=preguntaService.find(item.getPreguntaid());
                        respuesta.setEvaluacionid(evaluacion);
                        respuesta.setPreguntaid(pregunta);
                        respuesta.setUlastupdate(evaluacion.getUlastupdate());
                        if(item.getRespuesta()!=null){
                            respuesta.setRespuesta(item.getRespuesta());
                        }else if(item.getRespuestavalue()!=null){
                            respuesta.setRespuestavalue(item.getRespuestavalue());
                        }else if(item.getRespuesttext()!=null){
                            respuesta.setRespuesttext(item.getRespuesttext());
                        }                
                         respuestaService.create(respuesta);
                    }else{
                        respuesta.setDtlastupdate(new Date());
                        respuesta.setUlastupdate(evaluacion.getUlastupdate());
                        if(item.getRespuesta()!=null){
                            respuesta.setRespuesta(item.getRespuesta());
                        }else if(item.getRespuestavalue()!=null){
                            respuesta.setRespuestavalue(item.getRespuestavalue());
                        }else if(item.getRespuesttext()!=null){
                            respuesta.setRespuesttext(item.getRespuesttext());
                        }                
                         respuestaService.edit(respuesta);
                    }



                }
                 log.info("respuestas resgistradas");
            }
             //servicios
             if(request.getServicios()!=null ){
                 Serviciopeticion servicio;
                 Servicio serv;
                 for (ServicioPeticionRes item : request.getServicios()) {
                     serv=servService.find(item.getServicioid());
                     if(serv!=null){
                        servicio=new Serviciopeticion();
                        servicio.setBeneficiarioid(evaluacion.getRutadetid().getBeneficiarioid());
                        servicio.setEvaluacionid(evaluacion);
                        servicio.setServicioid(serv);                        
                        servicio.setCantidad(item.getCantidad());                       
                        servicio.setUlastupdate(request.getEvaluacion().getUlastupdate());
                        servPeticionService.create(servicio);
                     }
                 }
             }
             log.info("peticiones de servicio resgistradas");
               response.setStatus(ResponseStatus.SUCCESSFULL.id());
               response.setMessage("OK");
        }catch(Exception ex){
             log.error(ex);
            response.setStatus(ResponseStatus.ERROR.id());
            response.setMessage("Error inesperado");
        }
        return response;
    }

    /**
     * PUT method for updating or creating an instance of SincronizacionFacadeRest
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }

    
}
