/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import com.btree.saneamiento.bd.Beneficiario;

/**
 *
 * @author Diana Mejía
 */
public class BeneficiarioRes {
    private int codigoepsa;
    
    private int distrito;
    
    private Double lat;
   
    private Double lon;

   
    private String estado;

  
    private Long beneficiarioid;
  
    private String nombre;
    
   
    private String direccion;
  
    private String entidad;
   
    private boolean autorizamonitoreo;
   
    private boolean programaproteccion;
    
    
    public BeneficiarioRes(Beneficiario ben){
        this.beneficiarioid=ben.getBeneficiarioid();
        this.nombre=ben.getNombre();
        this.codigoepsa=ben.getCodigoepsa();
        this.distrito=ben.getDistrito();
        this.direccion=ben.getDireccion();
        this.lat=ben.getLat();
        this.lon=ben.getLon();
        this.entidad=ben.getEntidad();
        this.autorizamonitoreo=ben.getAutorizamonitoreo();
        this.programaproteccion=ben.getProgramaproteccion();
    }

    public int getCodigoepsa() {
        return codigoepsa;
    }

    public void setCodigoepsa(int codigoepsa) {
        this.codigoepsa = codigoepsa;
    }

    public int getDistrito() {
        return distrito;
    }

    public void setDistrito(int distrito) {
        this.distrito = distrito;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Long beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public boolean isAutorizamonitoreo() {
        return autorizamonitoreo;
    }

    public void setAutorizamonitoreo(boolean autorizamonitoreo) {
        this.autorizamonitoreo = autorizamonitoreo;
    }

    public boolean isProgramaproteccion() {
        return programaproteccion;
    }

    public void setProgramaproteccion(boolean programaproteccion) {
        this.programaproteccion = programaproteccion;
    }
    
    
    
}
