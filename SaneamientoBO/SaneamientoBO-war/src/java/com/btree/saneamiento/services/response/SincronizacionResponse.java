/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.response;

import com.btree.saneamiento.services.request.BeneficiarioRes;
import com.btree.saneamiento.services.request.GrupoRes;
import com.btree.saneamiento.services.request.PreguntaRes;
import com.btree.saneamiento.services.request.RutaRes;
import com.btree.saneamiento.services.request.ServicioRes;
import java.util.List;

/**
 *
 * @author Diana Mejia
 */
public class SincronizacionResponse extends Response {

 
    List<BeneficiarioRes> beneficiarios;
    List<RutaRes> rutas;
    List<PreguntaRes> preguntas;
    List<GrupoRes> grupos;
    List<ServicioRes> servicios;

    public List<ServicioRes> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioRes> servicios) {
        this.servicios = servicios;
    }
    

    
    
    public List<BeneficiarioRes> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<BeneficiarioRes> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    public List<RutaRes> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaRes> rutas) {
        this.rutas = rutas;
    }

    public List<PreguntaRes> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<PreguntaRes> preguntas) {
        this.preguntas = preguntas;
    }

    public List<GrupoRes> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoRes> grupos) {
        this.grupos = grupos;
    }
    

   

}
