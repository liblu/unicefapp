/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

import com.btree.saneamiento.bd.Servicio;

/**
 *
 * @author Diana Mejía
 */
public class ServicioRes {
    private Long servicioid;
    private String nombre;
    private String observacion;
    private boolean especificarcantidad;

    public ServicioRes(Servicio item) {
        this.servicioid = item.getServicioid();
        this.nombre = item.getNombre();
        this.observacion = item.getObservacion();
        this.especificarcantidad = item.getEspecificarcantidad();
    }
    
    

    public Long getServicioid() {
        return servicioid;
    }

    public void setServicioid(Long servicioid) {
        this.servicioid = servicioid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public boolean isEspecificarcantidad() {
        return especificarcantidad;
    }

    public void setEspecificarcantidad(boolean especificarcantidad) {
        this.especificarcantidad = especificarcantidad;
    }
    
    
}
