/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services.request;

/**
 *
 * @author Diana Mejía
 */
public class RespuestaRes {
    private Long preguntaid;
    private Boolean respuesta;
    private String respuesttext;
    private Integer respuestavalue;

    public Long getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Long preguntaid) {
        this.preguntaid = preguntaid;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuesttext() {
        return respuesttext;
    }

    public void setRespuesttext(String respuesttext) {
        this.respuesttext = respuesttext;
    }

    public Integer getRespuestavalue() {
        return respuestavalue;
    }

    public void setRespuestavalue(Integer respuestavalue) {
        this.respuestavalue = respuestavalue;
    }
    
    
    
}
