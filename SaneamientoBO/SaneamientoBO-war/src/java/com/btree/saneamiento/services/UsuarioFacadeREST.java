/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.services;


import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.util.AppException;
import com.btree.saneamiento.services.request.LoginUsuarioRequest;
import com.btree.saneamiento.services.response.LoginUsuarioResponse;
import com.btree.saneamiento.services.response.ResponseStatus;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Diana Mejia
 */
@Stateless
@Path("usuario")
public class UsuarioFacadeREST {

    @Context
    private UriInfo context;

    @EJB
    private UsuarioFacade usuarioService;
    @EJB
    private MonitorFacade monitorService;
   

    @POST
    @Path("loginUsuario")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public LoginUsuarioResponse loginUsuario(LoginUsuarioRequest request) {
        LoginUsuarioResponse loginUsuarioResponse = new LoginUsuarioResponse();
        try {
            Usuario usuario = usuarioService.loginUser(request.getUser(), request.getPassword());
            Monitor monitor= monitorService.findByUsuario(usuario);
                
                loginUsuarioResponse.setNombre(monitor.getNombre());
                loginUsuarioResponse.setMonitorId(monitor.getMonitorid());
                loginUsuarioResponse.setUsuarioid(usuario.getUsuarioid());
                loginUsuarioResponse.setStatus(ResponseStatus.SUCCESSFULL.id());
                loginUsuarioResponse.setMessage("OK");

            
        }catch(AppException ape){
            loginUsuarioResponse.setStatus(ResponseStatus.ERROR.id());
            loginUsuarioResponse.setMessage(ape.getMessage());
        } catch (Exception e) {
            loginUsuarioResponse.setStatus(ResponseStatus.ERROR.id());
            loginUsuarioResponse.setMessage("Fallo al iniciar sesión");
        }
        return loginUsuarioResponse;
    }

//    @POST
//    @Path("changePassword")
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public Response changePassword(ChangePasswordRequest request) {
//        Response response = new Response();
//        try {
//         //   usuarioService.changePassword(request.getUser(), request.getPasswordActual(), request.getPasswordNuevo());
//            response.setStatus(0);
//            response.setMessage("OK");
//        } catch (Exception e) {
//            response.setStatus(1);
//            response.setMessage(e.getMessage());
//        }
//        return response;
//    }
}
