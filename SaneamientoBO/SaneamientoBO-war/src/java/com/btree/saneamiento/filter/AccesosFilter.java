/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.filter;


import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.ln.PermisosFacade;
import com.btree.saneamiento.ln.ProgramaFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

@WebFilter(filterName = "AccesosFilter", urlPatterns = {"/adm/*"/*, "/faces/javax.faces.resource/jquery/*"*/})
public class AccesosFilter implements Filter {

    public static Logger log = Logger.getLogger(AccesosFilter.class);
    private static final boolean DEBUG = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    @EJB
    private ProgramaFacade programaService;
    @EJB
    private PermisosFacade permisoService;
    @Inject
    LoginBean login;

    public AccesosFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpSession session = ((HttpServletRequest) request).getSession();
            HttpServletRequest req = (HttpServletRequest) request;
            String path = req.getRequestURI();
            log.info(path);
            if (login != null) {
                if (login.getUser() == null) {
                    session.invalidate();
                    req.getSession(true);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/login.xhtml");
                    dispatcher.forward(request, response);
                } else {
                    String ip = request.getRemoteAddr();
                    String usuario = login.getUser();
                    log.info("User: " + usuario + " Ip: " + ip);
                    int posInicial = path.lastIndexOf("/");
                    int posFinal = path.lastIndexOf(".");
                    String urlPrograma = path.substring(posInicial + 1, posFinal);
                    log.info(urlPrograma);
                    Programa programa = programaService.obtainProgram(urlPrograma);
                    if ((programa == null || !permisoService.tienePermiso(login.getUsuario(), programa)) && !urlPrograma.equals("dashboard")) {
                        RequestDispatcher dispatcher = request.getRequestDispatcher("/404.xhtml");
                        dispatcher.forward(request, response);
                    } else {
                        chain.doFilter(request, response);
                    }
                }
            } else {
                session.invalidate();
                req.getSession(true);
                RequestDispatcher dispatcher = request.getRequestDispatcher("/login.xhtml");
                dispatcher.forward(request, response);
                /*RequestDispatcher dispatcher = request.getRequestDispatcher("/login.xhtml");
                dispatcher.forward(request, response);*/
            }
        } catch (Exception e) {
            //RequestDispatcher dispatcher = request.getRequestDispatcher("/login.xhtml");
            //dispatcher.forward(request, response);
            e.printStackTrace();
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (DEBUG) {
                log("AccesosFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     *
     * @return
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AccesosFilter()");
        }
        StringBuffer sb = new StringBuffer("AccesosFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
