/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.btree.saneamiento.converter;

import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.ln.PerfilFacade;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "perfilConverter")
@RequestScoped
public class PerfilConverter implements Converter{
    @Inject
    private PerfilFacade perfilService;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try{
        return value.isEmpty()?null: perfilService.find(Short.valueOf(value));
        }catch(NumberFormatException e){}
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof Perfil) {
            return ((Perfil) object).getPerfilid()+"";
        } else {
            return null;
        }

    }
}
