/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.converter;


import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "beneficiarioConverter")
@RequestScoped
public class BeneficiarioConverter implements Converter {

    @Inject
    private BeneficiarioFacade beneficiarioService;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value!=null && !value.trim().isEmpty()&& !value.equals("Ninguno")) {
            return beneficiarioService.find(Long.valueOf(value));
        }else{
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof Beneficiario) {
            return ((Beneficiario) object).getBeneficiarioid()+"";
        } else {
            return null;
        }

    }
}
