/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.converter;


import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.ln.ProgramaFacade;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "programaConverter")
@RequestScoped
public class ProgramaConverter implements Converter {

    @Inject
    private ProgramaFacade programaService;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        return programaService.find(Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof Programa) {
            return ((Programa) object).getProgramaid()+"";
        } else {
            return null;
        }

    }
}
