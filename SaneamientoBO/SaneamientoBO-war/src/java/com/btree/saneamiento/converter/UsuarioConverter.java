/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.converter;


import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "usuarioConverter")
@RequestScoped
public class UsuarioConverter implements Converter {

    @Inject
    private UsuarioFacade usuarioService;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value!=null && !value.trim().isEmpty()&& !value.equals("Ninguno")) {
            return usuarioService.find(Long.valueOf(value));
        }else{
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object instanceof Usuario) {
            return ((Usuario) object).getUsuarioid()+"";
        } else {
            return null;
        }

    }
}
