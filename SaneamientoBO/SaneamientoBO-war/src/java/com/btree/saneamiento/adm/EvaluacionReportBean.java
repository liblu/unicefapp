/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Evaluacion;
import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Respuesta;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.bd.Serviciopeticion;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.EvaluacionFacade;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.RespuestaFacade;
import com.btree.saneamiento.ln.RutaFacade;
import com.btree.saneamiento.ln.ServicioFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "evaluacionReportBean")
@ViewScoped
public class EvaluacionReportBean implements Serializable {

    public static Logger log = Logger.getLogger(EvaluacionReportBean.class);

    @EJB
    private Util util;
    
    @EJB
    private RutaFacade rutaService;
    @EJB
    private ServicioFacade servicioService;
    
    @EJB
    private EvaluacionFacade evaluacionService;
    
    @EJB
    private BeneficiarioFacade beneficiarioService;
    @EJB
    private MonitorFacade monitorService;
     
    @EJB
    private RespuestaFacade respuestaService;
    @Inject
    private LoginBean login;

   
    
    private Evaluacion evaluacionSelected;
    private List<Servicio> servicios;
    private List<Beneficiario> beneficiarios;
    private List<Monitor> monitores;
    private List<Ruta> rutas;
    private List<Respuesta> respuestas;
    private LazyDataModel<Evaluacion> lista;
    private boolean visibleEvalDetalle;
    private Respuesta respuestaSelected;
 //   private BigInteger cantidadBolsas;

//filtros de busqueda
    private Date fechaInicial;
    private Date fechaFinal;
    private Long ruta;
    private Long beneficiario;
    private Long monitor;
    private String estadoSelected;

    @PostConstruct
    public void init() {
        visibleEvalDetalle = false;
        monitores=monitorService.findAll();
        servicios = servicioService.findAll();       
        beneficiarios=beneficiarioService.findAll();
        rutas=rutaService.findAll();
        
        fechaInicial = new Date();
        fechaFinal = new Date();
        buscar();
    }
   //Metodos
  public void buscar() {
        lista = new LazyDataModel<Evaluacion>() {
            private static final long serialVersionUID = 889622822782770039L;

            @Override
            public List<Evaluacion> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, Object> filters) {
                try {
                    
                    fechaInicial = fechaInicial != null ? util.getFechaHoraInicial(fechaInicial) : null;
                    fechaFinal = fechaFinal != null ? util.getFechaHoraFinal(fechaFinal) : null;
                    
                    log.info("request: fIni "+util.convertDateToString("dd/MM/yyyy HH:mm:ss", fechaInicial)+" fFin " +util.convertDateToString("dd/MM/yyyy HH:mm:ss", fechaFinal)+ " monitor "+monitor+ "  beneficiario "+ beneficiario +"  estado "+estadoSelected);
                    List<Evaluacion> listtr = evaluacionService.findTransacciones(fechaInicial, fechaFinal,beneficiario,monitor, estadoSelected, first, pageSize);
                    long count = evaluacionService.countTransacciones(fechaInicial, fechaFinal,beneficiario, monitor,estadoSelected );

                    this.setRowCount((int) count);
                    return listtr;
                } catch (Exception e) {
                    log.error("Excepcion al paginar ", e);
                }
                return new ArrayList<>();
            }
        };
    }
    

    public void limpiar() {
        fechaInicial = null;
        fechaFinal = null;
        estadoSelected=null;
        monitor=null;
        beneficiario=null;
        lista = null;
    }
    
    public void detalle(Evaluacion evaluacion){
        visibleEvalDetalle=true;
        respuestas=respuestaService.respuestasByEvaluacion(evaluacion.getEvaluacionid());
    }
    
    public void delete(Serviciopeticion servicio) {
        try {
            if(servicio.getEstado().equals(Util.PENDIENTE)&& servicio.getUlastupdate().equals(login.getUser())){
               // servicioPeticionService.remove(servicio);
                util.showInfo(Util.MSG_DELETE);
            }
            util.showInfo(Util.MSG_DELETE_FAIL);
            
        } catch (Exception e) {          
                log.error("deleteServ", e);
                util.showError(Util.MSG_CRASH);
            
        }
    }
    
    
    public List<Ruta> getRutas() {
        return rutas;
    }

    public void setRutas(List<Ruta> rutas) {
        this.rutas = rutas;
    }

    public LazyDataModel<Evaluacion> getLista() {
        return lista;
    }

    //Propiedades

    public Respuesta getRespuestaSelected() {
        return respuestaSelected;
    }

    public void setRespuestaSelected(Respuesta respuestaSelected) {
        this.respuestaSelected = respuestaSelected;
    }

    
    
    public void setVisibleEvalDetalle(boolean visibleNuevoEditar) {
        this.visibleEvalDetalle = visibleNuevoEditar;
    }
    
    
    public void setLista(LazyDataModel<Evaluacion> lista) {
        this.lista = lista;
    }

    public boolean isVisibleEvalDetalle() {
        return visibleEvalDetalle;
    }
    
    

    public Long getBeneficiario() {
        return beneficiario;
    }

    public Evaluacion getEvaluacionSelected() {
        return evaluacionSelected;
    }

    public void setEvaluacionSelected(Evaluacion evaluacionSelected) {
        this.evaluacionSelected = evaluacionSelected;
    }

    public List<Monitor> getMonitores() {
        return monitores;
    }

    public void setMonitores(List<Monitor> monitores) {
        this.monitores = monitores;
    }

   
    
   
    public void setBeneficiario(Long beneficiario) {
        this.beneficiario = beneficiario;
    }

    public void setEstadoSelected(String estadoSelected) {
        this.estadoSelected = estadoSelected;
    }
    
    public String getEstadoSelected() {    
        return estadoSelected;
    }
    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Long getRuta() {
        return ruta;
    }

    public void setRuta(Long ruta) {
        this.ruta = ruta;
    }

    public Long getMonitor() {
        return monitor;
    }

    public void setMonitor(Long monitor) {
        this.monitor = monitor;
    }
    
    
    
    
   

    public List<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }

    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    public List<Respuesta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }
   

    

    

   

    

    
    
   
    
}
