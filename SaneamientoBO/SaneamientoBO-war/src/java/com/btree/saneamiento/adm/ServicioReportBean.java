/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.bd.Serviciopeticion;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.ServicioFacade;
import com.btree.saneamiento.ln.ServiciopeticionFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "servicioReportBean")
@ViewScoped
public class ServicioReportBean implements Serializable {

    public static Logger log = Logger.getLogger(ServicioReportBean.class);

    @EJB
    private Util util;
    
    @EJB
    private ServicioFacade servicioService;
    
     @EJB
    private ServiciopeticionFacade servicioPeticionService;
    
    @EJB
    private BeneficiarioFacade beneficiarioService;
     
    @Inject
    private LoginBean login;

   
    
//    private Servicio servicioSelected;
//    private Beneficiario beneficiarioSelected;    
    private Serviciopeticion peticionSelected;
    
    
    private List<Servicio> servicios;
    private List<Beneficiario> beneficiarios;
    private LazyDataModel<Serviciopeticion> lista;
 //   private BigInteger cantidadBolsas;

//filtros de busqueda
    private Date fechaInicial;
    private Date fechaFinal;
    private Long servicio;
    private Long beneficiario;
    private String estadoSelected;

    @PostConstruct
    public void init() {
        
         
        servicios = servicioService.findAll();       
        beneficiarios=beneficiarioService.findAll();
        
        
        fechaInicial = new Date();
        fechaFinal = new Date();
        buscar();
    }
   //Metodos
  public void buscar() {
        lista = new LazyDataModel<Serviciopeticion>() {
            private static final long serialVersionUID = 889622822782770039L;

            @Override
            public List<Serviciopeticion> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, Object> filters) {
                try {
                    
                    fechaInicial = fechaInicial != null ? util.getFechaHoraInicial(fechaInicial) : null;
                    fechaFinal = fechaFinal != null ? util.getFechaHoraFinal(fechaFinal) : null;
                    
                    log.info("request: fIni "+util.convertDateToString("dd/MM/yyyy HH:mm:ss", fechaInicial)+" fFin " +util.convertDateToString("dd/MM/yyyy HH:mm:ss", fechaFinal)+ " servicio "+servicio+ "  beneficiario "+ beneficiario +"  estado "+estadoSelected);
                    List<Serviciopeticion> listtr = servicioPeticionService.findTransacciones(fechaInicial, fechaFinal,beneficiario,servicio, estadoSelected, first, pageSize);
                    long count = servicioPeticionService.countTransacciones(fechaInicial, fechaFinal,beneficiario, servicio,estadoSelected );

                    this.setRowCount((int) count);
                    return listtr;
                } catch (Exception e) {
                    log.error("Excepcion al paginar ", e);
                }
                return new ArrayList<>();
            }
        };
    }
    

    public void limpiar() {
        fechaInicial = null;
        fechaFinal = null;
        estadoSelected=null;
        servicio=null;
        beneficiario=null;
        lista = null;
    }
    
    public void edit(Serviciopeticion servicio){
        
    }
    
    public void delete(Serviciopeticion servicio) {
        try {
            if(servicio.getEstado().equals(Util.PENDIENTE)){
                servicioPeticionService.remove(servicio);
                util.showInfo(Util.MSG_DELETE);
            }else
             util.showInfo("El servicio está en curso");
            
        } catch (Exception e) {          
                log.error("deleteServ", e);
                util.showError(Util.MSG_CRASH);
            
        }
    }
    
    
     //Propiedades
    
    public Long getServicio() {
        return servicio;
    }

    public void setServicio(Long servicio) {
        this.servicio = servicio;
    }

    public Long getBeneficiario() {
        return beneficiario;
    }

    public Serviciopeticion getPeticionSelected() {
        return peticionSelected;
    }

    public void setPeticionSelected(Serviciopeticion peticionSelected) {
        this.peticionSelected = peticionSelected;
    }

    
   
    public void setBeneficiario(Long beneficiario) {
        this.beneficiario = beneficiario;
    }

    public void setEstadoSelected(String estadoSelected) {
        this.estadoSelected = estadoSelected;
    }
    
    public String getEstadoSelected() {    
        return estadoSelected;
    }
    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
    
    
    
    public LazyDataModel<Serviciopeticion> getPeticionServicios() {
        return lista;
    }

    public void setPeticionServicios(LazyDataModel<Serviciopeticion> peticionServicios) {
        this.lista = peticionServicios;
    }

    public ServiciopeticionFacade getServicioPeticionService() {
        return servicioPeticionService;
    }

    public void setServicioPeticionService(ServiciopeticionFacade servicioPeticionService) {
        this.servicioPeticionService = servicioPeticionService;
    }

   

    public List<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }

    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }
   

    

    

   

    

    
    
   
    
}
