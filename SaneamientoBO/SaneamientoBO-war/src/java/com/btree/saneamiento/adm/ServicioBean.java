/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.ln.ServicioFacade;
import com.btree.saneamiento.ln.ServiciopeticionFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "servicioBean")
@ViewScoped
public class ServicioBean implements Serializable {

    public static Logger log = Logger.getLogger(ServicioBean.class);

@EJB
    private Util util;
     
    @EJB
    private ServicioFacade servicioService;
    @EJB
    private ServiciopeticionFacade peticionService;
    
    
    @Inject
    private LoginBean login;
    
    private List<Servicio> servicioList;

    
    private Servicio servicioSelected;
    private boolean visibleNuevoEditar;
    private boolean editActive;

   
    
    @PostConstruct
    public void init() {
         
        visibleNuevoEditar = false;
        editActive = false;
        servicioSelected = new Servicio();      
        servicioList=servicioService.findAll();
    }
    
    
   //Metodos
    public void save() {
        try {
            
            if (editActive) {  
                servicioSelected.setUlastupdate(login.getUser());
                servicioService.edit(servicioSelected);
            } else {               
               servicioSelected.setUlastupdate(login.getUser());
                servicioService.create(servicioSelected);
            }
            
            editActive = false;
            visibleNuevoEditar = false;            
            servicioList = servicioService.findAll();
            servicioSelected = new Servicio();
            util.showInfo("Registro realizado");
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void create() {
        visibleNuevoEditar = true;
        editActive = false; 
        servicioSelected = new Servicio();
        servicioList = servicioService.findAll();
            
    }

    public void edit(Servicio servicio) {
       visibleNuevoEditar = true;
       editActive = true;
       servicioSelected = servicio;
       servicioList = servicioService.findAll();
           
       
    }
public void delete(Servicio servicio) {
        try {
            if(peticionService.existeServicio(servicio)){
                util.showError(Util.MSG_DELETE_FAIL);
                return;
            }
            servicioService.remove(servicio);
            log.error(": Se procede al eliminado fisico");         
            util.showInfo(Util.MSG_DELETE);
            servicioList = servicioService.findAll();
        } catch (Exception e) {
           /* try {
                log.error(ex.getMessage() + ": Se procede al eliminado logico", ex);
                 servicio.setEstado(Util.INHABILITADO);            
                servicioService.edit(servicio);
                util.showInfo(Util.MSG_DELETE);
                 servicioList = servicioService.findAll();
            } catch (Exception e) {*/
                log.error(e.getMessage(), e);
                util.showError(Util.MSG_CRASH);
          // }
        }
    }
   
    //Propiedades
    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }
    
    public ServicioFacade getServicioService() {
        return servicioService;
    }

    public void setServicioService(ServicioFacade servicioService) {
        this.servicioService = servicioService;
    }

    public List<Servicio> getServicioList() {
        return servicioList;
    }

    public void setServicioList(List<Servicio> servicioList) {
        this.servicioList = servicioList;
    }

    public Servicio getServicioSelected() {
        return servicioSelected;
    }

    public void setServicioSelected(Servicio servicioSelected) {
        this.servicioSelected = servicioSelected;
    }

    public boolean isVisibleNuevoEditar() {
        return visibleNuevoEditar;
    }

    public void setVisibleNuevoEditar(boolean visibleNuevoEditar) {
        this.visibleNuevoEditar = visibleNuevoEditar;
    }

   
    
    

    
   

    

    

   

    

    
    
   
    
}
