/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.bd.Serviciopeticion;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.ServicioFacade;
import com.btree.saneamiento.ln.ServiciopeticionFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "solicitudServicioBean")
@ViewScoped
public class SolicitudServicioBean implements Serializable {

    public static Logger log = Logger.getLogger(SolicitudServicioBean.class);

    @EJB
    private Util util;
    
    @EJB
    private ServicioFacade servicioService;
    
     @EJB
    private ServiciopeticionFacade servicioPeticionService;
    
    @EJB
    private BeneficiarioFacade beneficiarioService;
     
    @Inject
    private LoginBean login;

   private static final Map<Long, Integer> SERVICIO_CANT = new HashMap<>();
    
    
    private List<Servicio> serviciosSelected;
    private Servicio servicioSelected;
    private Beneficiario beneficiarioSelected;    
    private Serviciopeticion peticionSelected;
    
    
    private List<Servicio> servicios;
    private List<Beneficiario> beneficiarios;
    private LazyDataModel<Serviciopeticion> lista;
    private int cantidadBolsas;



    @PostConstruct
    public void init() {
        
        serviciosSelected = new ArrayList<>();       
        servicios = servicioService.findAll();       
        beneficiarios=beneficiarioService.findAll();
        
        
       
    }
   //Metodos
  
    public void save() {
        try {
            if(serviciosSelected.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar  un Servicio", ""));
                return;
            }
            
            //check cantidad
            int cantidad=0;
             for(Servicio item: serviciosSelected){
                 if(item.getEspecificarcantidad()){
                     try{
                        cantidad=SERVICIO_CANT.get(item.getServicioid());
                     }catch(Exception e){
                     }
                     if( cantidad==0){
                         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe agregar cantidar para el servicio "+item.getNombre(), ""));
                
                         return;
                         
                     }
                 }
             }
            
                for(Servicio item: serviciosSelected){          
                    Serviciopeticion entity= new Serviciopeticion();
                    if(item.getEspecificarcantidad()){
                        cantidad=SERVICIO_CANT.get(item.getServicioid());
                        entity.setCantidad(cantidad);
                    }
                    entity.setBeneficiarioid(beneficiarioSelected);
                    entity.setServicioid(item);
                    entity.setUlastupdate(login.getUser());
                    servicioPeticionService.create(entity);
                }
            
           
            util.showInfo("Registro realizado");
            servicios = servicioService.findAll();
            serviciosSelected = new ArrayList<>();
            beneficiarioSelected=new Beneficiario();
            cantidadBolsas=1;
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

   
    
   public void agregarCantidad(){
      if(servicioSelected!=null){
       log.info("Servicio selecte "+servicioSelected.getNombre());
       SERVICIO_CANT.put(servicioSelected.getServicioid(), cantidadBolsas);
       
       
       
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cantidad "+cantidadBolsas+" agregado, correctamente", ""));
       cantidadBolsas=0;
            RequestContext.getCurrentInstance().execute("PF('changePass').hide()");
      }
   }
    
  
    
    
    
    
    

    //Propiedades
   
   
    
    public void setServicioSelected(Servicio servicioSelected) {
        this.servicioSelected = servicioSelected;
    }

    public Servicio getServicioSelected() {
        return servicioSelected;
    }

    public Beneficiario getBeneficiarioSelected() {
        return beneficiarioSelected;
    }

    public void setBeneficiarioSelected(Beneficiario beneficiarioSelected) {
        this.beneficiarioSelected = beneficiarioSelected;
    }
    
    public Serviciopeticion getPeticionSelected() {
        return peticionSelected;
    }

    public void setPeticionSelected(Serviciopeticion peticionSelected) {
        this.peticionSelected = peticionSelected;
    }

    public int getCantidadBolsas() {
        return cantidadBolsas;
    }

    public void setCantidadBolsas(int cantidadBolsas) {
        this.cantidadBolsas = cantidadBolsas;
    }
    
    public LazyDataModel<Serviciopeticion> getPeticionServicios() {
        return lista;
    }

    public void setPeticionServicios(LazyDataModel<Serviciopeticion> peticionServicios) {
        this.lista = peticionServicios;
    }

    public ServiciopeticionFacade getServicioPeticionService() {
        return servicioPeticionService;
    }

    public void setServicioPeticionService(ServiciopeticionFacade servicioPeticionService) {
        this.servicioPeticionService = servicioPeticionService;
    }

    public List<Servicio> getServiciosSelected() {
        return serviciosSelected;
    }

    public void setServiciosSelected(List<Servicio> serviciosSelected) {
        this.serviciosSelected = serviciosSelected;
    }

    

    public List<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<Servicio> servicios) {
        this.servicios = servicios;
    }

    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }
   

    

    

   

    

    
    
   
    
}
