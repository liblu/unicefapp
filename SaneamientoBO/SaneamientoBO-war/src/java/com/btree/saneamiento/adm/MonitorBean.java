/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.RutaFacade;
import com.btree.saneamiento.ln.UsuarioFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "monitorBean")
@ViewScoped
public class MonitorBean implements Serializable {

    public static Logger log = Logger.getLogger(MonitorBean.class);

@EJB
    private Util util;
     
    @EJB
    private MonitorFacade monitorService;
    @EJB
    private UsuarioFacade usuarioService;
    
    @EJB
    private RutaFacade rutaService;
    
    @Inject
    private LoginBean login;
    
    private List<Monitor> monitorList;
    private List<Usuario> usuarioList;
    
    private Monitor monitorSelected;
    private boolean visibleNuevoEditar;
    private boolean editActive;

    
    
    @PostConstruct
    public void init() {
         
        visibleNuevoEditar = false;
        editActive = false;
        monitorSelected = new Monitor();      
        monitorList=monitorService.findAll();
        usuarioList=usuarioService.findByPermisoMovil();
        
    }
    
    
   //Metodos
    public void save() {
        try {
           
            Monitor monitor=  monitorService.findByUser(monitorSelected.getUsuarioid());
         
            
            if (editActive) {  
                if(monitor!=null && monitor.getUsuarioid().equals(monitorSelected.getUsuarioid())){
                    util.showInfo("El usuario ya se encuentra asignado al monitor "+monitor.getNombre());
                    return;
                }
                monitorSelected.setUlastupdate(login.getUser());
                monitorSelected.setDtlastupdate(new Date());
                monitorService.edit(monitorSelected);
            } else {  
                if(monitor!=null){
                    util.showInfo("El usuario ya se encuentra asignado al monitor "+monitor.getNombre());
                    return;
                }
               monitorSelected.setUlastupdate(login.getUser());
                monitorService.create(monitorSelected);
            }
            
            editActive = false;
            visibleNuevoEditar = false;            
            monitorList = monitorService.findAll();
            monitorSelected = new Monitor();
            util.showInfo("Registro realizado");
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void create() {
        visibleNuevoEditar = true;
        editActive = false; 
        monitorSelected = new Monitor();
        monitorList = monitorService.findAll();
         
    }

    public void edit(Monitor monitor) {
       visibleNuevoEditar = true;
       editActive = true;
       monitorSelected = monitor;
       monitorList = monitorService.findAll();
         
       
    }

    public void delete(Monitor monitor) {
        try {
            if(rutaService.existeMonitor(monitor)){
                 util.showError(Util.MSG_DELETE_FAIL);
                 return;
            }
            monitorService.remove(monitor);
            log.error(": Se procede al eliminado fisico");         
            util.showInfo(Util.MSG_DELETE);
            monitorList = monitorService.findAll();
        } catch (Exception e) {
            
                log.error(e.getMessage(), e);
                util.showError(Util.MSG_CRASH);
            
        }
    }

 
   
    //Propiedades


    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }
    
    

    public boolean isVisibleNuevoEditar() {
        return visibleNuevoEditar;
    }

    public void setVisibleNuevoEditar(boolean visibleNuevoEditar) {
        this.visibleNuevoEditar = visibleNuevoEditar;
    }

    public List<Monitor> getMonitorList() {
        return monitorList;
    }

    public void setMonitorList(List<Monitor> monitorList) {
        this.monitorList = monitorList;
    }

    public Monitor getMonitorSelected() {
        return monitorSelected;
    }

    public void setMonitorSelected(Monitor monitorSelected) {
        this.monitorSelected = monitorSelected;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    
    
    
   
    
    

    
   

    

    

   

    

    
    
   
    
}
