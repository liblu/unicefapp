/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Pregunta;
import com.btree.saneamiento.ln.PlanillaFacade;
import com.btree.saneamiento.ln.PreguntaFacade;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "planillaBean")
@ViewScoped
public class PlanillaBean implements Serializable {

    public static Logger log = Logger.getLogger(PlanillaBean.class);


     
    @EJB
    private PreguntaFacade preguntaService;
    
    private List<Pregunta> preguntaList;
    
    @PostConstruct
    public void init() {
         
        preguntaList = preguntaService.findByPlanilla(Util.PLANILLA_DEFAULT);       
   
    }
    
   //Metodos
    public void save() {
        
    }

   
    //Propiedades

    public List<Pregunta> getPreguntaList() {
        return preguntaList;
    }

    public void setPreguntaList(List<Pregunta> preguntaList) {
        this.preguntaList = preguntaList;
    }

   
    
    

    
   

    

    

   

    

    
    
   
    
}
