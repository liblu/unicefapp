/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Planilla;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.EvaluacionFacade;
import com.btree.saneamiento.ln.MonitorFacade;
import com.btree.saneamiento.ln.PlanillaFacade;
import com.btree.saneamiento.ln.RutaFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "rutaBean")
@ViewScoped
public class RutaBean implements Serializable {

    public static Logger log = Logger.getLogger(RutaBean.class);

    @EJB
    private Util util;
    @EJB
    private RutaFacade rutaService;
    
    @EJB
    private BeneficiarioFacade beneficiarioService;
    @EJB
    private MonitorFacade monitorService;
    
    @EJB
    private PlanillaFacade planillaService;
    
    @EJB
    private EvaluacionFacade evaluacionService;
    
    
    @Inject
    private LoginBean login;

    private MapModel draggableModel;

    private Marker marker;
  

    private boolean visibleNuevoEditar;
    private boolean editActive;
    private Ruta rutaSelected;
    private Planilla planilla;
    private List<Ruta> rutas;
    private DualListModel<Beneficiario> beneficiarios;
    private List<Monitor> monitorList;
    private List<Beneficiario> beneficiariosSource;
    private List<Beneficiario> beneficiariosTarget;
    
   
    private String componentesJson;
   
   
    private static final Map<String, String> WEEK_DAY = new HashMap<>();
    

    @PostConstruct
    public void init() {
        visibleNuevoEditar = false;
        editActive = false;
        rutaSelected = new Ruta();
        planilla=planillaService.find(Util.PLANILLA_DEFAULT);
        rutas = rutaService.findAll(); 
        monitorList=monitorService.findAll();
        beneficiariosSource = beneficiarioService.beneficiarioNotInAllRuta();
        beneficiariosTarget = new ArrayList<>();
        beneficiarios=new DualListModel<>(beneficiariosSource, beneficiariosTarget);
        draggableModel=new DefaultMapModel();
        fillWeekDay();
    }
    //methodos
     public void save() {
        try {
            if(beneficiarios.getTarget().isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe asignar Beneficiarios", ""));
                return;
            }
            Ruta ruta=rutaService.findRuta(rutaSelected.getMonitorid().getMonitorid(), rutaSelected.getDia());
            if (editActive) {
                if(ruta!=null && ruta.getRutaid().equals(rutaSelected.getRutaid())){
                    rutaService.editar(login.getUser(),rutaSelected,beneficiarios.getTarget());
                }else{
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ya existe una ruta creada, para el día y monitor", ""));
                    return ;
                }
            } else {
                 if(ruta!=null){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ya existe una ruta creada, para el día y monitor", ""));
                    return ;
                 }
                rutaSelected.setPlanillaid(planilla);
                rutaService.insert(login.getUser(),rutaSelected,beneficiarios.getTarget());
            }
            
            editActive = false;
            visibleNuevoEditar = false;
            util.showInfo("Registro realizado");
            rutas = rutaService.findAll();
            rutaSelected = new Ruta();
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    
     
     
    

    public void create() {
        visibleNuevoEditar = true;
        editActive = false;     
        rutaSelected = new Ruta();
        beneficiariosSource = beneficiarioService.beneficiarioNotInAllRuta();
        beneficiariosTarget = new ArrayList<>();
        beneficiarios=new DualListModel<>(beneficiariosSource, beneficiariosTarget);
    }

    public void edit(Ruta ruta) {
        visibleNuevoEditar = true;
        editActive = true;
      
        this.rutaSelected = ruta;
        beneficiariosTarget = beneficiarioService.beneficiarioInRuta(ruta);
        beneficiariosSource = beneficiarioService.beneficiarioNotInAllRuta();        
       beneficiarios=new DualListModel<>(beneficiariosSource, beneficiariosTarget);
       loadMarkers();
       
    }

    public void delete(Ruta ruta) {
        try {
            
            if(evaluacionService.existeRuta(ruta)){
                util.showError(Util.MSG_DELETE_FAIL);
                return;
            }
            
            rutaService.remove(ruta);
            log.error(": Se procede al eliminado fisico");         
            util.showInfo(Util.MSG_DELETE);
            rutas = rutaService.findAll();
        } catch (Exception e) {
          
                log.error(e.getMessage(), e);
                util.showError(Util.MSG_CRASH);
           
        }
    }
   
    private void loadMarkers(){
        if(draggableModel!=null && draggableModel.getMarkers()!=null){
            draggableModel.getMarkers().clear();
        }
        
         int i = 0;
         for(Beneficiario item: beneficiarios.getTarget()){
             LatLng latlon=new LatLng(item.getLat(),item.getLon());
             StringBuilder stb=new StringBuilder("Nro: ");
             stb.append(i+1);
             stb.append(" - Beneficiario: ");
             stb.append(item.getNombre());
             Marker marker= new Marker(latlon,stb.toString());
             draggableModel.addOverlay(marker);
            i++;
         };
        
    }
    
    //Eventos
   
    public void onTransfer(TransferEvent event) {
        log.info("event transfer generated");
        loadMarkers();
    }  
    
    public void fillWeekDay(){
        if(WEEK_DAY.isEmpty()){
            WEEK_DAY.put("2", "Lunes");
            WEEK_DAY.put("3", "Martes");
            WEEK_DAY.put("4", "Miercoles");
            WEEK_DAY.put("5", "Jueves");
            WEEK_DAY.put("6", "Viernes");
            WEEK_DAY.put("7", "Sabado");
            WEEK_DAY.put("1", "Domingo");
        }
    }
    
    public String getWeekDay(String dia) {
        return WEEK_DAY.get(dia);
    }
     
//    public void onSelect(SelectEvent<Beneficiario> event) {
//        //mostrar lo necesario
//    }
//     
   
    public void onUnselect(UnselectEvent event) {
       // event.getObject()
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected",null));
    }
     
    public void onReorder() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "List Reordered", null));
    }
    
    //Propiedades

    public String getComponentesJson() {
        return componentesJson;
    }

    public void setComponentesJson(String componentesJson) {
        this.componentesJson = componentesJson;
    }
    
    
    public List<Monitor> getMonitorList() {
        return monitorList;
    }

    public void setMonitorList(List<Monitor> monitorList) {
        this.monitorList = monitorList;
    }
    
    public DualListModel<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(DualListModel<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }
    
     public List<Ruta> getRutas() {
        return rutas;
    }

    public void setRutas(List<Ruta> rutas) {
        this.rutas = rutas;
    }
    

    public Ruta getRutaSelected() {
        return rutaSelected;
    }

    public void setRutaSelected(Ruta rutaSelected) {
        this.rutaSelected = rutaSelected;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public MapModel getDraggableModel() {
        return draggableModel;
    }

    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();     
    }

    

    public boolean isVisibleNuevoEditar() {
        return visibleNuevoEditar;
    }

    public void setVisibleNuevoEditar(boolean visibleNuevoEditar) {
        log.info("setVisibleNuevoEditar  "+visibleNuevoEditar);
        this.visibleNuevoEditar = visibleNuevoEditar;
    }

    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }

    
    
   

   

    
    
    
    
   
    
}
