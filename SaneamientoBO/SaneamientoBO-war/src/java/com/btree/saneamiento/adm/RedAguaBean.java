/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;


import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.log4j.Logger;

/**
 *
 * @author Diana Mejía
 */
@Named(value = "redAguaBean")
@ViewScoped
public class RedAguaBean implements Serializable {

    public static Logger log = Logger.getLogger(RedAguaBean.class);

    

    private String circuitosPaths;
    private String planosPaths;
    private String componentesJson;

    @PostConstruct
    public void init() {
        

        //circuitosHidrometricosSelected = circuitosHidrometricos;
        //mapasSelected = mapas;
        //fuentesAbastecimientoSelected = fuentesAbastecimiento;
        //tanquesAlmacenamientoSelected = tanquesAlmacenamiento;
        //puntosControlSelected = puntosControl;
        //aduccionesSelected = aducciones;
        //estacionesBombeoSelected = estacionesBombeo;
        //plantasTratamientoSelected = plantasTratamiento;
        
        
    }

    

    public String getCircuitosPaths() {
        return circuitosPaths;
    }

    public String getPlanosPaths() {
        String planos=planosPaths;
        return planosPaths;
    }

    public void setPlanosPaths(String planosPaths) {
        this.planosPaths = planosPaths;
    }

    public void setCircuitosPaths(String circuitosPaths) {
        this.circuitosPaths = circuitosPaths;
    }

    
    public String getComponentesJson() {
        return componentesJson;
    }

    public void setComponentesJson(String componentesJson) {
        this.componentesJson = componentesJson;
    }

   
}
