/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.adm;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.ln.BeneficiarioFacade;
import com.btree.saneamiento.ln.RutadetalleFacade;
import com.btree.saneamiento.ln.ServiciopeticionFacade;
import com.btree.saneamiento.seguridad.LoginBean;
import com.btree.saneamiento.util.Util;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author Diana Mejia
 */
@Named(value = "beneficiarioBean")
@ViewScoped
public class BeneficiarioBean implements Serializable {

    public static Logger log = Logger.getLogger(BeneficiarioBean.class);

@EJB
    private Util util;
     
    @EJB
    private BeneficiarioFacade beneficiarioService;
    
    
    @Inject
    private LoginBean login;
    
    private List<Beneficiario> beneficiarioList;

    
    private Beneficiario beneficiarioSelected;
    
    @EJB
    private RutadetalleFacade rutadetService;
    
    @EJB
    private ServiciopeticionFacade servService;
    
    private boolean visibleNuevoEditar;
    private boolean editActive;

    private MapModel draggableModel;

    private Marker marker;
    
    public static final LatLng MAP_CENTER=new LatLng(-17.7621609,-63.1743539);
    
    @PostConstruct
    public void init() {
         
        visibleNuevoEditar = false;
        editActive = false;
        beneficiarioSelected = new Beneficiario();      
        beneficiarioList=beneficiarioService.findAll();
        
        draggableModel = new DefaultMapModel();
         marker = new Marker(MAP_CENTER);
        draggableModel.addOverlay(marker);

        for (Marker premarker : draggableModel.getMarkers()) {
            premarker.setDraggable(true);
        }
    }
    
    
   //Metodos
    public void save() {
        try {
            if(beneficiarioSelected.getLat()==null){
                util.showError("Seleccione una ubicacion en el mapa");
                return;
            }
            if (editActive) {  
                beneficiarioSelected.setUlastupdate(login.getUser());
                beneficiarioService.edit(beneficiarioSelected);
            } else {               
               beneficiarioSelected.setUlastupdate(login.getUser());
                beneficiarioService.create(beneficiarioSelected);
            }
            
            editActive = false;
            visibleNuevoEditar = false;            
            beneficiarioList = beneficiarioService.findAll();
            beneficiarioSelected = new Beneficiario();
            util.showInfo("Registro realizado");
           
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void create() {
        visibleNuevoEditar = true;
        editActive = false; 
        beneficiarioSelected = new Beneficiario();
        beneficiarioList = beneficiarioService.findAll();
        marker.setLatlng(MAP_CENTER);    
    }

    public void edit(Beneficiario beneficiario) {
       visibleNuevoEditar = true;
       editActive = true;
       beneficiarioSelected = beneficiario;
       beneficiarioList = beneficiarioService.findAll();
        LatLng coord1 = new LatLng(beneficiarioSelected.getLat(), beneficiarioSelected.getLon());
        marker.setLatlng(coord1);    
       
    }
public void delete(Beneficiario beneficiario) {
        try {
            if(rutadetService.existeBeneficiario(beneficiario) || servService.existeBeneficiario(beneficiario)){
               util.showError(Util.MSG_DELETE_FAIL);
                return;
            }
            beneficiarioService.remove(beneficiario);
            log.error(": Se procede al eliminado fisico");         
            util.showInfo(Util.MSG_DELETE);
            beneficiarioList = beneficiarioService.findAll();
        } catch (Exception e) {
            
           
                log.error(e.getMessage(), e);
                util.showError(Util.MSG_CRASH);
          
        }
    }

 public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        util.showInfo("Marker Dragged - Lat:" + marker.getLatlng().getLat() + ", Lng:" + marker.getLatlng().getLng());
        beneficiarioSelected.setLat(marker.getLatlng().getLat());
        beneficiarioSelected.setLon(marker.getLatlng().getLng());
    }
   
    //Propiedades
public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public MapModel getDraggableModel() {
        return draggableModel;
    }

    public boolean isEditActive() {
        return editActive;
    }

    public void setEditActive(boolean editActive) {
        this.editActive = editActive;
    }
    
    

    public boolean isVisibleNuevoEditar() {
        return visibleNuevoEditar;
    }

    public void setVisibleNuevoEditar(boolean visibleNuevoEditar) {
        this.visibleNuevoEditar = visibleNuevoEditar;
    }

    public List<Beneficiario> getBeneficiarioList() {
        return beneficiarioList;
    }

    public void setBeneficiarioList(List<Beneficiario> beneficiarioList) {
        this.beneficiarioList = beneficiarioList;
    }

    public Beneficiario getBeneficiarioSelected() {
        return beneficiarioSelected;
    }

    public void setBeneficiarioSelected(Beneficiario beneficiarioSelected) {
        this.beneficiarioSelected = beneficiarioSelected;
    }

    
    
   
    
    

    
   

    

    

   

    

    
    
   
    
}
