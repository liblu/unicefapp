/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.servlets;

import com.btree.saneamiento.util.Util;
import static com.btree.saneamiento.util.Util.SEPARATOR;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yugard
 */
@WebServlet("/pdf/*")
public class PdfServlet extends HttpServlet {

   

    @Override
    protected void doGet(
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String requestedPdf = request.getPathInfo();
            log(requestedPdf);
            File pdf;
            if (requestedPdf == null || requestedPdf.equals("/")) {
                response.sendError(
                        HttpServletResponse.SC_NOT_FOUND);
                return;
            } else {
                String realPath = System.getProperties().getProperty("user.home") + SEPARATOR;
                requestedPdf = realPath + requestedPdf.substring(1);
                pdf = new File(requestedPdf);
                if (!pdf.exists()) {
                    response.sendError(
                            HttpServletResponse.SC_NOT_FOUND);
                    return;
                }
            }
            response.reset();
            response.setContentType("application/pdf");
            response.setHeader("Content-Length",
                    String.valueOf(pdf.length()));
            Files.copy(pdf.toPath(), response.getOutputStream());
        } catch (Exception ex) {
            log(ex.getMessage(), ex);
        }
    }
}
