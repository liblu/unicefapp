/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.servlets;

import com.btree.saneamiento.util.Util;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.ejb.EJB;

@WebServlet("/image/*")
public class ImageServlet extends HttpServlet {

    @EJB
    private Util util;

    @Override
    protected void doGet(
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String requestedImage = request.getPathInfo();
        log(requestedImage);
        File image;
        if (requestedImage == null || requestedImage.equals("/")) {
            response.sendError(
                    HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        } else {
            requestedImage = util.getPathUserHome() + requestedImage.substring(1);
            image = new File(requestedImage);
            if (!image.exists()) {

                response.sendError(
                        HttpServletResponse.SC_NOT_FOUND); // 404.
                return;
            }
        }

        response.reset();
        response.setContentType("image/jpg");
        response.setHeader("Content-Length",
                String.valueOf(image.length()));
        Files.copy(image.toPath(), response.getOutputStream());
    }

}
