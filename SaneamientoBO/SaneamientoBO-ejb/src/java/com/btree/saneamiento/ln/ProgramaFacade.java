/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.util.ParametersQuery;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class ProgramaFacade extends AbstractFacade<Programa> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProgramaFacade() {
        super(Programa.class);
    }
    
    public Programa obtainProgram(String programaurl) throws Exception {
        ParametersQuery params = ParametersQuery.getParametersQuery();
        params.put("programaurl", programaurl);
        return findQuery("select t from Programa t where UPPER(t.url) = UPPER(:programaurl)", params);
    }
    
    

    public List<Programa> listarProgramas() throws Exception {
        return em.createQuery("select p from Programa p where p.visible = :est ").setParameter("est", true).getResultList();
    }
   
    public List<Programa> programaInPerfilById(short perfil){    
        return em.createQuery("select p FROM Programa p INNER JOIN Perfilprograma pp ON p.programaid = pp.perfilprogramaPK.programaid  WHERE pp.perfil.perfilid=:perfil AND p.visible = :est ").setParameter("est", true).setParameter("perfil", perfil).getResultList();
     }
    
     public List<Programa> programaInPerfil(Perfil perfil){    
        return em.createQuery("select p FROM Programa p INNER JOIN Perfilprograma pp ON p.programaid = pp.perfilprogramaPK.programaid  WHERE pp.perfil=:perfil AND p.visible = :est ").setParameter("est", true).setParameter("perfil", perfil).getResultList();
     }
    
    public List<Programa> programaNotInPerfil(Perfil perfil){
//        
        return em.createQuery("SELECT p FROM Programa p WHERE p.programaid NOT IN (SELECT rd.perfilprogramaPK.programaid FROM Perfilprograma rd WHERE rd.perfil =:perfil )").setParameter("perfil", perfil).getResultList();
    }
}
