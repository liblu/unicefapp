/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.util.Util;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class ServicioFacade extends AbstractFacade<Servicio> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServicioFacade() {
        super(Servicio.class);
    }
    
    @Override
    public List<Servicio> findAll(){
            return  em.createQuery("SELECT r FROM Servicio r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
    
    }
    
     @Override
    public void create(Servicio entity) {
        entity.setServicioid(getNextID());
        entity.setDtcreated(new Date());
        entity.setDtlastupdate(new Date());
        entity.setEstado(Util.HABILITADO);
        super.create(entity);
    }
    
     @Override
    public void edit(Servicio entity) {
       
        entity.setDtlastupdate(new Date());
        
        super.edit(entity);
    }
    
    
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.servicioid) from Servicio s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
}
