/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Menu;
import com.btree.saneamiento.bd.Perfilprograma;
import com.btree.saneamiento.bd.Permisos;
import com.btree.saneamiento.bd.PermisosPK;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.util.ParametersQuery;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class PermisosFacade extends AbstractFacade<Permisos> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;
     @EJB
    private UsuarioFacade usuarioService;
    
    public static final String ACTIVO = "H";
    public static final String INACTIVO = "I";
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PermisosFacade() {
        super(Permisos.class);
    }
    
    public List<Permisos> listarPermisos(Usuario usuario) throws Exception {
        ParametersQuery params = ParametersQuery.getParametersQuery();
        params.put("usuario", usuario);
        params.put("estado", ACTIVO);
        return findAllQuery("select t from Permisos t where t.usuario = :usuario and t.estado = :estado", params);
    }
    
     public List<Permisos> listarPermisosMenu(Usuario usuario,Menu menu) throws Exception {
        ParametersQuery params = ParametersQuery.getParametersQuery();
        params.put("usuario", usuario);
        params.put("estado", ACTIVO);
        params.put("menu", menu);
        return findAllQuery("select t from Permisos t where t.usuario = :usuario and t.estado = :estado and t.programa.menuid = :menu", params);
    }
     
     public boolean tienePermiso(Usuario usuario, Programa programa) throws Exception {
        ParametersQuery params = ParametersQuery.getParametersQuery();
        params.put("usuario", usuario);
        params.put("programa", programa);
        params.put("estado", ACTIVO);
        return findQuery("select t from Permisos t where t.usuario = :usuario and t.programa = :programa and t.estado = :estado ", params) != null;
    }
     
     public void enablePermiso(Perfilprograma perfilPrograma) throws Exception {
        //insert permiso mientras que el usuario tenga el mismo perfil y no tenga el programa
        //listar todos los usuarios que no tengan el programa pero que si tengan el perfil
        List<Usuario> usuarios = usuarioService.findAllByPerfil(perfilPrograma.getPerfil());
        for (Usuario usuario : usuarios) {
            System.out.println(usuario.getUsuario()+" "+perfilPrograma.getPrograma().getDescripcion());
            PermisosPK pk = new PermisosPK(perfilPrograma.getPrograma().getProgramaid(), usuario.getUsuarioid());
            Permisos permisoAux = find(pk);
            Permisos permiso;

            permiso = new Permisos(pk);
            permiso.setEstado(ACTIVO);
            permiso.setPrograma(perfilPrograma.getPrograma());
            permiso.setUsuario(usuario);

            if (permisoAux == null) {
                create(permiso);
            } else {
                edit(permiso);
            }
        }
    }
}
