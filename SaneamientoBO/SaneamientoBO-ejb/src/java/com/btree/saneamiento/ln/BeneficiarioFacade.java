/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.util.Util;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class BeneficiarioFacade extends AbstractFacade<Beneficiario> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BeneficiarioFacade() {
        super(Beneficiario.class);
    }
    
    @Override
    public void create(Beneficiario entity) {
         entity.setBeneficiarioid(getNextID());
     entity.setDtcreated(new Date());
        entity.setDtlastupdate(new Date());
        entity.setEstado(Util.HABILITADO);
        super.create(entity);
    }
    
    @Override
    public void edit(Beneficiario entity) {    
        entity.setDtlastupdate(new Date());       
        super.edit(entity);
    }
    
     @Override
    public List<Beneficiario> findAll() {
        return  em.createQuery("SELECT r FROM Beneficiario r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
    }
    
    public List<Beneficiario> beneficiarioNotInRuta(Ruta ruta){
//        
        return em.createQuery("SELECT b FROM Beneficiario b WHERE b.beneficiarioid NOT IN (SELECT rd.beneficiarioid.beneficiarioid FROM Rutadetalle rd INNER JOIN Ruta r ON rd.rutaid.rutaid=r.rutaid WHERE r.rutaid=:rutaval )").setParameter("rutaval", ruta.getRutaid()).getResultList();
    }
    
    public List<Beneficiario> beneficiarioNotInAllRuta(){
        return em.createQuery("SELECT b FROM Beneficiario b WHERE b.beneficiarioid NOT IN (SELECT rd.beneficiarioid.beneficiarioid FROM Rutadetalle rd INNER JOIN Ruta r ON rd.rutaid.rutaid=r.rutaid AND rd.estado='H')").getResultList();
    }
    
    
    public List<Beneficiario> beneficiarioInRuta(Ruta ruta){
       
       return  em.createQuery("SELECT b FROM  Beneficiario b INNER JOIN Rutadetalle rd ON b.beneficiarioid=rd.beneficiarioid.beneficiarioid WHERE rd.rutaid=:ruta AND rd.estado='H' ORDER BY rd.orden ").setParameter("ruta", ruta).getResultList();
   
    }
   
   public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.beneficiarioid) from Beneficiario s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
}
