/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.bd.Rutadetalle;
import com.btree.saneamiento.util.Util;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class RutaFacade extends AbstractFacade<Ruta> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;
    
    @EJB
    private RutadetalleFacade rutaDetalleService;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RutaFacade() {
        super(Ruta.class);
    }
    
    @Override
    public void create(Ruta entity) {
        entity.setRutaid(getNextID());
        entity.setDtcreated(new Date());
        entity.setDtlastupdate(new Date());
        entity.setEstado(Util.HABILITADO);
        super.create(entity);
    }
    
 
    public void insert(String user, Ruta entity, List<Beneficiario> beneficiarios){
        Date createDate=new Date();
        entity.setUlastupdate(user);
        create(entity);
        for(int i=0;i<beneficiarios.size();i++){
            Rutadetalle detail= new Rutadetalle();
            Beneficiario item=beneficiarios.get(i);            
            detail.setOrden(i+1);            
            detail.setBeneficiarioid(item);
            detail.setRutaid(entity);
            detail.setDtcreated(createDate);
            detail.setDtlastupdate(createDate);
            detail.setUlastupdate(user);
            detail.setEstado(Util.HABILITADO);
            rutaDetalleService.create(detail);
        }  
    }
    
    public void editar(String user, Ruta entity, List<Beneficiario> beneficiarios){
       
        Date createDate=new Date();
        entity.setUlastupdate(user);
        entity.setDtlastupdate(createDate);
        edit(entity);
        
        rutaDetalleService.disableAll(entity);
        for(int i=0;i<beneficiarios.size();i++){           
            Rutadetalle detail;
            Beneficiario item=beneficiarios.get(i);
            detail=rutaDetalleService.findByBeneficiario(item,entity);
            if(detail!=null){
                detail.setOrden(i+1);
                detail.setDtlastupdate(createDate);
                detail.setUlastupdate(user);
                detail.setEstado(Util.HABILITADO);
                rutaDetalleService.edit(detail);
            }else{
            detail= new Rutadetalle();            
            detail.setOrden(i+1);            
            detail.setBeneficiarioid(item);
            detail.setRutaid(entity);
            detail.setDtcreated(createDate);
            detail.setDtlastupdate(createDate);
            detail.setUlastupdate(user);
            detail.setEstado(Util.HABILITADO);
            rutaDetalleService.create(detail);
            }
        }  
    }
    
    
    @Override
    public List<Ruta> findAll() {
        return  em.createQuery("SELECT r FROM Ruta r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
    }
    
    public Ruta findRuta(Long monitorId, String dia)  {
        
       try{
        return (Ruta)em.createQuery("SELECT r FROM Ruta r WHERE r.monitorid.monitorid=:monitor AND r.dia=:dia AND r.estado=:estado").setParameter("monitor", monitorId).setParameter("dia", dia).setParameter("estado", Util.HABILITADO).getSingleResult();
       }catch(Exception e){
           return null;
       }
              
    }
    
    public boolean existeMonitor(Monitor monitor){
        return em.createQuery("SELECT r FROM Ruta r WHERE r.monitorid = :monitor").setParameter("monitor", monitor).getResultList().size()>0;
    }
    
    
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.rutaid) from Ruta s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
}
