/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Perfilprograma;
import com.btree.saneamiento.bd.PerfilprogramaPK;
import com.btree.saneamiento.bd.Programa;
import com.btree.saneamiento.util.Util;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class PerfilFacade extends AbstractFacade<Perfil> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;
    
     @EJB
    private PerfilprogramaFacade perfilProgramaService;
     @EJB
     private PermisosFacade permisoService;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PerfilFacade() {
        super(Perfil.class);
    }
    
    public void edit(Perfil entity, List<Programa> programas) throws Exception{
        super.edit(entity);
        perfilProgramaService.disableAll(entity);
        for (Programa programa : programas) {
             
            PerfilprogramaPK perfilProgramaPk = new PerfilprogramaPK(entity.getPerfilid(), programa.getProgramaid());
            Perfilprograma  perfilPrograma=perfilProgramaService.find(perfilProgramaPk);
             if (perfilPrograma == null) {
                perfilPrograma = new Perfilprograma();
                perfilPrograma.setPerfilprogramaPK(perfilProgramaPk);
                perfilPrograma.setEstado(Util.HABILITADO);
                perfilPrograma.setPrograma(programa);
                perfilPrograma.setPerfil(entity);
                perfilProgramaService.create(perfilPrograma);
            } else {
                perfilPrograma.setEstado(Util.HABILITADO);
                perfilProgramaService.edit(perfilPrograma);
            }
             permisoService.enablePermiso(perfilPrograma);
        }
    }
    
    public void create(Perfil entity, List<Programa> programas){
        entity.setPerfilid(getNextID());
        super.create(entity);
        em.flush();
        for (Programa programa : programas) {
            PerfilprogramaPK perfilProgramaPk = new PerfilprogramaPK(entity.getPerfilid(), programa.getProgramaid());
            Perfilprograma perfilPrograma = new Perfilprograma();
            perfilPrograma.setPerfilprogramaPK(perfilProgramaPk);
            perfilPrograma.setPerfil(entity);
            perfilPrograma.setPrograma(programa);
            perfilPrograma.setEstado(Util.HABILITADO);
            perfilProgramaService.create(perfilPrograma);
        }
    }
    
    @Override
    public List<Perfil> findAll(){
    return  em.createQuery("SELECT r FROM Perfil r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
    }
    
    public Short getNextID() {
        Short codigo = (Short) em.createQuery("select max(s.perfilid) from Perfil s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (short) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
}
