/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Evaluacion;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.bd.Rutadetalle;
import com.btree.saneamiento.util.Util;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class EvaluacionFacade extends AbstractFacade<Evaluacion> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EvaluacionFacade() {
        super(Evaluacion.class);
    }
    
    @Override
    public void create(Evaluacion entity){
        Date date=new Date();
        
        entity.setDtcreated(date);
        entity.setDtlastupdate(date);
        entity.setEvaluacionid(getNextID());
        super.create(entity);
    }
    
    public Evaluacion createByRutadetalle(Rutadetalle ruta)  throws Exception{
        Date date=new Date();
        Evaluacion evaluacion=findByRutadetalle(ruta.getRutadetid());
        if(evaluacion==null){
        
            evaluacion=new Evaluacion();
            evaluacion.setRutadetid(ruta);
            evaluacion.setPlanillaid(ruta.getRutaid().getPlanillaid());
            evaluacion.setEstado(Util.PENDIENTE);
            evaluacion.setDtcreated(date);
            evaluacion.setDtlastupdate(date);
            evaluacion.setUlastupdate(ruta.getUlastupdate());
            evaluacion.setEvaluacionid(getNextID());
            super.create(evaluacion);
        }
        return evaluacion;
    }
    
    @Override
    public void edit(Evaluacion entity){
        entity.setDtlastupdate(new Date());
        entity.setEstado(Util.COMPLETADO);
        super.edit(entity);
    }
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("SELECT max(m.evaluacionid) FROM Evaluacion m ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
    public boolean existeRuta(Ruta ruta){
        return em.createQuery("SELECT r FROM Evaluacion r WHERE r.rutadetid.rutaid = :ruta").setParameter("ruta", ruta).getResultList().size()>0;
    }

    public List<Evaluacion> findTransacciones(Date fechaInicial, Date fechaFinal, Long beneficiario, Long monitor, String estadoSelected, int first, int pageSize) {
          CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Evaluacion> cq = cb.createQuery(Evaluacion.class);
        Root<Evaluacion> e = cq.from(Evaluacion.class);

        List<Predicate> predicates = new ArrayList<>();

        prepareQuery(cb, e, predicates,  fechaInicial,  fechaFinal,  beneficiario,  monitor,  estadoSelected);

        cq.where(predicates.toArray(new Predicate[]{}));
        cq.orderBy(cb.desc(e.get("dtcreated")));
        Query queryQ = em.createQuery(cq);
        if (first > 0) {
            queryQ.setFirstResult(first);
        }
        if (pageSize > 0) {
            queryQ.setMaxResults(pageSize);
        }
        return queryQ.getResultList();
    }

    public long countTransacciones(Date fechaInicial, Date fechaFinal, Long beneficiario, Long monitor, String estadoSelected) {
          CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Evaluacion> e = cq.from(Evaluacion.class);

        cq.select(cb.count(e));
        List<Predicate> predicates = new ArrayList<>();

        prepareQuery(cb, e, predicates,  fechaInicial,  fechaFinal,  beneficiario,  monitor,  estadoSelected);

        cq.where(predicates.toArray(new Predicate[]{}));

        Query queryQ = em.createQuery(cq);

        return (long) queryQ.getSingleResult();
    }
    
    
    private void prepareQuery(CriteriaBuilder cb, Root<Evaluacion> e, List<Predicate> predicates, Date fechaInicio, Date fechaFin, Long beneficiarioSelected, Long monitorSelected, String estadoSelected) {
        if (fechaInicio != null) {
            Predicate pred = cb.greaterThanOrEqualTo(
                    e.<Timestamp>get("dtcreated"),
                    new Timestamp(fechaInicio.getTime()));
            predicates.add(pred);
        }
        if (fechaFin != null) {
            Predicate pred = cb.lessThanOrEqualTo(
                    e.<Timestamp>get("dtcreated"),
                    new Timestamp(fechaFin.getTime()));
            predicates.add(pred);
        }
        
        if (monitorSelected != null ) {
            Predicate pred = cb.equal(e.get("rutadetid").get("rutaid").get("monitorid").get("monitorid"), monitorSelected);
            predicates.add(pred);
        }
        
        if(beneficiarioSelected!=null){
            Predicate pred = cb.equal(e.get("rutadetid").get("beneficiarioid").get("beneficiarioid"), beneficiarioSelected);
            predicates.add(pred);
        }
        
        if(estadoSelected!=null && !estadoSelected.isEmpty()){
            Predicate pred = cb.equal(e.get("estado"), estadoSelected);
            predicates.add(pred);
        }
    }

    public Evaluacion findByRutadetalle(Long rutadetid)  {
        try{
      
            return (Evaluacion)em.createQuery("SELECT e FROM Evaluacion e WHERE e.rutadetid.rutadetid=:rutadetid").setParameter("rutadetid", rutadetid).getSingleResult();
        }catch(Exception e){
            return null;
        }
       
    }
  
    
}
