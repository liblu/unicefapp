/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Perfilprograma;
import com.btree.saneamiento.bd.Permisos;
import com.btree.saneamiento.bd.PermisosPK;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.util.AppException;
import com.btree.saneamiento.util.BCrypt;
import com.btree.saneamiento.util.ParametersQuery;
import com.btree.saneamiento.util.Util;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    @EJB
    PermisosFacade permisoService;
    @EJB
    PerfilFacade perfilService;
    @EJB
    PerfilprogramaFacade perfilProgramaService;
    @EJB
    ProgramaFacade programaService;

    @Override
    public List<Usuario> findAll() {
       return  em.createQuery("SELECT u FROM Usuario u WHERE u.estado='H'").getResultList();
    }
    
    
  

    public String generateID() {
        String uniqueID = UUID.randomUUID().toString().replace("-", "").substring(0, 20);

        return uniqueID;
    }
    
    public boolean existUser(String login) throws Exception {
        ParametersQuery parameters = ParametersQuery.getParametersQuery();
        parameters.put("login", login);
        String query = "select T from Usuario T where T.usuario = :login  and T.estado = 'H'";
        return findQuery(query, parameters) != null;
    }

    public Usuario getUser(String login) throws Exception {
        ParametersQuery parameters = ParametersQuery.getParametersQuery();
        parameters.put("login", login);
        String query = "select T from usuario T where T.usuario = :login and T.estado = 'H'";
        return findQuery(query, parameters);
    }

 
    public void creat(Usuario entity) throws Exception {
        if (!existUser(entity.getUsuario())) {
            entity.setUsuario(generateID());
           
            entity.setFechaalta(new Date());
            entity.setContrasenia(BCrypt.hashpw(entity.getContrasenia(), BCrypt.gensalt(12)));
            super.create(entity);
        } else {
            throw new AppException("Ya existe el usuario");
        }
    }
    
   
    public void editar(Usuario entity) throws Exception {
        
            Perfil perfil=perfilService.find(entity.getPerfilid());
            Collection<Permisos> permisos = permisoService.listarPermisos(entity);
            for (Permisos permiso : permisos) {
                permiso.setEstado(Util.INHABILITADO);               
                permisoService.edit(permiso);
            }
            
            
            
            for (Perfilprograma programa : perfil.getPerfilprogramaCollection()) {
                PermisosPK permisoPk = new PermisosPK(programa.getPrograma().getProgramaid(), entity.getUsuarioid());
                Permisos permiso = permisoService.find(permisoPk);
                if (permiso == null) {
                    permiso = new Permisos();
                    permiso.setPermisosPK(permisoPk);
                    permiso.setPrograma(programa.getPrograma());
                    permiso.setUsuario(entity);
                    permiso.setEstado(Util.HABILITADO);
                    permisoService.create(permiso);
                    entity.getPermisosCollection().add(permiso);
                } else {
                    permiso.setEstado(Util.HABILITADO);
                    permisoService.edit(permiso);
                }
            }
            
            super.edit(entity);
            
            
      
    }

    public void crear(Usuario entity) throws Exception {
        if (!existUser(entity.getUsuario())) {
            Date dateCreated=new Date();
            entity.setUsuarioid(getNextID());
            entity.setFechaalta(dateCreated);
            entity.setContrasenia(BCrypt.hashpw(entity.getContrasenia(), BCrypt.gensalt(12)));
            create(entity);
            Perfil perfil=perfilService.find(entity.getPerfilid());
            for (Perfilprograma programa : perfil.getPerfilprogramaCollection()) {
                PermisosPK permisoPk = new PermisosPK(programa.getPrograma().getProgramaid(), entity.getUsuarioid());
                Permisos permiso = new Permisos();
                permiso.setPermisosPK(permisoPk);
                permiso.setUsuario(entity);
                permiso.setEstado(Util.HABILITADO);
                permiso.setPrograma(programa.getPrograma());
                permisoService.create(permiso);
                entity.getPermisosCollection().add(permiso);
                super.edit(entity);
            }
            
        } else {
            throw new AppException("Ya existe el usuario");
        }
    }

    @Override
    public void remove(Usuario entity) {
        entity.setEstado(Util.INHABILITADO);
        super.edit(entity);
    }
    
    
    public Usuario loginUser(String user, String password) throws Exception {
        ParametersQuery p = ParametersQuery.getParametersQuery();
        p.put("usuario", user);
        Usuario usuario = findQuery("Select u from Usuario u where u.estado='H' and u.usuario = :usuario", p);
        if (usuario == null) {
            throw new AppException("Usuario no existe o está inactivo");
        }
        if(usuario.getUsuario().equals("admin")&& (usuario.getContrasenia()==null || usuario.getContrasenia().isEmpty())){
           
            usuario.setContrasenia(BCrypt.hashpw(password, BCrypt.gensalt(12)));
            super.edit(usuario);
        }
        // return usuario;
            if (BCrypt.checkpw(password, usuario.getContrasenia())) {
                return usuario;
            } else {
                throw new AppException("Usuario y/o Password Incorrecto");
            }
        
    }
    
    public void changePassword(String user, String passwordActual, String passwordNuevo) throws Exception {
        Usuario usuario = loginUser(user, passwordActual);
        usuario.setContrasenia(passwordNuevo);
        editPassword(usuario);
    }

    public void editPassword(Usuario entity) throws Exception {
        entity.setFechaalta(new Date());
        entity.setContrasenia(BCrypt.hashpw(entity.getContrasenia(), BCrypt.gensalt(12)));
        super.edit(entity);
    }
    
    public boolean existePerfil(Perfil perfil){
      
       
        List<Usuario> user= em.createQuery("select u from Usuario u where u.perfilid=:perfil").setParameter("perfil", perfil.getPerfilid()).getResultList();
        
        return user.size()>0;
    }
    
    public List<Usuario> findAllByPerfil(Perfil perfil) throws Exception {
        return em.createQuery("select t from Usuario t where t.perfilid = :perfil").setParameter("perfil", perfil.getPerfilid()).getResultList();
    }

  public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.usuarioid) from Usuario s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }

    public List<Usuario> findByPermisoMovil() {
        return em.createQuery("SELECT u from Usuario u WHERE u.usuarioid IN (SELECT p.usuario.usuarioid FROM Permisos p WHERE  p.programa.programaid=:permiso)").setParameter("permiso", Util.PERMISO_APP).getResultList();
    }
}
