/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Ruta;
import com.btree.saneamiento.bd.Rutadetalle;
import com.btree.saneamiento.util.Util;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class RutadetalleFacade extends AbstractFacade<Rutadetalle> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RutadetalleFacade() {
        super(Rutadetalle.class);
    }
    
    @Override
    public void create(Rutadetalle entity){
        entity.setRutadetid(getNextID());
        super.create(entity);
    }
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.rutadetid) from Rutadetalle s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
    public boolean existeBeneficiario(Beneficiario beneficiario){
        return em.createQuery("SELECT r FROM Rutadetalle r WHERE r.beneficiarioid = :benef").setParameter("benef", beneficiario).getResultList().size()>0;
    }

    public Rutadetalle findByBeneficiario(Beneficiario item, Ruta ruta) {
        try{
            return (Rutadetalle) em.createQuery("SELECT rd FROM Rutadetalle rd WHERE rd.beneficiarioid=:beneficiario AND rd.rutaid=:ruta").setParameter("ruta", ruta).setParameter("beneficiario", item).getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
          
        }
        return null;
    }

    public void removeByRuta(Ruta ruta){
        em.createQuery("DELETE FROM Rutadetalle rd WHERE rd.rutaid=:ruta ").setParameter("ruta", ruta);
    }

    public void disableAll(Ruta ruta) {
        List<Rutadetalle> lista= findByRuta(ruta);
        for(Rutadetalle item:lista){
            item.setEstado(Util.INHABILITADO);
            edit(item);
        }
    }

    private List<Rutadetalle> findByRuta(Ruta ruta) {
        return em.createQuery("SELECT rd FROM Rutadetalle rd INNER JOIN Ruta r ON rd.rutaid.rutaid=r.rutaid WHERE rd.estado=:estadoval AND r=:rutaval").setParameter("estadoval", Util.HABILITADO).setParameter("rutaval", ruta).getResultList();
    }
    
   
    
}
