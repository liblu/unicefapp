/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.util.ParametersQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Diana Mejía
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    protected T findQuery(String sql, ParametersQuery p) throws Exception {
        Map<String, Object> parametros = p != null ? p.getParametros() : null;
        try {
            Query query = getEntityManager().createQuery(sql);
            if (parametros != null) {
                List<String> keys = new ArrayList<>(parametros.keySet());
                for (String key : keys) {
                    query.setParameter(key, parametros.get(key));
                }
            }
            query.setMaxResults(1);
            return (T) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            throw e;
        }
    }
    
     protected List<T> findAllQuery(String sql, ParametersQuery p) throws Exception {
        Map<String, Object> parametros = p != null ? p.getParametros() : null;
        try {
            Query query = getEntityManager().createQuery(sql);
            if (parametros != null) {
                List<String> keys = new ArrayList<>(parametros.keySet());
                for (String key : keys) {
                    query.setParameter(key, parametros.get(key));
                }
            }
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }
    
}
