/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Monitor;
import com.btree.saneamiento.bd.Usuario;
import com.btree.saneamiento.util.AppException;
import com.btree.saneamiento.util.ParametersQuery;
import com.btree.saneamiento.util.Util;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class MonitorFacade extends AbstractFacade<Monitor> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MonitorFacade() {
        super(Monitor.class);
    }
    
    public Monitor findByUsuario(Usuario usuario) throws Exception{
      
        ParametersQuery p = ParametersQuery.getParametersQuery();
        p.put("user", usuario);
        
        Monitor item= findQuery("SELECT m FROM Monitor m WHERE m.estado='H' AND m.usuarioid=:user ",p);
        if(item==null){
            throw new AppException("Usuario no corresponde a un Monitor o está inactivo");
        }
        return item;
        
    }
    
    public boolean existeUsuario(Usuario usuario){
        return em.createQuery("SELECT m FROM Monitor m WHERE m.usuarioid=:user").setParameter("user", usuario).getResultList().size()>0;
    }
    
    public Monitor findByUser(Usuario usuario) {
      try{     
       
        return (Monitor) em.createQuery("SELECT m FROM Monitor m WHERE m.estado='H' AND m.usuarioid=:user ").setParameter("user", usuario).getSingleResult();
      }catch(Exception e){
          return null;
      }
    }
    
    @Override
    public List<Monitor> findAll(){
       return em.createQuery("SELECT m FROM Monitor m WHERE m.estado='H'").getResultList();
    }
    
    @Override
    public void create(Monitor monitor){
            Date dateCreated=new Date();
            monitor.setMonitorid(getNextID());
            monitor.setDtcreated(dateCreated);
            monitor.setDtlastupdate(dateCreated);
            monitor.setEstado(Util.HABILITADO);
            super.create(monitor);
           
    }
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("SELECT max(m.monitorid) FROM Monitor m ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
    
}
