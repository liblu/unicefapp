/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Planilla;
import com.btree.saneamiento.util.Util;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class PlanillaFacade extends AbstractFacade<Planilla> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlanillaFacade() {
        super(Planilla.class);
    }
    
     @Override
    public List<Planilla> findAll() {
        return  em.createQuery("SELECT r FROM Planilla r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
    }
    
}
