/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Pregunta;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class PreguntaFacade extends AbstractFacade<Pregunta> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PreguntaFacade() {
        super(Pregunta.class);
    }
    
//      @Override
//    public List<Pregunta> findAll() {
//        return  em.createQuery("SELECT r FROM Pregunta r WHERE r.estado=:estado").setParameter("estado", Util.HABILITADO).getResultList();
//    }
    
     public List<Pregunta> findByPlanilla(Long planillaId) {
        return  em.createQuery("SELECT r FROM Pregunta r WHERE r.planillaid.planillaid=:planilla ").setParameter("planilla", planillaId).getResultList();
    }
}
