/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Respuesta;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class RespuestaFacade extends AbstractFacade<Respuesta> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RespuestaFacade() {
        super(Respuesta.class);
    }
    
    public List<Respuesta> respuestasByEvaluacion(Long evaluacionid){
        return em.createQuery("SELECT r FROM Respuesta r WHERE r.evaluacionid.evaluacionid=:eval ORDER BY r.preguntaid").setParameter("eval", evaluacionid).getResultList();
    }

    @Override
    public void create(Respuesta entity) {
        Date date=new Date();
        
        entity.setDtcreated(date);
        entity.setDtlastupdate(date);
        entity.setRespuestaid(getNextID());
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Long getNextID() {
        Long codigo = (Long) em.createQuery("SELECT max(m.respuestaid) FROM Respuesta m ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }

    public Respuesta findByPreguntaEvaluacion(Long evaluacionid, Long preguntaid) {
        try{
            return (Respuesta) em.createQuery("SELECT r FROM Respuesta r WHERE r.evaluacionid.evaluacionid=:eval and r.preguntaid.preguntaid=:preg").setParameter("eval", evaluacionid).setParameter("preg", preguntaid).getSingleResult();
        }catch(Exception e){
            return null;
        }
    }
    
}
