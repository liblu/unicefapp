/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Beneficiario;
import com.btree.saneamiento.bd.Servicio;
import com.btree.saneamiento.bd.Serviciopeticion;
import com.btree.saneamiento.util.Util;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class ServiciopeticionFacade extends AbstractFacade<Serviciopeticion> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServiciopeticionFacade() {
        super(Serviciopeticion.class);
    }
    
   
    
    @Override
    public void create(Serviciopeticion entity) {
        entity.setServiciopeticionid(getNextID());
        entity.setDtcreated(new Date());
        entity.setDtlastupdate(new Date());
        entity.setEstado(Util.PENDIENTE);
        super.create(entity);
    }
    
     public Long getNextID() {
        Long codigo = (Long) em.createQuery("select max(s.serviciopeticionid) from Serviciopeticion s ").getSingleResult();
        
        if (codigo == null) {
            codigo = (long) 1;
        } else {
            codigo++;
        }
        return codigo;
    }
     
    public List<Serviciopeticion> findTransacciones(Date fechaInicial, Date fechaFinal, Long beneficiarioSelected, Long servicioSelected, String estadoSelected,int first, int pageSize) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Serviciopeticion> cq = cb.createQuery(Serviciopeticion.class);
        Root<Serviciopeticion> e = cq.from(Serviciopeticion.class);

        List<Predicate> predicates = new ArrayList<>();

        prepareQuery(cb, e, predicates,  fechaInicial,  fechaFinal,  beneficiarioSelected,  servicioSelected,  estadoSelected);

        cq.where(predicates.toArray(new Predicate[]{}));
        cq.orderBy(cb.desc(e.get("dtcreated")));
        Query queryQ = em.createQuery(cq);
        if (first > 0) {
            queryQ.setFirstResult(first);
        }
        if (pageSize > 0) {
            queryQ.setMaxResults(pageSize);
        }
        return queryQ.getResultList();
        
    }

    public long countTransacciones(Date fechaInicial, Date fechaFinal, Long beneficiarioSelected, Long servicioSelected, String estadoSelected) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Serviciopeticion> e = cq.from(Serviciopeticion.class);

        cq.select(cb.count(e));
        List<Predicate> predicates = new ArrayList<>();

        prepareQuery(cb, e, predicates,  fechaInicial,  fechaFinal,  beneficiarioSelected,  servicioSelected,  estadoSelected);

        cq.where(predicates.toArray(new Predicate[]{}));

        Query queryQ = em.createQuery(cq);

        return (long) queryQ.getSingleResult();
    }
    

    private void prepareQuery(CriteriaBuilder cb, Root<Serviciopeticion> e, List<Predicate> predicates, Date fechaInicio, Date fechaFin, Long beneficiarioSelected, Long servicioSelected, String estadoSelected) {
        if (fechaInicio != null) {
            Predicate pred = cb.greaterThanOrEqualTo(
                    e.<Timestamp>get("dtcreated"),
                    new Timestamp(fechaInicio.getTime()));
            predicates.add(pred);
        }
        if (fechaFin != null) {
            Predicate pred = cb.lessThanOrEqualTo(
                    e.<Timestamp>get("dtcreated"),
                    new Timestamp(fechaFin.getTime()));
            predicates.add(pred);
        }
        
        if (servicioSelected != null ) {
            Predicate pred = cb.equal(e.get("servicioid").get("servicioid"), servicioSelected);
            predicates.add(pred);
        }
        
        if(beneficiarioSelected!=null){
            Predicate pred = cb.equal(e.get("beneficiarioid").get("beneficiarioid"), beneficiarioSelected);
            predicates.add(pred);
        }
        
        if(estadoSelected!=null && !estadoSelected.isEmpty()){
            Predicate pred = cb.equal(e.get("estado"), estadoSelected);
            predicates.add(pred);
        }
    }
    
    
     public boolean existeBeneficiario(Beneficiario beneficiario){
        return em.createQuery("SELECT r FROM Serviciopeticion r WHERE r.beneficiarioid = :benef").setParameter("benef", beneficiario).getResultList().size()>0;
    }
     
      public boolean existeServicio(Servicio servicio){
        return em.createQuery("SELECT r FROM Serviciopeticion r WHERE r.servicioid = :benef").setParameter("benef", servicio).getResultList().size()>0;
    }
    
}
