/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.ln;

import com.btree.saneamiento.bd.Perfil;
import com.btree.saneamiento.bd.Perfilprograma;
import com.btree.saneamiento.util.Util;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diana Mejía
 */
@Stateless
public class PerfilprogramaFacade extends AbstractFacade<Perfilprograma> {

    @PersistenceContext(unitName = "SigmoPeriagua-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PerfilprogramaFacade() {
        super(Perfilprograma.class);
    }
    
    public List<Perfilprograma> findAllByPerfil(Perfil perfil){
     return  em.createQuery("SELECT r FROM Perfilprograma r WHERE r.perfil=:perfil AND r.estado=:estado").setParameter("perfil", perfil).setParameter("estado", Util.HABILITADO).getResultList();
    
    }
    
    public void disableAll(Perfil perfil){
            em.createQuery("UPDATE Perfilprograma pp SET pp.estado='I' WHERE pp.perfil=:perfil ").setParameter("perfil", perfil);
    
    }
    
//    public Perfilprograma findByPrograma(){
//         return (Perfilprograma) em.createQuery("SELECT r FROM Perfilprograma r WHERE r.perfilprogramaPK=:id").setParameter("perfil", perfil).setParameter("estado", Util.HABILITADO);
//    
//    }
    
}
