/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "servicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s")
    , @NamedQuery(name = "Servicio.findByServicioid", query = "SELECT s FROM Servicio s WHERE s.servicioid = :servicioid")
    , @NamedQuery(name = "Servicio.findByNombre", query = "SELECT s FROM Servicio s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Servicio.findByObservacion", query = "SELECT s FROM Servicio s WHERE s.observacion = :observacion")
    , @NamedQuery(name = "Servicio.findByEspecificarcantidad", query = "SELECT s FROM Servicio s WHERE s.especificarcantidad = :especificarcantidad")
    , @NamedQuery(name = "Servicio.findByEstado", query = "SELECT s FROM Servicio s WHERE s.estado = :estado")
    , @NamedQuery(name = "Servicio.findByUlastupdate", query = "SELECT s FROM Servicio s WHERE s.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Servicio.findByDtlastupdate", query = "SELECT s FROM Servicio s WHERE s.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Servicio.findByDtcreated", query = "SELECT s FROM Servicio s WHERE s.dtcreated = :dtcreated")})
public class Servicio implements Serializable {

    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "servicioid")
    private Long servicioid;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "especificarcantidad")
    private boolean especificarcantidad;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "servicioid")
    private Collection<Serviciopeticion> serviciopeticionCollection;
    
  

    public Servicio() {
    }

    public Servicio(Long servicioid) {
        this.servicioid = servicioid;
    }

    public Servicio(Long servicioid, String nombre, boolean especificarcantidad, String estado, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.servicioid = servicioid;
        this.nombre = nombre;
        this.especificarcantidad = especificarcantidad;
        this.estado = estado;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getServicioid() {
        return servicioid;
    }

    public void setServicioid(Long servicioid) {
        this.servicioid = servicioid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public boolean getEspecificarcantidad() {
        return especificarcantidad;
    }

    public void setEspecificarcantidad(boolean especificarcantidad) {
        this.especificarcantidad = especificarcantidad;
    }


    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Serviciopeticion> getServiciopeticionCollection() {
        return serviciopeticionCollection;
    }

    public void setServiciopeticionCollection(Collection<Serviciopeticion> serviciopeticionCollection) {
        this.serviciopeticionCollection = serviciopeticionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (servicioid != null ? servicioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.servicioid == null && other.servicioid != null) || (this.servicioid != null && !this.servicioid.equals(other.servicioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Servicio[ servicioid=" + servicioid + " ]";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
