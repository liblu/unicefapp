/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Diana Mejía
 */
@Embeddable
public class PermisosPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "programaid")
    private int programaid;
    @Basic(optional = false)
    @Column(name = "usuarioid")
    private long usuarioid;

    public PermisosPK() {
    }

    public PermisosPK(int programaid, long usuarioid) {
        this.programaid = programaid;
        this.usuarioid = usuarioid;
    }

    public int getProgramaid() {
        return programaid;
    }

    public void setProgramaid(int programaid) {
        this.programaid = programaid;
    }

    public long getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(long usuarioid) {
        this.usuarioid = usuarioid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) programaid;
        hash += (int) usuarioid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermisosPK)) {
            return false;
        }
        PermisosPK other = (PermisosPK) object;
        if (this.programaid != other.programaid) {
            return false;
        }
        if (this.usuarioid != other.usuarioid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.PermisosPK[ programaid=" + programaid + ", usuarioid=" + usuarioid + " ]";
    }
    
}
