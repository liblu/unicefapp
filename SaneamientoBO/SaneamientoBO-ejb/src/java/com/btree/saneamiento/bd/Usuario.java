/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByContrasenia", query = "SELECT u FROM Usuario u WHERE u.contrasenia = :contrasenia")
    , @NamedQuery(name = "Usuario.findByEstado", query = "SELECT u FROM Usuario u WHERE u.estado = :estado")
    , @NamedQuery(name = "Usuario.findByFechaalta", query = "SELECT u FROM Usuario u WHERE u.fechaalta = :fechaalta")
    , @NamedQuery(name = "Usuario.findByFechabaja", query = "SELECT u FROM Usuario u WHERE u.fechabaja = :fechabaja")
    , @NamedQuery(name = "Usuario.findByNombrecompleto", query = "SELECT u FROM Usuario u WHERE u.nombrecompleto = :nombrecompleto")
    , @NamedQuery(name = "Usuario.findByTipousuario", query = "SELECT u FROM Usuario u WHERE u.tipousuario = :tipousuario")
    , @NamedQuery(name = "Usuario.findByUsuario", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario")
    , @NamedQuery(name = "Usuario.findByUsuarioid", query = "SELECT u FROM Usuario u WHERE u.usuarioid = :usuarioid")
    , @NamedQuery(name = "Usuario.findByPerfilid", query = "SELECT u FROM Usuario u WHERE u.perfilid = :perfilid")
    , @NamedQuery(name = "Usuario.findByEmpresaid", query = "SELECT u FROM Usuario u WHERE u.empresaid = :empresaid")
    , @NamedQuery(name = "Usuario.findByLdap", query = "SELECT u FROM Usuario u WHERE u.ldap = :ldap")})
public class Usuario implements Serializable {

    @OneToMany(mappedBy = "usuarioid")
    private Collection<Monitor> monitorCollection;

    private static final long serialVersionUID = 1L;
    @Column(name = "contrasenia")
    private String contrasenia;
    @Column(name = "estado")
    private String estado;
    @Column(name = "fechaalta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaalta;
    @Column(name = "fechabaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechabaja;
    @Column(name = "nombrecompleto")
    private String nombrecompleto;
    @Column(name = "tipousuario")
    private String tipousuario;
    @Column(name = "usuario")
    private String usuario;
    @Id
    @Basic(optional = false)
    @Column(name = "usuarioid")
    private Long usuarioid;
    @Column(name = "perfilid")
    private Short perfilid;
    @Column(name = "empresaid")
    private Short empresaid;
    @Column(name = "ldap")
    private String ldap;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
    private Collection<Permisos> permisosCollection;

    public Usuario() {
    }

    public Usuario(Long usuarioid) {
        this.usuarioid = usuarioid;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Date fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Date getFechabaja() {
        return fechabaja;
    }

    public void setFechabaja(Date fechabaja) {
        this.fechabaja = fechabaja;
    }

    public String getNombrecompleto() {
        return nombrecompleto;
    }

    public void setNombrecompleto(String nombrecompleto) {
        this.nombrecompleto = nombrecompleto;
    }

    public String getTipousuario() {
        return tipousuario;
    }

    public void setTipousuario(String tipousuario) {
        this.tipousuario = tipousuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Long getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(Long usuarioid) {
        this.usuarioid = usuarioid;
    }

    public Short getPerfilid() {
        return perfilid;
    }

    public void setPerfilid(Short perfilid) {
        this.perfilid = perfilid;
    }

    public Short getEmpresaid() {
        return empresaid;
    }

    public void setEmpresaid(Short empresaid) {
        this.empresaid = empresaid;
    }

    public String getLdap() {
        return ldap;
    }

    public void setLdap(String ldap) {
        this.ldap = ldap;
    }

    @XmlTransient
    public Collection<Permisos> getPermisosCollection() {
        return permisosCollection;
    }

    public void setPermisosCollection(Collection<Permisos> permisosCollection) {
        this.permisosCollection = permisosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioid != null ? usuarioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioid == null && other.usuarioid != null) || (this.usuarioid != null && !this.usuarioid.equals(other.usuarioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Usuario[ usuarioid=" + usuarioid + " ]";
    }

    @XmlTransient
    public Collection<Monitor> getMonitorCollection() {
        return monitorCollection;
    }

    public void setMonitorCollection(Collection<Monitor> monitorCollection) {
        this.monitorCollection = monitorCollection;
    }
    
}
