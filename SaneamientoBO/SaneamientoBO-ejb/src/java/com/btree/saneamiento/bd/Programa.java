/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "programa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Programa.findAll", query = "SELECT p FROM Programa p")
    , @NamedQuery(name = "Programa.findByDescripcion", query = "SELECT p FROM Programa p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Programa.findByUrl", query = "SELECT p FROM Programa p WHERE p.url = :url")
    , @NamedQuery(name = "Programa.findByVisible", query = "SELECT p FROM Programa p WHERE p.visible = :visible")
    , @NamedQuery(name = "Programa.findByProgramaid", query = "SELECT p FROM Programa p WHERE p.programaid = :programaid")})
public class Programa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "url")
    private String url;
    @Column(name = "visible")
    private Boolean visible;
    @Id
    @Basic(optional = false)
    @Column(name = "programaid")
    private Integer programaid;
    @JoinColumn(name = "menuid", referencedColumnName = "menuid")
    @ManyToOne
    private Menu menuid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "programa")
    private Collection<Permisos> permisosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "programa")
    private Collection<Perfilprograma> perfilprogramaCollection;

    public Programa() {
    }

    public Programa(Integer programaid) {
        this.programaid = programaid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getProgramaid() {
        return programaid;
    }

    public void setProgramaid(Integer programaid) {
        this.programaid = programaid;
    }

    public Menu getMenuid() {
        return menuid;
    }

    public void setMenuid(Menu menuid) {
        this.menuid = menuid;
    }

    @XmlTransient
    public Collection<Permisos> getPermisosCollection() {
        return permisosCollection;
    }

    public void setPermisosCollection(Collection<Permisos> permisosCollection) {
        this.permisosCollection = permisosCollection;
    }

    @XmlTransient
    public Collection<Perfilprograma> getPerfilprogramaCollection() {
        return perfilprogramaCollection;
    }

    public void setPerfilprogramaCollection(Collection<Perfilprograma> perfilprogramaCollection) {
        this.perfilprogramaCollection = perfilprogramaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (programaid != null ? programaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Programa)) {
            return false;
        }
        Programa other = (Programa) object;
        if ((this.programaid == null && other.programaid != null) || (this.programaid != null && !this.programaid.equals(other.programaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Programa[ programaid=" + programaid + " ]";
    }
    
}
