/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "beneficiario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Beneficiario.findAll", query = "SELECT b FROM Beneficiario b")
    , @NamedQuery(name = "Beneficiario.findByBeneficiarioid", query = "SELECT b FROM Beneficiario b WHERE b.beneficiarioid = :beneficiarioid")
    , @NamedQuery(name = "Beneficiario.findByCodigoepsa", query = "SELECT b FROM Beneficiario b WHERE b.codigoepsa = :codigoepsa")
    , @NamedQuery(name = "Beneficiario.findByNombre", query = "SELECT b FROM Beneficiario b WHERE b.nombre = :nombre")    
    , @NamedQuery(name = "Beneficiario.findByDistrito", query = "SELECT b FROM Beneficiario b WHERE b.distrito = :distrito")
    , @NamedQuery(name = "Beneficiario.findByDireccion", query = "SELECT b FROM Beneficiario b WHERE b.direccion = :direccion")
    , @NamedQuery(name = "Beneficiario.findByEntidad", query = "SELECT b FROM Beneficiario b WHERE b.entidad = :entidad")
    , @NamedQuery(name = "Beneficiario.findByAutorizamonitoreo", query = "SELECT b FROM Beneficiario b WHERE b.autorizamonitoreo = :autorizamonitoreo")
    , @NamedQuery(name = "Beneficiario.findByProgramaproteccion", query = "SELECT b FROM Beneficiario b WHERE b.programaproteccion = :programaproteccion")
    , @NamedQuery(name = "Beneficiario.findByLat", query = "SELECT b FROM Beneficiario b WHERE b.lat = :lat")
    , @NamedQuery(name = "Beneficiario.findByLon", query = "SELECT b FROM Beneficiario b WHERE b.lon = :lon")
    , @NamedQuery(name = "Beneficiario.findByUlastupdate", query = "SELECT b FROM Beneficiario b WHERE b.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Beneficiario.findByDtlastupdate", query = "SELECT b FROM Beneficiario b WHERE b.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Beneficiario.findByDtcreated", query = "SELECT b FROM Beneficiario b WHERE b.dtcreated = :dtcreated")})
public class Beneficiario implements Serializable {

    @Basic(optional = false)
    @Column(name = "codigoepsa")
    private int codigoepsa;
    @Basic(optional = false)
    @Column(name = "distrito")
    private int distrito;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "lat")
    private Double lat;
    @Column(name = "lon")
    private Double lon;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "beneficiarioid")
    private Collection<Serviciopeticion> serviciopeticionCollection;

    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "beneficiarioid")
    private Long beneficiarioid;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "entidad")
    private String entidad;
    @Basic(optional = false)
    @Column(name = "autorizamonitoreo")
    private boolean autorizamonitoreo;
    @Basic(optional = false)
    @Column(name = "programaproteccion")
    private boolean programaproteccion;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "beneficiarioid")
    private Collection<Rutadetalle> rutadetalleCollection;

    public Beneficiario() {
    }

    public Beneficiario(Long beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }

    public Beneficiario(Long beneficiarioid, int codigoepsa, String nombre,  int distrito, String direccion, boolean autorizamonitoreo, boolean programaproteccion, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.beneficiarioid = beneficiarioid;
        this.codigoepsa = codigoepsa;
        this.nombre = nombre;
        
        this.distrito = distrito;
        this.direccion = direccion;
        this.autorizamonitoreo = autorizamonitoreo;
        this.programaproteccion = programaproteccion;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Long beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public boolean getAutorizamonitoreo() {
        return autorizamonitoreo;
    }

    public void setAutorizamonitoreo(boolean autorizamonitoreo) {
        this.autorizamonitoreo = autorizamonitoreo;
    }

    public boolean getProgramaproteccion() {
        return programaproteccion;
    }

    public void setProgramaproteccion(boolean programaproteccion) {
        this.programaproteccion = programaproteccion;
    }


    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Rutadetalle> getRutadetalleCollection() {
        return rutadetalleCollection;
    }

    public void setRutadetalleCollection(Collection<Rutadetalle> rutadetalleCollection) {
        this.rutadetalleCollection = rutadetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (beneficiarioid != null ? beneficiarioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Beneficiario)) {
            return false;
        }
        Beneficiario other = (Beneficiario) object;
        if ((this.beneficiarioid == null && other.beneficiarioid != null) || (this.beneficiarioid != null && !this.beneficiarioid.equals(other.beneficiarioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Beneficiario[ beneficiarioid=" + beneficiarioid + " ]";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Serviciopeticion> getServiciopeticionCollection() {
        return serviciopeticionCollection;
    }

    public void setServiciopeticionCollection(Collection<Serviciopeticion> serviciopeticionCollection) {
        this.serviciopeticionCollection = serviciopeticionCollection;
    }

    public int getCodigoepsa() {
        return codigoepsa;
    }

    public void setCodigoepsa(int codigoepsa) {
        this.codigoepsa = codigoepsa;
    }

    public int getDistrito() {
        return distrito;
    }

    public void setDistrito(int distrito) {
        this.distrito = distrito;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
    
}
