/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "perfilprograma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Perfilprograma.findAll", query = "SELECT p FROM Perfilprograma p")
    , @NamedQuery(name = "Perfilprograma.findByEstado", query = "SELECT p FROM Perfilprograma p WHERE p.estado = :estado")
    , @NamedQuery(name = "Perfilprograma.findByPerfilid", query = "SELECT p FROM Perfilprograma p WHERE p.perfilprogramaPK.perfilid = :perfilid")
    , @NamedQuery(name = "Perfilprograma.findByProgramaid", query = "SELECT p FROM Perfilprograma p WHERE p.perfilprogramaPK.programaid = :programaid")})
public class Perfilprograma implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PerfilprogramaPK perfilprogramaPK;
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "perfilid", referencedColumnName = "perfilid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Perfil perfil;
    @JoinColumn(name = "programaid", referencedColumnName = "programaid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Programa programa;

    public Perfilprograma() {
    }

    public Perfilprograma(PerfilprogramaPK perfilprogramaPK) {
        this.perfilprogramaPK = perfilprogramaPK;
    }

    public Perfilprograma(short perfilid, int programaid) {
        this.perfilprogramaPK = new PerfilprogramaPK(perfilid, programaid);
    }

    public PerfilprogramaPK getPerfilprogramaPK() {
        return perfilprogramaPK;
    }

    public void setPerfilprogramaPK(PerfilprogramaPK perfilprogramaPK) {
        this.perfilprogramaPK = perfilprogramaPK;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfilprogramaPK != null ? perfilprogramaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Perfilprograma)) {
            return false;
        }
        Perfilprograma other = (Perfilprograma) object;
        if ((this.perfilprogramaPK == null && other.perfilprogramaPK != null) || (this.perfilprogramaPK != null && !this.perfilprogramaPK.equals(other.perfilprogramaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Perfilprograma[ perfilprogramaPK=" + perfilprogramaPK + " ]";
    }
    
}
