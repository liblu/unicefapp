/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Diana Mejía
 */
@Embeddable
public class PerfilprogramaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "perfilid")
    private short perfilid;
    @Basic(optional = false)
    @Column(name = "programaid")
    private int programaid;

    public PerfilprogramaPK() {
    }

    public PerfilprogramaPK(short perfilid, int programaid) {
        this.perfilid = perfilid;
        this.programaid = programaid;
    }

    public short getPerfilid() {
        return perfilid;
    }

    public void setPerfilid(short perfilid) {
        this.perfilid = perfilid;
    }

    public int getProgramaid() {
        return programaid;
    }

    public void setProgramaid(int programaid) {
        this.programaid = programaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) perfilid;
        hash += (int) programaid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PerfilprogramaPK)) {
            return false;
        }
        PerfilprogramaPK other = (PerfilprogramaPK) object;
        if (this.perfilid != other.perfilid) {
            return false;
        }
        if (this.programaid != other.programaid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.PerfilprogramaPK[ perfilid=" + perfilid + ", programaid=" + programaid + " ]";
    }
    
}
