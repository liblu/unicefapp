/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "serviciopeticion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Serviciopeticion.findAll", query = "SELECT s FROM Serviciopeticion s")
    , @NamedQuery(name = "Serviciopeticion.findByServiciopeticionid", query = "SELECT s FROM Serviciopeticion s WHERE s.serviciopeticionid = :serviciopeticionid")
    , @NamedQuery(name = "Serviciopeticion.findByEstado", query = "SELECT s FROM Serviciopeticion s WHERE s.estado = :estado")
    , @NamedQuery(name = "Serviciopeticion.findByCantidad", query = "SELECT s FROM Serviciopeticion s WHERE s.cantidad = :cantidad")
    , @NamedQuery(name = "Serviciopeticion.findByUlastupdate", query = "SELECT s FROM Serviciopeticion s WHERE s.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Serviciopeticion.findByDtlastupdate", query = "SELECT s FROM Serviciopeticion s WHERE s.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Serviciopeticion.findByDtcreated", query = "SELECT s FROM Serviciopeticion s WHERE s.dtcreated = :dtcreated")})
public class Serviciopeticion implements Serializable {

    @JoinColumn(name = "beneficiarioid", referencedColumnName = "beneficiarioid")
    @ManyToOne(optional = false)
    private Beneficiario beneficiarioid;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "serviciopeticionid")
    private Long serviciopeticionid;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @JoinColumn(name = "evaluacionid", referencedColumnName = "evaluacionid")
    @ManyToOne(optional = false)
    private Evaluacion evaluacionid;
    @JoinColumn(name = "servicioid", referencedColumnName = "servicioid")
    @ManyToOne(optional = false)
    private Servicio servicioid;

    public Serviciopeticion() {
    }

    public Serviciopeticion(Long serviciopeticionid) {
        this.serviciopeticionid = serviciopeticionid;
    }

    public Serviciopeticion(Long serviciopeticionid, String estado, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.serviciopeticionid = serviciopeticionid;
        this.estado = estado;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getServiciopeticionid() {
        return serviciopeticionid;
    }

    public void setServiciopeticionid(Long serviciopeticionid) {
        this.serviciopeticionid = serviciopeticionid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    public Evaluacion getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Evaluacion evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Servicio getServicioid() {
        return servicioid;
    }

    public void setServicioid(Servicio servicioid) {
        this.servicioid = servicioid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviciopeticionid != null ? serviciopeticionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Serviciopeticion)) {
            return false;
        }
        Serviciopeticion other = (Serviciopeticion) object;
        if ((this.serviciopeticionid == null && other.serviciopeticionid != null) || (this.serviciopeticionid != null && !this.serviciopeticionid.equals(other.serviciopeticionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Serviciopeticion[ serviciopeticionid=" + serviciopeticionid + " ]";
    }

    public Beneficiario getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Beneficiario beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }
    
}
