/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "planilla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Planilla.findAll", query = "SELECT p FROM Planilla p")
    , @NamedQuery(name = "Planilla.findByPlanillaid", query = "SELECT p FROM Planilla p WHERE p.planillaid = :planillaid")
    , @NamedQuery(name = "Planilla.findByNombre", query = "SELECT p FROM Planilla p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Planilla.findByDescripcion", query = "SELECT p FROM Planilla p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Planilla.findByUlastupdate", query = "SELECT p FROM Planilla p WHERE p.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Planilla.findByDtlastupdate", query = "SELECT p FROM Planilla p WHERE p.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Planilla.findByDtcreated", query = "SELECT p FROM Planilla p WHERE p.dtcreated = :dtcreated")})
public class Planilla implements Serializable {

    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "planillaid")
    private Long planillaid;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planillaid")
    private Collection<Ruta> rutaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planillaid")
    private Collection<Evaluacion> evaluacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planillaid")
    private Collection<Pregunta> preguntaCollection;

    public Planilla() {
    }

    public Planilla(Long planillaid) {
        this.planillaid = planillaid;
    }

    public Planilla(Long planillaid, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.planillaid = planillaid;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Long planillaid) {
        this.planillaid = planillaid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Ruta> getRutaCollection() {
        return rutaCollection;
    }

    public void setRutaCollection(Collection<Ruta> rutaCollection) {
        this.rutaCollection = rutaCollection;
    }

    @XmlTransient
    public Collection<Evaluacion> getEvaluacionCollection() {
        return evaluacionCollection;
    }

    public void setEvaluacionCollection(Collection<Evaluacion> evaluacionCollection) {
        this.evaluacionCollection = evaluacionCollection;
    }

    @XmlTransient
    public Collection<Pregunta> getPreguntaCollection() {
        return preguntaCollection;
    }

    public void setPreguntaCollection(Collection<Pregunta> preguntaCollection) {
        this.preguntaCollection = preguntaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planillaid != null ? planillaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planilla)) {
            return false;
        }
        Planilla other = (Planilla) object;
        if ((this.planillaid == null && other.planillaid != null) || (this.planillaid != null && !this.planillaid.equals(other.planillaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Planilla[ planillaid=" + planillaid + " ]";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
