/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "monitor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Monitor.findAll", query = "SELECT m FROM Monitor m")
    , @NamedQuery(name = "Monitor.findByMonitorid", query = "SELECT m FROM Monitor m WHERE m.monitorid = :monitorid")
    , @NamedQuery(name = "Monitor.findByNombre", query = "SELECT m FROM Monitor m WHERE m.nombre = :nombre")    
    , @NamedQuery(name = "Monitor.findByCodigoepsa", query = "SELECT m FROM Monitor m WHERE m.codigoepsa = :codigoepsa")
    , @NamedQuery(name = "Monitor.findByUlastupdate", query = "SELECT m FROM Monitor m WHERE m.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Monitor.findByDtlastupdate", query = "SELECT m FROM Monitor m WHERE m.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Monitor.findByDtcreated", query = "SELECT m FROM Monitor m WHERE m.dtcreated = :dtcreated")})
public class Monitor implements Serializable {

    @JoinColumn(name = "usuarioid", referencedColumnName = "usuarioid")
    @ManyToOne
    private Usuario usuarioid;

    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "monitorid")
    private Long monitorid;
    @Column(name = "nombre")
    private String nombre;
   
    @Column(name = "codigoepsa")
    private int codigoepsa;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "monitorid")
    private Collection<Ruta> rutaCollection;

    public Monitor() {
    }

    public Monitor(Long monitorid) {
        this.monitorid = monitorid;
    }

    public Monitor(Long monitorid, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.monitorid = monitorid;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getMonitorid() {
        return monitorid;
    }

    public void setMonitorid(Long monitorid) {
        this.monitorid = monitorid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   

    public Integer getCodigoepsa() {
        return codigoepsa;
    }

    public void setCodigoepsa(Integer codigoepsa) {
        this.codigoepsa = codigoepsa;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Ruta> getRutaCollection() {
        return rutaCollection;
    }

    public void setRutaCollection(Collection<Ruta> rutaCollection) {
        this.rutaCollection = rutaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (monitorid != null ? monitorid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Monitor)) {
            return false;
        }
        Monitor other = (Monitor) object;
        if ((this.monitorid == null && other.monitorid != null) || (this.monitorid != null && !this.monitorid.equals(other.monitorid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Monitor[ monitorid=" + monitorid + " ]";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Usuario getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(Usuario usuarioid) {
        this.usuarioid = usuarioid;
    }
    
}
