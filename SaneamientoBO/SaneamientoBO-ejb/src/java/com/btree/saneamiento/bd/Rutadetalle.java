/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "rutadetalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rutadetalle.findAll", query = "SELECT r FROM Rutadetalle r")
    , @NamedQuery(name = "Rutadetalle.findByRutadetid", query = "SELECT r FROM Rutadetalle r WHERE r.rutadetid = :rutadetid")
    , @NamedQuery(name = "Rutadetalle.findByOrden", query = "SELECT r FROM Rutadetalle r WHERE r.orden = :orden")
    , @NamedQuery(name = "Rutadetalle.findByUlastupdate", query = "SELECT r FROM Rutadetalle r WHERE r.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Rutadetalle.findByDtlastupdate", query = "SELECT r FROM Rutadetalle r WHERE r.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Rutadetalle.findByDtcreated", query = "SELECT r FROM Rutadetalle r WHERE r.dtcreated = :dtcreated")})
public class Rutadetalle implements Serializable {

    @Basic(optional = false)
    @Column(name = "orden")
    private int orden;
    @Column(name = "estado")
    private String estado;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "rutadetid")
    private Long rutadetid;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rutadetid")
    private Collection<Evaluacion> evaluacionCollection;
    @JoinColumn(name = "beneficiarioid", referencedColumnName = "beneficiarioid")
    @ManyToOne(optional = false)
    private Beneficiario beneficiarioid;
    @JoinColumn(name = "rutaid", referencedColumnName = "rutaid")
    @ManyToOne(optional = false)
    private Ruta rutaid;

    public Rutadetalle() {
    }

    public Rutadetalle(Long rutadetid) {
        this.rutadetid = rutadetid;
    }

    public Rutadetalle(Long rutadetid, Integer orden, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.rutadetid = rutadetid;
        this.orden = orden;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getRutadetid() {
        return rutadetid;
    }

    public void setRutadetid(Long rutadetid) {
        this.rutadetid = rutadetid;
    }


    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Evaluacion> getEvaluacionCollection() {
        return evaluacionCollection;
    }

    public void setEvaluacionCollection(Collection<Evaluacion> evaluacionCollection) {
        this.evaluacionCollection = evaluacionCollection;
    }

    public Beneficiario getBeneficiarioid() {
        return beneficiarioid;
    }

    public void setBeneficiarioid(Beneficiario beneficiarioid) {
        this.beneficiarioid = beneficiarioid;
    }

    public Ruta getRutaid() {
        return rutaid;
    }

    public void setRutaid(Ruta rutaid) {
        this.rutaid = rutaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutadetid != null ? rutadetid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rutadetalle)) {
            return false;
        }
        Rutadetalle other = (Rutadetalle) object;
        if ((this.rutadetid == null && other.rutadetid != null) || (this.rutadetid != null && !this.rutadetid.equals(other.rutadetid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Rutadetalle[ rutadetid=" + rutadetid + " ]";
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }
    
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
