/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "evaluacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluacion.findAll", query = "SELECT e FROM Evaluacion e")
    , @NamedQuery(name = "Evaluacion.findByEvaluacionid", query = "SELECT e FROM Evaluacion e WHERE e.evaluacionid = :evaluacionid")
    , @NamedQuery(name = "Evaluacion.findByDescripcion", query = "SELECT e FROM Evaluacion e WHERE e.descripcion = :descripcion")
    , @NamedQuery(name = "Evaluacion.findByPicture", query = "SELECT e FROM Evaluacion e WHERE e.picture = :picture")
    , @NamedQuery(name = "Evaluacion.findByUlastupdate", query = "SELECT e FROM Evaluacion e WHERE e.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Evaluacion.findByDtlastupdate", query = "SELECT e FROM Evaluacion e WHERE e.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Evaluacion.findByDtcreated", query = "SELECT e FROM Evaluacion e WHERE e.dtcreated = :dtcreated")})
public class Evaluacion implements Serializable {

    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "evaluacionid")
    private Long evaluacionid;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "picture")
    private String picture;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @JoinColumn(name = "planillaid", referencedColumnName = "planillaid")
    @ManyToOne(optional = false)
    private Planilla planillaid;
    @JoinColumn(name = "rutadetid", referencedColumnName = "rutadetid")
    @ManyToOne(optional = false)
    private Rutadetalle rutadetid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionid")
    private Collection<Respuesta> respuestaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionid")
    private Collection<Serviciopeticion> serviciopeticionCollection;

    public Evaluacion() {
    }

    public Evaluacion(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Evaluacion(Long evaluacionid, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.evaluacionid = evaluacionid;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    public Planilla getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Planilla planillaid) {
        this.planillaid = planillaid;
    }

    public Rutadetalle getRutadetid() {
        return rutadetid;
    }

    public void setRutadetid(Rutadetalle rutadetid) {
        this.rutadetid = rutadetid;
    }

    @XmlTransient
    public Collection<Respuesta> getRespuestaCollection() {
        return respuestaCollection;
    }

    public void setRespuestaCollection(Collection<Respuesta> respuestaCollection) {
        this.respuestaCollection = respuestaCollection;
    }

    @XmlTransient
    public Collection<Serviciopeticion> getServiciopeticionCollection() {
        return serviciopeticionCollection;
    }

    public void setServiciopeticionCollection(Collection<Serviciopeticion> serviciopeticionCollection) {
        this.serviciopeticionCollection = serviciopeticionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluacionid != null ? evaluacionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluacion)) {
            return false;
        }
        Evaluacion other = (Evaluacion) object;
        if ((this.evaluacionid == null && other.evaluacionid != null) || (this.evaluacionid != null && !this.evaluacionid.equals(other.evaluacionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Evaluacion[ evaluacionid=" + evaluacionid + " ]";
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
