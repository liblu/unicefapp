/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "grupo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupo.findAll", query = "SELECT g FROM Grupo g")
    , @NamedQuery(name = "Grupo.findByGrupoid", query = "SELECT g FROM Grupo g WHERE g.grupoid = :grupoid")
    , @NamedQuery(name = "Grupo.findByDescripcion", query = "SELECT g FROM Grupo g WHERE g.descripcion = :descripcion")
    , @NamedQuery(name = "Grupo.findByUlastupdate", query = "SELECT g FROM Grupo g WHERE g.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Grupo.findByDtlastupdate", query = "SELECT g FROM Grupo g WHERE g.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Grupo.findByDtcreated", query = "SELECT g FROM Grupo g WHERE g.dtcreated = :dtcreated")})
public class Grupo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "grupoid")
    private Long grupoid;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoid")
    private Collection<Pregunta> preguntaCollection;

    public Grupo() {
    }

    public Grupo(Long grupoid) {
        this.grupoid = grupoid;
    }

    public Grupo(Long grupoid, String descripcion, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.grupoid = grupoid;
        this.descripcion = descripcion;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getGrupoid() {
        return grupoid;
    }

    public void setGrupoid(Long grupoid) {
        this.grupoid = grupoid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    @XmlTransient
    public Collection<Pregunta> getPreguntaCollection() {
        return preguntaCollection;
    }

    public void setPreguntaCollection(Collection<Pregunta> preguntaCollection) {
        this.preguntaCollection = preguntaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupoid != null ? grupoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.grupoid == null && other.grupoid != null) || (this.grupoid != null && !this.grupoid.equals(other.grupoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Grupo[ grupoid=" + grupoid + " ]";
    }
    
}
