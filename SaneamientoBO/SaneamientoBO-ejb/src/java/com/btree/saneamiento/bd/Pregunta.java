/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "pregunta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pregunta.findAll", query = "SELECT p FROM Pregunta p")
    , @NamedQuery(name = "Pregunta.findByPreguntaid", query = "SELECT p FROM Pregunta p WHERE p.preguntaid = :preguntaid")
    , @NamedQuery(name = "Pregunta.findByDescripcion", query = "SELECT p FROM Pregunta p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Pregunta.findByTiporespuesta", query = "SELECT p FROM Pregunta p WHERE p.tiporespuesta = :tiporespuesta")
    , @NamedQuery(name = "Pregunta.findByUlastupdate", query = "SELECT p FROM Pregunta p WHERE p.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Pregunta.findByDtlastupdate", query = "SELECT p FROM Pregunta p WHERE p.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Pregunta.findByDtcreated", query = "SELECT p FROM Pregunta p WHERE p.dtcreated = :dtcreated")})
public class Pregunta implements Serializable {

    @Column(name = "estado")
    private String estado;
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "preguntaid")
    private Long preguntaid;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "tiporespuesta")
    private String tiporespuesta;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @JoinColumn(name = "grupoid", referencedColumnName = "grupoid")
    @ManyToOne(optional = false)
    private Grupo grupoid;
    @JoinColumn(name = "planillaid", referencedColumnName = "planillaid")
    @ManyToOne(optional = false)
    private Planilla planillaid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "preguntaid")
    private Collection<Respuesta> respuestaCollection;

    public Pregunta() {
    }

    public Pregunta(Long preguntaid) {
        this.preguntaid = preguntaid;
    }

    public Pregunta(Long preguntaid, String descripcion, String tiporespuesta, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.preguntaid = preguntaid;
        this.descripcion = descripcion;
        this.tiporespuesta = tiporespuesta;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Long preguntaid) {
        this.preguntaid = preguntaid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTiporespuesta() {
        return tiporespuesta;
    }

    public void setTiporespuesta(String tiporespuesta) {
        this.tiporespuesta = tiporespuesta;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    public Grupo getGrupoid() {
        return grupoid;
    }

    public void setGrupoid(Grupo grupoid) {
        this.grupoid = grupoid;
    }

    public Planilla getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Planilla planillaid) {
        this.planillaid = planillaid;
    }

    @XmlTransient
    public Collection<Respuesta> getRespuestaCollection() {
        return respuestaCollection;
    }

    public void setRespuestaCollection(Collection<Respuesta> respuestaCollection) {
        this.respuestaCollection = respuestaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaid != null ? preguntaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pregunta)) {
            return false;
        }
        Pregunta other = (Pregunta) object;
        if ((this.preguntaid == null && other.preguntaid != null) || (this.preguntaid != null && !this.preguntaid.equals(other.preguntaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Pregunta[ preguntaid=" + preguntaid + " ]";
    }
    
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
