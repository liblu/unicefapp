/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "ruta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ruta.findAll", query = "SELECT r FROM Ruta r")
    , @NamedQuery(name = "Ruta.findByRutaid", query = "SELECT r FROM Ruta r WHERE r.rutaid = :rutaid")
    , @NamedQuery(name = "Ruta.findByNombre", query = "SELECT r FROM Ruta r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "Ruta.findByDescripcion", query = "SELECT r FROM Ruta r WHERE r.descripcion = :descripcion")    
    , @NamedQuery(name = "Ruta.findByEstado", query = "SELECT r FROM Ruta r WHERE r.estado = :estado")
    , @NamedQuery(name = "Ruta.findByDia", query = "SELECT r FROM Ruta r WHERE r.dia = :dia")
    , @NamedQuery(name = "Ruta.findByUlastupdate", query = "SELECT r FROM Ruta r WHERE r.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Ruta.findByDtlastupdate", query = "SELECT r FROM Ruta r WHERE r.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Ruta.findByDtcreated", query = "SELECT r FROM Ruta r WHERE r.dtcreated = :dtcreated")})
public class Ruta implements Serializable {

    @Column(name = "descripcion")
    private String descripcion;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "rutaid")
    private Long rutaid;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @Column(name = "dia")
    private String dia;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @JoinColumn(name = "monitorid", referencedColumnName = "monitorid")
    @ManyToOne(optional = false)
    private Monitor monitorid;
    @JoinColumn(name = "planillaid", referencedColumnName = "planillaid")
    @ManyToOne(optional = false)
    private Planilla planillaid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rutaid")
    private Collection<Rutadetalle> rutadetalleCollection;

    public Ruta() {
    }

    public Ruta(Long rutaid) {
        this.rutaid = rutaid;
    }

    public Ruta(Long rutaid, String nombre, String estado, String dia, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.rutaid = rutaid;
        this.nombre = nombre;
        this.estado = estado;
        this.dia = dia;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getRutaid() {
        return rutaid;
    }

    public void setRutaid(Long rutaid) {
        this.rutaid = rutaid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

   

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    public Monitor getMonitorid() {
        return monitorid;
    }

    public void setMonitorid(Monitor monitorid) {
        this.monitorid = monitorid;
    }

    public Planilla getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Planilla planillaid) {
        this.planillaid = planillaid;
    }

    @XmlTransient
    public Collection<Rutadetalle> getRutadetalleCollection() {
        return rutadetalleCollection;
    }

    public void setRutadetalleCollection(Collection<Rutadetalle> rutadetalleCollection) {
        this.rutadetalleCollection = rutadetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutaid != null ? rutaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ruta)) {
            return false;
        }
        Ruta other = (Ruta) object;
        if ((this.rutaid == null && other.rutaid != null) || (this.rutaid != null && !this.rutaid.equals(other.rutaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Ruta[ rutaid=" + rutaid + " ]";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
