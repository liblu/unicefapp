/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "respuesta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Respuesta.findAll", query = "SELECT r FROM Respuesta r")
    , @NamedQuery(name = "Respuesta.findByRespuestaid", query = "SELECT r FROM Respuesta r WHERE r.respuestaid = :respuestaid")
    , @NamedQuery(name = "Respuesta.findByRespuesta", query = "SELECT r FROM Respuesta r WHERE r.respuesta = :respuesta")
    , @NamedQuery(name = "Respuesta.findByRespuesttext", query = "SELECT r FROM Respuesta r WHERE r.respuesttext = :respuesttext")
    , @NamedQuery(name = "Respuesta.findByRespuestavalue", query = "SELECT r FROM Respuesta r WHERE r.respuestavalue = :respuestavalue")
    , @NamedQuery(name = "Respuesta.findByUlastupdate", query = "SELECT r FROM Respuesta r WHERE r.ulastupdate = :ulastupdate")
    , @NamedQuery(name = "Respuesta.findByDtlastupdate", query = "SELECT r FROM Respuesta r WHERE r.dtlastupdate = :dtlastupdate")
    , @NamedQuery(name = "Respuesta.findByDtcreated", query = "SELECT r FROM Respuesta r WHERE r.dtcreated = :dtcreated")})
public class Respuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "respuestaid")
    private Long respuestaid;
    @Column(name = "respuesta")
    private Boolean respuesta;
    @Column(name = "respuesttext")
    private String respuesttext;
    @Column(name = "respuestavalue")
    private Integer respuestavalue;
    @Basic(optional = false)
    @Column(name = "ulastupdate")
    private String ulastupdate;
    @Basic(optional = false)
    @Column(name = "dtlastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtlastupdate;
    @Basic(optional = false)
    @Column(name = "dtcreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtcreated;
    @JoinColumn(name = "evaluacionid", referencedColumnName = "evaluacionid")
    @ManyToOne(optional = false)
    private Evaluacion evaluacionid;
    @JoinColumn(name = "preguntaid", referencedColumnName = "preguntaid")
    @ManyToOne(optional = false)
    private Pregunta preguntaid;

    public Respuesta() {
    }

    public Respuesta(Long respuestaid) {
        this.respuestaid = respuestaid;
    }

    public Respuesta(Long respuestaid, String ulastupdate, Date dtlastupdate, Date dtcreated) {
        this.respuestaid = respuestaid;
        this.ulastupdate = ulastupdate;
        this.dtlastupdate = dtlastupdate;
        this.dtcreated = dtcreated;
    }

    public Long getRespuestaid() {
        return respuestaid;
    }

    public void setRespuestaid(Long respuestaid) {
        this.respuestaid = respuestaid;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuesttext() {
        return respuesttext;
    }

    public void setRespuesttext(String respuesttext) {
        this.respuesttext = respuesttext;
    }

    public Integer getRespuestavalue() {
        return respuestavalue;
    }

    public void setRespuestavalue(Integer respuestavalue) {
        this.respuestavalue = respuestavalue;
    }

    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }

    public Date getDtlastupdate() {
        return dtlastupdate;
    }

    public void setDtlastupdate(Date dtlastupdate) {
        this.dtlastupdate = dtlastupdate;
    }

    public Date getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Date dtcreated) {
        this.dtcreated = dtcreated;
    }

    public Evaluacion getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Evaluacion evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Pregunta getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Pregunta preguntaid) {
        this.preguntaid = preguntaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respuestaid != null ? respuestaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Respuesta)) {
            return false;
        }
        Respuesta other = (Respuesta) object;
        if ((this.respuestaid == null && other.respuestaid != null) || (this.respuestaid != null && !this.respuestaid.equals(other.respuestaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Respuesta[ respuestaid=" + respuestaid + " ]";
    }
    
}
