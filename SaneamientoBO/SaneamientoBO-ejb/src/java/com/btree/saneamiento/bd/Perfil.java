/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.bd;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Diana Mejía
 */
@Entity
@Table(name = "perfil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Perfil.findAll", query = "SELECT p FROM Perfil p")
    , @NamedQuery(name = "Perfil.findByDescripcion", query = "SELECT p FROM Perfil p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Perfil.findByEstado", query = "SELECT p FROM Perfil p WHERE p.estado = :estado")
    , @NamedQuery(name = "Perfil.findByPerfilid", query = "SELECT p FROM Perfil p WHERE p.perfilid = :perfilid")
    , @NamedQuery(name = "Perfil.findByLdap", query = "SELECT p FROM Perfil p WHERE p.ldap = :ldap")})
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "estado")
    private String estado;
    @Id
    @Basic(optional = false)
    @Column(name = "perfilid")
    private Short perfilid;
    @Column(name = "ldap")
    private String ldap;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "perfil")
    private Collection<Perfilprograma> perfilprogramaCollection;

    public Perfil() {
    }

    public Perfil(Short perfilid) {
        this.perfilid = perfilid;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Short getPerfilid() {
        return perfilid;
    }

    public void setPerfilid(Short perfilid) {
        this.perfilid = perfilid;
    }

    public String getLdap() {
        return ldap;
    }

    public void setLdap(String ldap) {
        this.ldap = ldap;
    }

    @XmlTransient
    public Collection<Perfilprograma> getPerfilprogramaCollection() {
        return perfilprogramaCollection;
    }

    public void setPerfilprogramaCollection(Collection<Perfilprograma> perfilprogramaCollection) {
        this.perfilprogramaCollection = perfilprogramaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perfilid != null ? perfilid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Perfil)) {
            return false;
        }
        Perfil other = (Perfil) object;
        if ((this.perfilid == null && other.perfilid != null) || (this.perfilid != null && !this.perfilid.equals(other.perfilid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.btree.saneamiento.bd.Perfil[ perfilid=" + perfilid + " ]";
    }
    
}
