/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.util;

/**
 *
 * @author Daniel Yugar
 */
public class AppException extends Exception {

    public AppException(String message) {
        super(message);
    }
}
