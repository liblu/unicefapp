/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.util;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import com.google.gson.JsonObject;

/**
 *
 * @author diana
 */
@Singleton
@LocalBean
public class PushNotification {
    public static Logger log = Logger.getLogger(PushNotification.class.getName());


    
// @EJB
// private ReportsCooperativaFacade cooperativaService;
 
 
    private final String keyServer = "AIzaSyABn9qYVrE7XBQvSo_8tkwkH99dVSFXoGM ";

    public void sendNotification(Integer componente, Integer tipoComp, Integer tipMed, Double medida, String mensaje) throws MalformedURLException {
        String uri = "https://fcm.googleapis.com/fcm/send";
        try {
            String topic= "";
           //  String topic= cooperativaService.find(ReportsCooperativaFacade.ID).getCooNombreCorto();
            Logger.getLogger(PushNotification.class.getName()).log(Level.INFO, "topic>"+topic);
                    
            JsonObject json = new JsonObject();
            json.addProperty("componente", componente);
            json.addProperty("tipoComp", tipoComp);
            json.addProperty("mensaje", mensaje);
            json.addProperty("tipoMed", tipMed);
            json.addProperty("medida", medida);

            JsonObject jsonBody = new JsonObject();
            jsonBody.addProperty("to", "/topics/"+topic);
            jsonBody.add("data", json);

            String request = jsonBody.toString();

            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key=" + keyServer);

// For POST only - START
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(request);
            wr.flush();
            wr.close();

            // For POST only - END
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                log.info("notification>"+ mensaje+ " response>"+response.toString());
            
            } else {
                
                log.info("POST request not worked");
                
            }

        } catch (IOException ex) {
            Logger.getLogger(PushNotification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
