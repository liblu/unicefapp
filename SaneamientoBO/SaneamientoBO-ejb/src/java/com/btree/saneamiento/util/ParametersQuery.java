package com.btree.saneamiento.util;

import java.util.HashMap;
import java.util.Map;

public class ParametersQuery {

	private final Map<String, Object> parametros;

	private ParametersQuery() {
		this.parametros = new HashMap<>();
	}

	public void put(String nombre, Object valor) {
		parametros.put(nombre, valor);
	}

	public Map<String, Object> getParametros() {
		return parametros;
	}

	public static ParametersQuery getParametersQuery() {
		return new ParametersQuery();
	}

}
