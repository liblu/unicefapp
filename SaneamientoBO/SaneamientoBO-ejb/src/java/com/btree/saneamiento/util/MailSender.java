/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.util;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Victor
 */
public class MailSender {

    /**
     * @param args the command line arguments
     */
//    public void MailSender() {
//        // TODO code application logic here
//    }
    /**
     * Multipart
     */
    private MimeMultipart mp;
    /**
     * MimeMessage object
     */
    private MimeMessage message;
    private Session session;
    public boolean auth;
    
    private String mensaje;

    public MailSender(String host, String port, boolean auth, boolean tls) throws AddressException, MessagingException {

        mp = new MimeMultipart();
        Properties props = new Properties();
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", tls);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        session = Session.getInstance(props, null);
        message = new MimeMessage(session);
        message.setSentDate(new Date());
        this.auth = auth;
    }
    
    public MimeMessage getMessage(){
        return message;
    }

    public void setFrom(String from) throws AddressException, MessagingException {
        message.setFrom(new InternetAddress(from));
    }

    public void setTo(String to) throws AddressException, MessagingException {
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
    }

    public void setSubject(String subject) throws MessagingException {
        message.setSubject(subject);
    }

    public void addBodyPart(String text) throws MessagingException {
        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(text);
        mp.addBodyPart(bodyPart);
    }
    
    public void addMessageContent(String mensaje){
        this.mensaje = mensaje;
    }

    public void addAttachment(String file) throws MessagingException {
        // create a new message part
        MimeBodyPart msgPart = new MimeBodyPart();

        // attach the file to the message
        FileDataSource fds = new FileDataSource(file);
        msgPart.setDataHandler(new DataHandler(fds));
        msgPart.setFileName(fds.getName());

        // Adds new message part
        mp.addBodyPart(msgPart);
    }

    public void sendMail(String servidorSMTP, String usuarioSMTP, String passwordSMTP) throws MessagingException {
        message.setContent(mensaje,"text/html; charset=utf-8");
        message.saveChanges();
        Transport tr = session.getTransport("smtp");
        if (auth) {
            tr.connect(servidorSMTP, usuarioSMTP, passwordSMTP);
        } else {
            tr.connect();
        }
        tr.sendMessage(message, message.getAllRecipients());
        tr.close();
    }
}
