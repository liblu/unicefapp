/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Diana Mejia
 */
@Singleton
@LocalBean
public class Util {

    public static final String SEPARATOR = System.getProperties().getProperty("file.separator");
    //Estados
    public static final String HABILITADO = "H";
    public static final String INHABILITADO = "I";  
    public static final String PENDIENTE = "P";
    public static final String REALIZADO = "R";
    public static final String COMPLETADO = "C";
    public static final String AUTORIZADO = "A";
    public static final String COMERCIAL = "M";
    
    public static final String MSG_DELETE="Eliminado satisfactoriamente";
     public static final String MSG_CRASH="Error inesperado";
    public static final String MSG_DELETE_FAIL="No se puede eliminar, debido a que es utilizado por otros registros";
    public static final Long USUARIO_SYS=1L;
    public static final Long PERMISO_APP=11L;
    
   public static final String DATE_FORMAT = "dd/MM/yyyy";
    
    public static final Long PLANILLA_DEFAULT=1L;

    public void saveFile(String path, InputStream inputStream) throws IOException {
        OutputStream outStream = null;
        try {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            File targetFile = new File(path);
            if (!targetFile.exists()) {
                targetFile.createNewFile();
            }
            outStream = new FileOutputStream(targetFile);
            outStream.write(buffer);
        } catch (Exception e) {
            throw e;
        } finally {
            if (outStream != null) {
                outStream.close();
            }
        }
    }

    public void renameFile(String pathOrigen, String pathDestino) throws Exception {
        try {
            File afile = new File(pathOrigen);
            File dfile = new File(pathDestino);
            if (dfile.exists()) {
                dfile.delete();
            }
            if (!afile.renameTo(dfile)) {
                throw new Exception("No se pudo cambiar el nombre del archivo");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void removeFile(String path1) {
        try {
            File file = new File(path1);
            file.delete();
        } catch (Exception e) {
        }
    }

    public String getExtension(String fileName) {
        String extension = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
        return extension;
    }

    public String getUrlPathCircuito() {
        String urlPath = "SICMO_PERIAGUA" + SEPARATOR + "Circuitos" + SEPARATOR;
        return urlPath;
    }

    public String getUrlPathImagenInformativa() {
        String urlPath = "SICMO_PERIAGUA" + SEPARATOR + "ImagenInformativa" + SEPARATOR;
        return urlPath;
    }

    public String getUrlPathInformacionAdicional() {
        String urlPath = "SICMO_PERIAGUA" + SEPARATOR + "InformacionAdicional" + SEPARATOR;
        return urlPath;
    }

    public String getPathUserHome() {
        String realPath = System.getProperties().getProperty("user.home") + SEPARATOR;
        return realPath;
    }

    public String getSeparator() {
        return SEPARATOR;
    }

    public void showInfo(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, ""));
    }

    public void showWarn(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, mensaje, ""));
    }

    public void showError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, ""));
    }

    public Date getFechaHoraFinal(Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.HOUR_OF_DAY, 23);
        fecha = new Date(c.getTimeInMillis());
        return fecha;
    }

    public Date getFechaHoraInicial(Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        fecha = new Date(c.getTimeInMillis());
        return fecha;
    }

    public Date getFechaAdd(Date fecha, int field, int cant) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(field, cant);
        return new Date(c.getTimeInMillis());
    }

    public Date getFechaAdd(int field, int cant) {
        Calendar c = Calendar.getInstance();
        c.add(field, cant);
        return new Date(c.getTimeInMillis());
    }

    public String convertDateToString(String format, Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(fecha);
    }

    public int substractMinutes(Date fechaInicial, Date fechaFinal) {
        long diff = fechaFinal.getTime() - fechaInicial.getTime();
        return (int) (diff / (60 * 1000));
    }

    public int substractSeconds(Date fechaInicial, Date fechaFinal) {
        long diff = fechaFinal.getTime() - fechaInicial.getTime();
        return (int) (diff / 1000);
    }

    public Time dateDiff(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        Time time = new Time((int) diffHours, (int) diffMinutes, (int) diffSeconds);
        return time;
    }

    public String decimalFormat(Object value) {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(value);
    }

    public Date convertStringToDate(String format, String fecha) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(fecha);
    }

}
