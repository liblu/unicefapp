/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btree.saneamiento.util;

/**
 *
 * @author Diana Mejía
 */
public enum WeekDay {    
    Lunes(1),
    Martes(2),
    Miercoles(3),
    Jueves(4),
    Viernes(5),
    Sabado(6),
    Domingo(7);
    public final int label;
 
    private WeekDay(int value) {
        this.label = value;
    }
    
}
