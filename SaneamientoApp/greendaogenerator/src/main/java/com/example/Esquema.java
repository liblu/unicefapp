package com.example;


import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.ToMany;
import org.greenrobot.greendao.generator.ToOne;

public class Esquema {
    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(15, "com.btree.unicef.saneamiento.model.entities_generated");
        schema.enableActiveEntitiesByDefault();
        schema.enableKeepSectionsByDefault();
        schema.setDefaultJavaPackageDao("com.btree.unicef.saneamiento.model.daos");

        //-------------nuevo
        createDatosSession(schema);
        createPlanilla(schema);
       // createComponentes(schema);

        new DaoGenerator().generateAll(schema, ".");
    }



    private static void createPlanilla(Schema schema){
        Entity grupo=schema.addEntity("GrupoRes");
        grupo.addLongProperty("grupoid").primaryKey();
        grupo.addStringProperty("descripcion");

        Entity pregunta = schema.addEntity("PreguntaRes");
        pregunta.addLongProperty("preguntaid").primaryKey();
        pregunta.addStringProperty("descripcion");
        pregunta.addStringProperty("tiporespuesta");
        pregunta.addStringProperty("grupoDes");
        pregunta.addLongProperty("planillaid");
        pregunta.addStringProperty("planillaDes");

        Property grupoid = pregunta.addLongProperty("grupoid").notNull().getProperty();
        ToMany grupoToPregunta= grupo.addToMany(pregunta,grupoid);
        ToOne preguntaToGrupo = pregunta.addToOne(grupo,grupoid);

        Entity ruta = schema.addEntity("RutaRes");
        ruta.addLongProperty("rutadetid").primaryKey();
        ruta.addLongProperty("rutaid");
        ruta.addLongProperty("monitorid");
        ruta.addStringProperty("descripcion");
        ruta.addIntProperty("orden");
        ruta.addLongProperty("beneficiarioid");
        ruta.addStringProperty("beneficiarioDes");
        ruta.addStringProperty("estado");
        ruta.addLongProperty("evaluacionid");
        ruta.addStringProperty("evaluacionobs");
        ruta.addStringProperty("picture");
        ruta.addIntProperty("itemsCount");
        ruta.addStringProperty("ulastupdate");
        ruta.addLongProperty("planillaid");

        Entity respuesta = schema.addEntity("Respuesta");
        respuesta.addIdProperty().primaryKey().autoincrement();
        respuesta.addBooleanProperty("respuesta");
        respuesta.addStringProperty("respuesttext");
        respuesta.addIntProperty("respuestavalue");
        respuesta.addLongProperty("evaluacionid");
        respuesta.addLongProperty("preguntaid");



        Entity beneficiario = schema.addEntity("BeneficiarioRes");
        beneficiario.addLongProperty("beneficiarioid").primaryKey();
        beneficiario.addStringProperty("nombre");
        beneficiario.addStringProperty("direccion");
        beneficiario.addBooleanProperty("autorizamonitoreo");
        beneficiario.addBooleanProperty("programaproteccion");
        beneficiario.addIntProperty("codigoepsa");
        beneficiario.addIntProperty("distrito");
        beneficiario.addDoubleProperty("lat");
        beneficiario.addDoubleProperty("lon");

        Entity servicio = schema.addEntity("ServicioRes");
        servicio.addLongProperty("servicioid").primaryKey();
        servicio.addStringProperty("nombre");
        servicio.addStringProperty("observacion");
        servicio.addBooleanProperty("especificarcantidad");

        Entity serviciopeticion = schema.addEntity("Serviciopeticion");
        serviciopeticion.addIdProperty().primaryKey().autoincrement();
        serviciopeticion.addLongProperty("servicioid");
        serviciopeticion.addStringProperty("observacion");
        serviciopeticion.addIntProperty("cantidad");
        serviciopeticion.addLongProperty("evaluacionid");
    }

    private static void createDatosSession(Schema schema) {
        Entity datosSession= schema.addEntity("DatosSesion");
        datosSession.setSuperclass("MResponse");
        datosSession.addImport("com.btree.unicef.saneamiento.service.restbase.MResponse");
        datosSession.addLongProperty("usuarioid").primaryKey();
        datosSession.addStringProperty("user");
        datosSession.addStringProperty("nombre");
        datosSession.addStringProperty("password");
        datosSession.addDateProperty("lastLogin");
        datosSession.addDateProperty("lastUpdate");
        datosSession.addLongProperty("monitorId");

    }

}
