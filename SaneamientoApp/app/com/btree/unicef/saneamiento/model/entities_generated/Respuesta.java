package com.btree.unicef.saneamiento.model.entities_generated;

import org.greenrobot.greendao.annotation.*;

import com.btree.unicef.saneamiento.model.daos.DaoSession;
import org.greenrobot.greendao.DaoException;

import com.btree.unicef.saneamiento.model.daos.RespuestaDao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "RESPUESTA".
 */
@Entity(active = true)
public class Respuesta {

    @Id(autoincrement = true)
    private Long id;
    private Boolean respuesta;
    private String respuesttext;
    private Integer respuestavalue;
    private Long evaluacionid;
    private Long preguntaid;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient RespuestaDao myDao;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public Respuesta() {
    }

    public Respuesta(Long id) {
        this.id = id;
    }

    @Generated
    public Respuesta(Long id, Boolean respuesta, String respuesttext, Integer respuestavalue, Long evaluacionid, Long preguntaid) {
        this.id = id;
        this.respuesta = respuesta;
        this.respuesttext = respuesttext;
        this.respuestavalue = respuestavalue;
        this.evaluacionid = evaluacionid;
        this.preguntaid = preguntaid;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRespuestaDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuesttext() {
        return respuesttext;
    }

    public void setRespuesttext(String respuesttext) {
        this.respuesttext = respuesttext;
    }

    public Integer getRespuestavalue() {
        return respuestavalue;
    }

    public void setRespuestavalue(Integer respuestavalue) {
        this.respuestavalue = respuestavalue;
    }

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Long getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Long preguntaid) {
        this.preguntaid = preguntaid;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
