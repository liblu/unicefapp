package com.btree.unicef.saneamiento.model.entities_generated;

import org.greenrobot.greendao.annotation.*;

import com.btree.unicef.saneamiento.model.daos.DaoSession;
import org.greenrobot.greendao.DaoException;

import com.btree.unicef.saneamiento.model.daos.ServicioDao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "SERVICIO".
 */
@Entity(active = true)
public class Servicio {

    @Id
    private Long servicioid;
    private String nombre;
    private String observacion;
    private Boolean especificarcantidad;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient ServicioDao myDao;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public Servicio() {
    }

    public Servicio(Long servicioid) {
        this.servicioid = servicioid;
    }

    @Generated
    public Servicio(Long servicioid, String nombre, String observacion, Boolean especificarcantidad) {
        this.servicioid = servicioid;
        this.nombre = nombre;
        this.observacion = observacion;
        this.especificarcantidad = especificarcantidad;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getServicioDao() : null;
    }

    public Long getServicioid() {
        return servicioid;
    }

    public void setServicioid(Long servicioid) {
        this.servicioid = servicioid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Boolean getEspecificarcantidad() {
        return especificarcantidad;
    }

    public void setEspecificarcantidad(Boolean especificarcantidad) {
        this.especificarcantidad = especificarcantidad;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
