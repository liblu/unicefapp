package com.btree.unicef.saneamiento.interfaces;
/**
 * Created by Diana Mejia on 31/03/2016.
 */
public interface IDialogOkCancel extends IDialogConfirmar {
	void cancelar();
}
