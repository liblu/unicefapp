package com.btree.unicef.saneamiento.model.enums;

/**
 * Created by Diana Mejia on 19/04/2016.
 */
public enum StateAlert {
    ERROR, SUCCESSFUL,WARNING,TITLE;
}
