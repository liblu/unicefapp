package com.btree.unicef.saneamiento.interfaces;

public interface IImagen {
	
	String getDirImagen();
	int getNroCarpeta();

}
