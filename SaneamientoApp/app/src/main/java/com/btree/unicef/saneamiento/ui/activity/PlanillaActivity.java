package com.btree.unicef.saneamiento.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.PreguntaBl;
import com.btree.unicef.saneamiento.bussiness.RespuestaBl;
import com.btree.unicef.saneamiento.bussiness.RutaBl;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.service.rest.ApiManager;
import com.btree.unicef.saneamiento.service.rest.response.ResponseListener;
import com.btree.unicef.saneamiento.ui.adapters.EvaluacionPageAdapter;
import com.btree.unicef.saneamiento.utils.KeyUtils;
import com.btree.unicef.saneamiento.utils.XMLReference;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;

/**
 * Created by Diana Mejia
 */
public class PlanillaActivity extends BaseActivity implements View.OnClickListener {



    @XMLReference(idRes = R.id.lbl_ruta)
    private TextView lblRuta;

    @XMLReference(idRes = R.id.lbl_beneficiario)
    private TextView lblBeneficiario;

    @XMLReference(idRes = R.id.tabLayout)
    private TabLayout tabLayout;
    @XMLReference(idRes = R.id.btn_aceptar)
    private Button btnAceptar;
    @XMLReference(idRes = R.id.btn_cancel)
    private Button btnCancelar;
    @XMLReference(idRes = R.id.switch1)
    private Switch monitoreoPosible;

    @XMLReference(idRes = R.id.pager)
    private ViewPager2 viewPager;

    private RutaBl rutaBl;
    private RespuestaBl respuestaBl;
    private PreguntaBl preguntaBl;
    private EvaluacionPageAdapter evalAdapter;
    private RutaRes rutaSelected;

    @Override
    public int getLayoutId() {
        return R.layout.activity_planila;
    }

    @Override
    public void inicializarVariables() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rutaBl=new RutaBl();
        respuestaBl=new RespuestaBl();
        preguntaBl=new PreguntaBl();
    }


    @Override
    public void instanciarIGU() {
        btnAceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        Long idRuta = getIntent().getLongExtra(KeyUtils.KEY_RUTA, -1);
        rutaSelected= rutaBl.getRuta(idRuta);
        if(rutaSelected==null) {
            return;
        }
            lblBeneficiario.setText(rutaSelected.getBeneficiarioDes());
            lblRuta.setText(rutaSelected.getDescripcion());
        evalAdapter=new EvaluacionPageAdapter(PlanillaActivity.this,rutaSelected.getEvaluacionid());
        viewPager.setAdapter(evalAdapter);

        TabLayoutMediator mediator= new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position){
                    case 0:{
                        tab.setText("Preguntas");
                        tab.setIcon(R.drawable.ic_planilla);
                        break;
                    }
                    case 1:{
                        tab.setText("Servicios");
                        tab.setIcon(R.drawable.ic_servicio);
                        break;
                    }
                }
            }
        });
        mediator.attach();

        viewPager.setAdapter(evalAdapter);
    }

    @Override
    public String getToolbarTitle() {
        return "Planilla";
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar:
                Integer respuestas= respuestaBl.countRespuesta(rutaSelected.getEvaluacionid());
                Integer preguntas=preguntaBl.countPreguntas();
                if(!monitoreoPosible.isChecked()){

                    return;
                }
                if(monitoreoPosible.isChecked() && respuestas.equals(preguntas) ) {
                    ApiManager.getInstance(PlanillaActivity.this).evaluacionRegister(rutaSelected, new ResponseListener() {
                        @Override
                        public void onSuccess(Object o) {
                            Intent intent = new Intent(PlanillaActivity.this, RutaActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });
                }else{
                    showAlertWaring("Debe rellenar las preguntas antes de enviar");
                }
                break;

            case  R.id.btn_cancel:
                onBackPressed();
                break;
        }
    }


}
