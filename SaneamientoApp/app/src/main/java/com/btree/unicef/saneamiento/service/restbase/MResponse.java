package com.btree.unicef.saneamiento.service.restbase;

/**
 * Created by Diana Mejia on 25/02/2018.
 */

public class MResponse {
    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }
}
