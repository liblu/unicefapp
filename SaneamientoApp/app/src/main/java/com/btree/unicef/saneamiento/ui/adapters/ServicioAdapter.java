package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.ServiciopeticionBl;
import com.btree.unicef.saneamiento.model.entities_generated.ServicioRes;
import com.btree.unicef.saneamiento.model.entities_generated.Serviciopeticion;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class ServicioAdapter extends RecyclerView.Adapter<ServicioAdapter.VHIComponente> {

    private List<ServicioRes> servicioList;

    private Context context;
    private View.OnClickListener onClickListener;
    private Switch.OnCheckedChangeListener checkedListener;
    private Long evaluacionid;
    private ServiciopeticionBl serviciopeticionBl;

    public ServicioAdapter(Context context, Long evaluacionid, List<ServicioRes> list) {
        this.context = context;
        this.servicioList = list;
        this.evaluacionid=evaluacionid;
        this.serviciopeticionBl=new ServiciopeticionBl();
    }

    public void setListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setCheckedListener(RadioButton.OnCheckedChangeListener listener){
        this.checkedListener=listener;
    }

    @Override
    public VHIComponente onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_servicio, parent, false);
        return new VHIComponente(v);
    }

    @Override
    public void onBindViewHolder(final VHIComponente holder, int position) {
        ServicioRes item = servicioList.get(position);
        Serviciopeticion serviciopeticion=serviciopeticionBl.getByEvaluacionServicio(evaluacionid,item.getServicioid());
        holder.lblNombre.setText(item.getNombre());
        if(item.getEspecificarcantidad()){
            holder.ly_cantidad.setVisibility(View.VISIBLE);
            holder.rbSelect.setVisibility(View.GONE);
            if(serviciopeticion!=null){
                holder.lblCantidad.setText(String.valueOf(serviciopeticion.getCantidad()));
            }else{
                holder.lblCantidad.setText("-");
            }

            holder.btnAddCantidad.setOnClickListener(onClickListener);
            holder.btnAddCantidad.setTag(item.getServicioid());
        }else{
            holder.rbSelect.setVisibility(View.VISIBLE);
            holder.ly_cantidad.setVisibility(View.GONE);
            holder.rbSelect.setChecked(serviciopeticion!=null);
            holder.rbSelect.setOnCheckedChangeListener(checkedListener);
            holder.rbSelect.setTag(item.getServicioid());
        }

    }

    @Override
    public int getItemCount() {
        return servicioList.size();
    }



    public void myNotifyDataSetChanged(){
        notifyDataSetChanged();

    }


    public static class VHIComponente extends RecyclerView.ViewHolder  {
        TextView lblNombre;
        Switch rbSelect;
        TextView lblCantidad;
        ImageButton btnAddCantidad;
        LinearLayout ly_cantidad;

        public VHIComponente(View itemView) {
            super(itemView);

            lblNombre = (TextView) itemView.findViewById(R.id.lbl_nombre);
            rbSelect = (Switch) itemView.findViewById(R.id.rb_resp);
            lblCantidad = (TextView) itemView.findViewById(R.id.lbl_cant);
            btnAddCantidad = (ImageButton) itemView.findViewById(R.id.btn_add_cantidad);
            ly_cantidad= (LinearLayout) itemView.findViewById(R.id.ly_cantidad);

        }


    }
}
