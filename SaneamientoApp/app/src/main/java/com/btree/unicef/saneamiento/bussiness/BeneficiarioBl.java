package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.BeneficiarioResDao;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.entities_generated.BeneficiarioRes;

import java.util.List;

/**
 * Created by Diana Mejia.
 */

public class BeneficiarioBl extends ObjetoBLBase<BeneficiarioResDao> {

    public BeneficiarioBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected BeneficiarioResDao getDao(DaoSession daoSession) {
        return daoSession.getBeneficiarioResDao();
    }

    public List<BeneficiarioRes> getBeneficiarios() {
        return getDefaultDao().loadAll();
    }

    /*public BeneficiarioRes getComponente(long keyComp) {
        return getDefaultDao().queryBuilder().where(Properties.Id.eq(keyComp)).unique();
    }

    public Componente getComponente(long tipoId, long compId) {
        return getDefaultDao().queryBuilder().where(Properties.ComponenteId.eq(compId), Properties.TipoComponenteId.eq(tipoId)).unique();
    }

    public  List<Componente> getComponenteByTipo(long tipo){
        return getDefaultDao().queryBuilder().where(Properties.TipoComponenteId.eq(tipo)).list();
    }*/
}
