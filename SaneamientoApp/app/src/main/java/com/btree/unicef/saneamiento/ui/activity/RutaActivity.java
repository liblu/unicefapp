package com.btree.unicef.saneamiento.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.RutaBl;
import com.btree.unicef.saneamiento.interfaces.IBusqueda;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.ui.adapters.RutaAdapter;
import com.btree.unicef.saneamiento.utils.KeyUtils;
import com.btree.unicef.saneamiento.utils.XMLReference;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class RutaActivity extends BaseActivity implements View.OnClickListener, IBusqueda {

    @XMLReference(idRes = R.id.rvCliente)
    private RecyclerView rvCliente;
    private RutaAdapter rutaAdapter;
    @XMLReference(idRes = R.id.lbl_cant_clientes)
    private TextView lblCantidad;
    Boolean refreshing = false;
    List<RutaRes> rutaList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_ruta;
    }

    @Override
    public void inicializarVariables() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSearchListener(this);
    }


    @Override
    public void instanciarIGU() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rutaAdapter = new RutaAdapter(this, rutaList, lblCantidad);
        rutaAdapter.setListener(this);
        cargarRutas();
        rvCliente.setAdapter(rutaAdapter);
        rvCliente.setLayoutManager(layoutManager);
      // fbRegistrarCliente.setOnClickListener(this);

    }

    @Override
    public String getToolbarTitle() {
        return "Rutas";
    }

    private void cargarRutas() {
        rutaList.clear();
        rutaList.addAll(new RutaBl().getOrdenesVisita());
        rutaAdapter.myNotifyDataSetChanged();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.fb_new_client:
//                startActivity(new Intent(this, PuntoControlRegistrarActivity.class));
//                break;
            case R.id.btn_opt_planilla:
                if(v.getTag()!= null){
                    Long idItem= (Long) v.getTag();
                    Intent intent= new Intent(RutaActivity.this,PlanillaActivity.class);
                    intent.putExtra(KeyUtils.KEY_RUTA,idItem);
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void newQuery(String criterio) {
        rutaAdapter.getFilter().filter(criterio);
    }
}
