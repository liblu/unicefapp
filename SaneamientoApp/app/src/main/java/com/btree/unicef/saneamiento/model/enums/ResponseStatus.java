package com.btree.unicef.saneamiento.model.enums;

/**
 * Created by Diana Mejia on 02/08/2017.
 */

public enum  ResponseStatus {
    SUCCESSFULL(1),ERROR(2),NO_UPDATES(3),ERROR_LOGICO(4);

    private Integer status;
    ResponseStatus(Integer status) {
        this.status=status;
    }

    public Integer id() {
        return status;
    }
}
