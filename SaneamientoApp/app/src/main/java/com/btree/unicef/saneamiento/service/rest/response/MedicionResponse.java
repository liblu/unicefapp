package com.btree.unicef.saneamiento.service.rest.response;

/**
 * Created by Diana Mejia on 16/07/2018.
 */

public class MedicionResponse {
    Double medida;
    Double medMax;
    Double medMin;
    String fechaHra;

    public MedicionResponse() {
    }



    public MedicionResponse(String fechaHra,Double medida, Double medMax, Double medMin) {
        this.medida = medida;
        this.medMax = medMax;
        this.medMin = medMin;
        this.fechaHra = fechaHra;
    }


    public Double getMedida() {
        return medida;
    }

    public void setMedida(Double medida) {
        this.medida = medida;
    }

    public Double getMedMax() {
        return medMax;
    }

    public void setMedMax(Double medMax) {
        this.medMax = medMax;
    }

    public Double getMedMin() {
        return medMin;
    }

    public void setMedMin(Double medMin) {
        this.medMin = medMin;
    }

    public String getFechaHra() {
        return fechaHra;
    }

    public void setFechaHra(String fechaHra) {
        this.fechaHra = fechaHra;
    }


}
