package com.btree.unicef.saneamiento.service.location;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Diana Mejia on 01/11/2016.
 */

public interface TrazarRutaListener {
    void trazarRutaSuccess(List<LatLng> pointList, CameraUpdate cu, String distancia);
    void trazarRutaFailure(String mensaje);
}
