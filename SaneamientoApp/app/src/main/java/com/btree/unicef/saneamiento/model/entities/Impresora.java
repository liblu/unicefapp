package com.btree.unicef.saneamiento.model.entities;

/**
 * Created by Diana Mejia on 08/08/2016.
 */
public class Impresora {
    String name;
    String mac;

    public Impresora(String name, String mac) {
        this.name = name;
        this.mac = mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
