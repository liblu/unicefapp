package com.btree.unicef.saneamiento.utils;

/**
 * Created by Diana Mejia
 * */

public enum EvaluacionStatus {
    ENABLE,
    SUCCESS,
    QUEUE,
}
