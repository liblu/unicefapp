package com.btree.unicef.saneamiento.ui.dialog;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.interfaces.IDialogOkCancel;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia on 15/04/2016.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ClienteInfoDialog extends DialogFragment implements View.OnClickListener {

    private IDialogOkCancel listener;
    private static final String key_object="key_object";
   // private Cliente cliente;

    private TextView lblError;
  //  VisitaProgramada rutaCliente;

    public static ClienteInfoDialog showDialogCliente(Long idCliente, IDialogOkCancel listener) {
        ClienteInfoDialog actualDialog = new ClienteInfoDialog();
        actualDialog.setCancelable(false);
        actualDialog.setListener(listener);
        actualDialog.setStyle(STYLE_NO_FRAME, R.style.Dialog_No_Border);
        Bundle bundle= new Bundle();
        bundle.putLong(ClienteInfoDialog.key_object,idCliente);
        actualDialog.setArguments(bundle);
        return actualDialog;
    }

    public void setListener(IDialogOkCancel listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        View v = inflater.inflate(R.layout.dialog_info_cliente, container, false);
        View btnComoLlegar=v.findViewById(R.id.btn_como_llegar);
        View btnLlamar=v.findViewById(R.id.btn_llamar);
        View btnPedido=v.findViewById(R.id.btn_pedido);
        v.findViewById(R.id.btn_cerrar).setOnClickListener(this);
        lblError= (TextView) v.findViewById(R.id.lbl_error);
        btnComoLlegar.setOnClickListener(this);
        btnLlamar.setOnClickListener(this);
        btnPedido.setOnClickListener(this);

        TextView lblTitle= (TextView) v.findViewById(R.id.lbl_title_dialog);
        TextView lblDireccion= (TextView) v.findViewById(R.id.lbl_direccion);
        TextView lblLlamar= (TextView) v.findViewById(R.id.lbl_llamar);
        ImageView imgRealizarPedido= (ImageView) v.findViewById(R.id.img_realizar_pedido);
        Long idCliente = getArguments().getLong(key_object);
//        cliente= new ClienteBl().getCliente(idCliente);
//        lblLlamar.setText("Llamar al: "+ cliente.getMovil());
//        lblTitle.setText(cliente.getClienteDes());
//        lblDireccion.setText(cliente.getDireccion());
//
//        rutaCliente= new VisitasBl(getActivity()).getVisita(idCliente);
//        imgRealizarPedido.setImageResource(rutaCliente.getLlego()?R.mipmap.paste:R.mipmap.paste_disable);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cerrar:
                dismiss();
                break;

            case R.id.btn_como_llegar:
                listener.aceptar(null);
                dismiss();
                break;

            case R.id.btn_pedido:
//                if(rutaCliente.getLlego()) {
//                    listener.cancelar();
//                    dismiss();
//                }else{
//                    showError("Para atender al cliente registre su llegada..");
//                }
                break;

            case R.id.btn_llamar:
              //  Utils.llamarTelefono(getActivity(),cliente.getMovil());
                dismiss();
                break;
        }
    }

    private void showError(String error) {
        lblError.setText(error);
        lblError.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.GONE);
            }
        }, 3000);
    }
}

