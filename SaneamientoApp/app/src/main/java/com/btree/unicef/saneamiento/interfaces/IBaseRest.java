package com.btree.unicef.saneamiento.interfaces;


/**
 * Created by Diana Mejia
 */
public interface IBaseRest {
    void onFinish(Object rest);

    void onFailure( IDialogConfirmar listener, String mensaje);

    void onErrorLogico(IDialogConfirmar listener,String mensaje);

    void showProgressDialog(Object clases, CharSequence mensaje);
}
