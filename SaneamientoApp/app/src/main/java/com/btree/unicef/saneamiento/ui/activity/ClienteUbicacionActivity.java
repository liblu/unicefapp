package com.btree.unicef.saneamiento.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.view.View;
import android.widget.Button;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.utils.XMLReference;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.core.content.ContextCompat;


public class ClienteUbicacionActivity extends BaseActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationChangeListener, GoogleMap.OnMapLongClickListener {

    private GoogleMap mMap;

    private static final LatLng SCZ_VIEW = new LatLng(-17.783848596044916, -63.1809747558593);
    private static final int ZOOM = 11;
    private LatLng position;
   // private Cliente cliente;
    @XMLReference(idRes = R.id.btn_ubicacion)
    Button btnUbicacion;

    @Override
    public int getLayoutId() {
        return R.layout.activity_cliente_ubicacion;
    }

    @Override
    public void inicializarVariables() {


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void instanciarIGU() {
        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position != null) {
                        Intent intent = new Intent();
                      //  intent.putExtra(PuntoControlRegistrarActivity.KEY_LOCATION, position);
                        setResult(RESULT_OK, intent);
                        finish();

                } else {
                    showAlertWaring("Debe marcar una ubicacion en el mapa...");
                }
            }
        });
    }

    @Override
    public String getToolbarTitle() {
        return "Ubicacion ";
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);
        enableMyLocation();

        mMap.setOnMyLocationChangeListener(this);

        situarMapa(SCZ_VIEW, ZOOM);

//        if (cliente != null && cliente.getPosLat() != null && cliente.getPosLat() != 0) {
//            LatLng latLng = new LatLng(cliente.getPosLat(), cliente.getPosLon());
//            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource(EstadoComp.ACTUAL.getImageResourse());
//            mMap.addMarker(new MarkerOptions().position(latLng).title(cliente.getClienteDes()).icon(bitmapDescriptor));
//        }
    }


    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMyLocationChange(Location location) {
        LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
        position = sydney;
        mMap.addMarker(new MarkerOptions().position(sydney).title("Aqui estoy"));
        situarMapa(sydney, 15);
        mMap.setOnMyLocationChangeListener(null);
    }

    private void situarMapa(LatLng place, int zoom) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place).zoom(zoom).bearing(0).tilt(0).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        position = latLng;
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).title("Punto de Control..."));
        situarMapa(latLng, 15);
    }


}
