package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;

import java.util.List;


/**
 * Created by Diana Mejia
 */
public class GrupoAdapter extends BaseExpandableListAdapter {
   private List<GrupoRes> grupoResList;
    private final LayoutInflater inflater;
    View.OnClickListener onClickListener;
    public GrupoAdapter(Context context, List<GrupoRes> list ) {
        this.inflater = LayoutInflater.from(context);
        this.grupoResList =list;
    }

    public void setListener(View.OnClickListener listener){
        onClickListener=listener;
    }

    @Override
    public int getGroupCount() {
        return grupoResList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return grupoResList.get(i).getPreguntaResList().size();
    }

    @Override
    public Object getGroup(int i) {
        return grupoResList.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return grupoResList.get(groupPosition).getPreguntaResList().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        View resultView = view;
        ViewHolder holder;

        if (resultView == null) {
            resultView = inflater.inflate(R.layout.card_grupo, null);
            holder = new ViewHolder();
            holder.lblNombre = (TextView) resultView.findViewById(R.id.lblNombreGrupo);
            resultView.setTag(holder);
        } else {
            holder = (ViewHolder) resultView.getTag();
        }

        final GrupoRes item = (GrupoRes) getGroup(i);

        holder.lblNombre.setText(item.getDescripcion());

        return resultView;

    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        View resultView = view;
        ViewChildHolder holder;

        if (resultView == null) {
            resultView = inflater.inflate(R.layout.card_planilla_model, null);
            holder = new ViewChildHolder();
            holder.lblpregunta = (TextView) resultView.findViewById(R.id.lbl_pregunta);
            holder.lblTipo=(TextView)resultView.findViewById(R.id.lbl_resp);
            resultView.setTag(holder);
        } else {
            holder = (ViewChildHolder) resultView.getTag();
        }

        final PreguntaRes item = (PreguntaRes) getChild(i,i1);

        holder.lblpregunta.setText(item.getDescripcion());
        String tipoResp=item.getTiporespuesta().equals("B")?"SI/NO":item.getTiporespuesta().equals("T")?"TEXTO":"NUMERICO";
        holder.lblTipo.setText(tipoResp);
        return resultView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {

        TextView lblNombre;


    }

    private class ViewChildHolder{
        TextView lblpregunta;
        TextView lblTipo;
    }

}
