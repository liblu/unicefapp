package com.btree.unicef.saneamiento.service.rest.request;

import com.btree.unicef.saneamiento.model.entities_generated.Respuesta;

/**
 * Created by Diana Mejia
 */
public class RespuestaRes {

    private Boolean respuesta;
    private String respuesttext;
    private Integer respuestavalue;
    private Long evaluacionid;
    private Long preguntaid;

    public RespuestaRes(Respuesta item){

        this.respuesta=item.getRespuesta();
        this.respuestavalue=item.getRespuestavalue();
        this.respuesttext=item.getRespuesttext();
        this.evaluacionid=item.getEvaluacionid();
        this.preguntaid=item.getPreguntaid();

    }



    public Boolean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public String getRespuesttext() {
        return respuesttext;
    }

    public void setRespuesttext(String respuesttext) {
        this.respuesttext = respuesttext;
    }

    public Integer getRespuestavalue() {
        return respuestavalue;
    }

    public void setRespuestavalue(Integer respuestavalue) {
        this.respuestavalue = respuestavalue;
    }

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Long getPreguntaid() {
        return preguntaid;
    }

    public void setPreguntaid(Long preguntaid) {
        this.preguntaid = preguntaid;
    }
}
