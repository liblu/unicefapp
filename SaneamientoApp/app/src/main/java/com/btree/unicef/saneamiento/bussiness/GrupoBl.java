package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.GrupoResDao;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.RutaResDao;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;

import java.util.List;

/**
 * Created by Diana Mejia
 */

public class GrupoBl extends ObjetoBLBase<GrupoResDao> {

    public GrupoBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected GrupoResDao getDao(DaoSession daoSession) {
        return daoSession.getGrupoResDao();
    }

    public List<GrupoRes> findByRuta(Long rutaid){
        return getDefaultDao().queryBuilder().where(RutaResDao.Properties.Rutaid.eq(rutaid)).list();

    }
    /*public Medicion getMedicion(long movilId) {
        return getDefaultDao().queryBuilder().where(MedicionDao.Properties.MovilId.eq(movilId)).unique();
    }*/
}
