package com.btree.unicef.saneamiento.utils;

/**
 * Created by Diana Mejia on 09/08/2016.
 */
public class KeyUtils {
    public static final String KEY_PRODUCTO = "KEY_PRODUCTO";
    public static final String KEY_GALERIA_MODO_LISTA="KEY_GALERIA_MODO_LISTA";
    public static final String KEY_LINEA = "KEY_LINEA";
    public static String KEY_PRODUCTOS_LIST = "KEY_PRODUCTOS_LIST";
    public static final String KEY_SCREEN_ORIGIN = "KEY_SCREEN_ORIGIN";
    public static final String KEY_PRECIOS_LIST="KEY_PRECIOS_LIST";
    public static final String KEY_PRECIO_DEFAULT="KEY_PRECIO_DEFAULT";
    public static final String JSON_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss";
    public static final String KEY_PROCESAR_PEDIDO="KEY_PROCESAR_PEDIDO";
    public static final String KEY_RUTA ="KEY_CLIENTE";

    public static final int REQUEST_PRINTER = 2000;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
}
