package com.btree.unicef.saneamiento.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.interfaces.IDialogOkCancel;
import com.btree.unicef.saneamiento.model.enums.StateAlert;

/**
 * Created by Diana Mejia on 29/08/2016.
 */
public class AppDialog {

    public static Dialog showAlert(Context context, final IDialogConfirmar listener, StateAlert state, CharSequence title, CharSequence mensaje) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.alert_dialog);


        TextView lblTitle = (TextView) dialog.findViewById(R.id.lbl_title_dialog);
        TextView lblMensaje = (TextView) dialog.findViewById(R.id.lbl_mensaje_dialog);
        ImageView iconDialog = (ImageView) dialog.findViewById(R.id.img_aviso);
        Button btnAceptar = (Button) dialog.findViewById(R.id.btn_aceptar);

        lblTitle.setText(title);
        lblMensaje.setText(mensaje);
        if (state == StateAlert.ERROR) {
            iconDialog.setImageResource(R.mipmap.close);
        } else if (state == StateAlert.SUCCESSFUL) {
            iconDialog.setImageResource(R.mipmap.check);
        } else if (state == StateAlert.WARNING) {
            iconDialog.setImageResource(R.mipmap.warning);
        } else {
            iconDialog.setVisibility(View.GONE);
        }


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.aceptar("");
                }
                dialog.dismiss();
            }
        });

        if (listener instanceof IDialogOkCancel) {
            Button btnCancelar = (Button) dialog.findViewById(R.id.btn_cancelar);
            btnCancelar.setVisibility(View.VISIBLE);
            btnCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        ((IDialogOkCancel) listener).cancelar();
                    }
                    dialog.dismiss();
                }
            });
        }
        return dialog;
    }
}
