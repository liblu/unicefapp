package com.btree.unicef.saneamiento.ui.activity;

import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.GrupoBl;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;
import com.btree.unicef.saneamiento.ui.adapters.GrupoAdapter;
import com.btree.unicef.saneamiento.utils.XMLReference;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class PlanillaModelActivity extends BaseActivity implements View.OnClickListener {

    @XMLReference(idRes = R.id.lvPlanilla)
    private ExpandableListView lvPlanilla;
    @XMLReference(idRes = R.id.lbl_cant_clientes)
    private TextView lblCantClientes;
    Boolean refreshing = false;
    List<GrupoRes> grupoResList = new ArrayList<>();
    private GrupoAdapter grupoAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_planila_model;
    }

    @Override
    public void inicializarVariables() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        grupoResList=new GrupoBl().getDefaultDao().loadAll();
        for(GrupoRes item:grupoResList){
            List<PreguntaRes> preguntas=item.getPreguntaResList();
            String cant="";
        }

    }


    @Override
    public void instanciarIGU() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        grupoAdapter = new GrupoAdapter(this, grupoResList);
        grupoAdapter.setListener(this);

        lvPlanilla.setAdapter(grupoAdapter);
        grupoAdapter.notifyDataSetChanged();

    }

    @Override
    public String getToolbarTitle() {
        return "Formato Planilla";
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.fb_new_client:
//                startActivity(new Intent(this, PuntoControlRegistrarActivity.class));
//                break;
            case R.id.btn_opt_planilla:
//                if(v.getTag()!= null){
//                    Long idCliente= (Long) v.getTag();
//                    Intent intent= new Intent(ComponentesActivity.this,ClienteUbicacionActivity.class);
//                    intent.putExtra(KeyUtils.KEY_CLIENTE,idCliente);
//                    startActivity(intent);
//                }
                break;

            case  R.id.btn_opt_ubicacion:
                break;
        }
    }


}
