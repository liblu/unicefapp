package com.btree.unicef.saneamiento.service.rest.response;

import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.service.restbase.MResponse;

import java.util.List;

/**
 * Created by Diana Mejia
 */
public class RutaResponse extends MResponse {

    List<RutaRes> rutas;
    public List<RutaRes> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaRes> rutas) {
        this.rutas = rutas;
    }


}
