package com.btree.unicef.saneamiento.service.rest;

import android.os.AsyncTask;
import android.util.Log;

import com.btree.unicef.saneamiento.bussiness.BeneficiarioBl;
import com.btree.unicef.saneamiento.bussiness.GrupoBl;
import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.bussiness.PreguntaBl;
import com.btree.unicef.saneamiento.bussiness.RutaBl;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.model.enums.ResponseStatus;
import com.btree.unicef.saneamiento.service.rest.request.SincronizacionRequest;
import com.btree.unicef.saneamiento.service.rest.response.SincronizacionResponse;
import com.btree.unicef.saneamiento.service.restbase.RestBuilder;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Diana Mejia
 */

public class UtilsDownloadAsyncTask extends AsyncTask<Void, Long, Long> {

    private String responsemsg;

    @Override
    protected Long doInBackground(Void... params) {
        try {
            ParametersBl parametersBl=new ParametersBl();
            DatosSesion datosSesion=parametersBl.getDatosSession();
            SincronizacionRequest request= new SincronizacionRequest();
           // request.setDtLastSync(datosSesion.getLastUpdate());
            request.setMonitorId(datosSesion.getMonitorId());

            Call<SincronizacionResponse> call = RestBuilder.getDataService().downloadData(request);
            Response<SincronizacionResponse> response = call.execute();
            if(response.isSuccessful()){
                SincronizacionResponse dtResponse= response.body();
                if(dtResponse.getStatus()== ResponseStatus.SUCCESSFULL.id()){
                    BeneficiarioBl beneficiarioBl= new BeneficiarioBl();
                    beneficiarioBl.getDefaultDao().deleteAll();
                    beneficiarioBl.getDefaultDao().insertInTx(dtResponse.getBeneficiarios());
                    Log.i("Beneficiarios ","Succefull");

                    PreguntaBl preguntaBl=new PreguntaBl();
                    preguntaBl.getDefaultDao().deleteAll();
                    preguntaBl.getDefaultDao().insertInTx(dtResponse.getPreguntas());
                    Log.i("Preguntas ","Succefull");

                    RutaBl rutaBl=new RutaBl();
                    rutaBl.getDefaultDao().deleteAll();
                    rutaBl.getDefaultDao().insertInTx(dtResponse.getRutas());
                    Log.i("Rutas ","Succefull");

                    GrupoBl grupoBl=new GrupoBl();
                    grupoBl.getDefaultDao().deleteAll();
                    grupoBl.getDefaultDao().insertInTx(dtResponse.getGrupos());
                    Log.i("Grupos ","Succefull");
                    return 1L;
                }else{
                    responsemsg= "lng: "+dtResponse.getMessage();
                    return -1L;
                }
            }else{
                responsemsg="lng: "+response.message();
                return -1L;
            }




        } catch (Exception e) {
            Log.e("Error en descarga", e.getMessage());
        }
        return -1L;
    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
        if (aLong == 1L) {
            Log.i("Descarga finalizada", "descarga exitosa----------------------");
        } else {
            Log.e("Descarga finalizada", "descarga finalizada con errores----------------------");
        }
    }
}
