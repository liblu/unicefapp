package com.btree.unicef.saneamiento.model.entities;


public class ConfigMovil {
    private Long VentaParaId;
    private Double MontoMaxVtaNit;
    private boolean PedVentaRecDesc;
    private Integer Tracking;
    private Boolean ImprimirPedido;

    private String HoraIni;
    private String HoraFin;

    public String getHoraFin() {
        return HoraFin;
    }

    public String getHoraIni() {
        return HoraIni;
    }

    public Long getVentaParaId() {
        return VentaParaId;
    }

    public Double getMontoMaxVtaNit() {
        return MontoMaxVtaNit;
    }

    public Integer getTracking() {
        return Tracking;
    }

    public boolean isPedVentaRecDesc() {
        return PedVentaRecDesc;
    }

    public Boolean getImprimirPedido() {
        return ImprimirPedido;
    }
}
