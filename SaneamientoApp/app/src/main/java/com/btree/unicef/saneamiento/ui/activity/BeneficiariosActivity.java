package com.btree.unicef.saneamiento.ui.activity;

import android.view.View;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.BeneficiarioBl;
import com.btree.unicef.saneamiento.interfaces.IBusqueda;
import com.btree.unicef.saneamiento.model.entities_generated.BeneficiarioRes;
import com.btree.unicef.saneamiento.ui.adapters.BeneficiarioAdapter;
import com.btree.unicef.saneamiento.utils.XMLReference;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class BeneficiariosActivity extends BaseActivity implements View.OnClickListener, IBusqueda {

    @XMLReference(idRes = R.id.rvCliente)
    private RecyclerView rvCliente;
    private BeneficiarioAdapter beneficiarioAdapter;
    @XMLReference(idRes = R.id.lbl_cant_clientes)
    private TextView lblCantidad;
    Boolean refreshing = false;
    List<BeneficiarioRes> beneficiarioList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_beneficiario;
    }

    @Override
    public void inicializarVariables() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSearchListener(this);
    }


    @Override
    public void instanciarIGU() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        beneficiarioAdapter = new BeneficiarioAdapter(this, beneficiarioList, lblCantidad);
        beneficiarioAdapter.setListener(this);


                    cargarBeneficiarios();

        rvCliente.setAdapter(beneficiarioAdapter);
        rvCliente.setLayoutManager(layoutManager);
      // fbRegistrarCliente.setOnClickListener(this);

    }

    @Override
    public String getToolbarTitle() {
        return "Beneficiarios";
    }

    private void cargarBeneficiarios() {
        beneficiarioList.clear();
        beneficiarioList.addAll(new BeneficiarioBl().getBeneficiarios());
        beneficiarioAdapter.myNotifyDataSetChanged();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.fb_new_client:
//                startActivity(new Intent(this, PuntoControlRegistrarActivity.class));
//                break;
            case R.id.card_view:
//                if(v.getTag()!= null){
//                    Long idCliente= (Long) v.getTag();
//                    Intent intent= new Intent(ComponentesActivity.this,ClienteUbicacionActivity.class);
//                    intent.putExtra(KeyUtils.KEY_CLIENTE,idCliente);
//                    startActivity(intent);
//                }
                break;
        }
    }

    @Override
    public void newQuery(String criterio) {
        beneficiarioAdapter.getFilter().filter(criterio);
    }
}
