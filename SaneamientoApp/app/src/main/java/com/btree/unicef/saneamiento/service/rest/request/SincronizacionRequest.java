package com.btree.unicef.saneamiento.service.rest.request;

/**
 * Created by Diana Mejia
 */
public class SincronizacionRequest {
    private String dtLastSync;
    private Long monitorId;
    private String user;

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public String getDtLastSync() {
        return dtLastSync;
    }

    public void setDtLastSync(String dtLastSync) {
        this.dtLastSync = dtLastSync;
    }

    public Long getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Long monitorId) {
        this.monitorId = monitorId;
    }
}
