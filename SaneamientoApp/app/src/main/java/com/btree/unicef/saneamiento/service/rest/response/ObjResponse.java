package com.btree.unicef.saneamiento.service.rest.response;

public  class ObjResponse<T> implements MyResponse{
    private String ServiceName;
    private int status;
    private String LastUpdateDate;
    private String message;
    private T values;


    public String getServiceName() {
        return this.ServiceName;
    }


    public T getValues() {
        return values;
    }


    public int getStatus() {
        return status;
    }


    public String getMessage() {
        return message;
    }


    public String getLastUpdate() {
        return LastUpdateDate;
    }
}
