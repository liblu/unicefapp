package com.btree.unicef.saneamiento.service.rest.response;

/**
 * Created by Marcos on 09/10/2015.
 */
public interface MyResponse {

    public Object getValues();
    public int getStatus();
    public String getMessage();
    public String getServiceName();
    public String getLastUpdate();
}
