package com.btree.unicef.saneamiento.service.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import androidx.core.content.ContextCompat;


public class LocalizacionBL implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, LocationListener {


	private TrackingBLListener listener;
	private LocationRequest mLocationRequest;

	private GoogleApiClient mGoogleApiClient;
	private Context context;


	private static final String TAG = "LocationService";
	private static LocalizacionBL instance;

	public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
	public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;




	public static LocalizacionBL getInstance(Context context) {
		if (instance == null) {
			instance = new LocalizacionBL(context);
		}
		return instance;
	}

	public LocalizacionBL(Context context) {
		this.context = context;
	}

	public void startListening(TrackingBLListener listener) {
		Log.d(TAG, "startTracking");

		this.listener = listener;
		buildGoogleApiClient();

		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
			if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		} else {
			Log.e(TAG, "unable to connect to google play services.");
		}
	}


	@Override
	public void onConnected(Bundle bundle) {
		startLocationUpdates();
	}

	protected synchronized void buildGoogleApiClient() {
		Log.i(TAG, "Building GoogleApiClient");
		mGoogleApiClient = new GoogleApiClient.Builder(context)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
		createLocationRequest();
	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	private void startLocationUpdates() {

		if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

			//LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
 		}
	}

	public  void stopLocationUpdates() {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			//LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onConnectionSuspended(int i) {
		Log.e(TAG, "onDisconnected");
		stopLocationUpdates();
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			Log.e(TAG, "position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());
			listener.onLocationChanged(location);
		}
	}

	@Override
	public void onStatusChanged(String s, int i, Bundle bundle) {

	}

	@Override
	public void onProviderEnabled(String s) {

	}

	@Override
	public void onProviderDisabled(String s) {

	}


	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.e(TAG, "onConnectionFailed");
		stopLocationUpdates();
	}




	public  interface TrackingBLListener {
		void onLocationChanged(Location location);
	}
}
