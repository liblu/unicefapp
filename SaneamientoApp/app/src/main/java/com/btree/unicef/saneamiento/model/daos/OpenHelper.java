package com.btree.unicef.saneamiento.model.daos;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import org.greenrobot.greendao.database.Database;


public class OpenHelper extends DaoMaster.OpenHelper {

	public OpenHelper(Context context, String name, CursorFactory factory) {
		super(context, name, factory);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void onCreate(Database db) {
		//super.onCreate(db);
		Log.i("greenDAO", "Creating tables for schema version "
				+ DaoMaster.SCHEMA_VERSION);
		DaoMaster.createAllTables(db, true);
	}

	@Override
	public void onUpgrade(Database db, int oldVersion, int newVersion) {
		//super.onUpgrade(db, oldVersion, newVersion);
		DaoMaster.dropAllTables(db, true);
		onCreate(db);
	}



}