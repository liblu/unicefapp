package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.PreguntaResDao;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;

/**
 * Created by Diana Mejia
 */

public class PreguntaBl extends ObjetoBLBase<PreguntaResDao> {

    public PreguntaBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected PreguntaResDao getDao(DaoSession daoSession) {
        return daoSession.getPreguntaResDao();
    }

    public PreguntaRes getById(Long preguntaid) {
        return getDefaultDao().queryBuilder().where(PreguntaResDao.Properties.Preguntaid.eq(preguntaid)).unique();
    }

    public Integer countPreguntas() {
        return getDefaultDao().loadAll().size();
    }
}
