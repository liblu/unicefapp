package com.btree.unicef.saneamiento.service.restbase;


import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.service.rest.request.RegisterRequest;
import com.btree.unicef.saneamiento.service.rest.request.SincronizacionRequest;
import com.btree.unicef.saneamiento.service.rest.response.ObjResponse;
import com.btree.unicef.saneamiento.service.rest.response.RutaResponse;
import com.btree.unicef.saneamiento.service.rest.response.SincronizacionResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Diana Mejia on 20/07/2017.
 */

public interface RequestMethods {

    @POST("usuario/loginUsuario/")
    Call<DatosSesion> iniciarSesion(@Body DatosSesion datosSesion);




    @POST("sincronizacion/sync/")
    Call<SincronizacionResponse> downloadData(@Body SincronizacionRequest request);


    @POST("sincronizacion/ruta/")
    Call<RutaResponse>downloadRuta(@Body SincronizacionRequest request);

    @POST("sincronizacion/register/")
    Call<MResponse>evaluacionRegister(@Body RegisterRequest request);

    @Multipart
    @POST("wsImageEval.ashx")
    Call<ObjResponse> postImage(@Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("wsImageEval.ashx")
    Call<ObjResponse> uploadImage(@Field("fileName") String fileName,@Field("Image") String image);


}
