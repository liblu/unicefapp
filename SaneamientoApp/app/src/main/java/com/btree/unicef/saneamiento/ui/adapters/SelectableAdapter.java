package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;

import com.btree.unicef.saneamiento.interfaces.IChecked;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diana Mejia on 15/04/2016.
 */
public class SelectableAdapter<T extends IChecked> extends ArrayAdapter<T> implements View.OnClickListener, Filterable {

    List<T> objects;

    public SelectableAdapter(Context context, List<T> objects) {
        super(context, 0, objects);
        this.objects = objects;
    }

    T itemSelected;

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof IChecked) {
            itemSelected = (T) v.getTag();
            itemSelected.setChecked(true);
            actualizarLista(itemSelected);
        }
    }

    private void actualizarLista(T itemSelected) {

        for (int i = 0; i < getCount(); i++) {
                T item = getItem(i);
            if (item!=itemSelected) {
                item.setChecked(false);
            } else {
                item.setChecked(true);
            }
        }
        notifyDataSetChanged();
    }

    public T getItemSelected() {
        if(itemSelected!=null){
            return itemSelected;
        }else{
           return getItemAnterior();
        }
    }

    public T getItemAnterior(){
        T itemAnterior=null;
        for(int i = 0;i <getCount();i++){
            T currentItem= getItem(i);
            if(currentItem.getChecked()){
                itemAnterior=currentItem;
                break;
            }
        }
        return itemAnterior;
    }

    private class ViewHolder {
        View cardCliente;
        TextView lblNombre;
        TextView lblNit;
       RadioButton radioButton;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        return  null;
    }

    @Override
    public T getItem(int position) {
        return objects.get(position);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    private List<T> lstClientes;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults onReturn = new FilterResults();
                final List<T> results = new ArrayList<>();
                if (lstClientes == null) {
                    lstClientes = objects;
                }

                if (lstClientes != null && !lstClientes.isEmpty() && constraint != null) {
                    for (T item : lstClientes) {
                        if (item.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            results.add(item);
                        }
                    }
                    onReturn.values = results;
                }
                return onReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                objects = (List<T>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
