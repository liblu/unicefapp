package com.btree.unicef.saneamiento.service.rest.response;

/**
 * Created by Diana Mejia on 02/08/2017.
 */

public interface ResponseListener {
    void onSuccess(Object o);

}
