package com.btree.unicef.saneamiento.service.rest.request;

import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;

/**
 * Created by Diana Mejia
 */
public class EvaluacionRes {
    private String descripcion;
    private String picture;
    private Long rutadetid;
    private Long planillaid;
    private Long evaluacionid;
    private String ulastupdate;
    private String estado;

    public EvaluacionRes(RutaRes item){
        this.setEvaluacionid(item.getEvaluacionid());
        this.descripcion=item.getEvaluacionobs();
        this.rutadetid=item.getRutadetid();
        this.planillaid=item.getPlanillaid();
        this.ulastupdate=item.getUlastupdate();

    }

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



    public String getUlastupdate() {
        return ulastupdate;
    }

    public void setUlastupdate(String ulastupdate) {
        this.ulastupdate = ulastupdate;
    }



    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Long getRutadetid() {
        return rutadetid;
    }

    public void setRutadetid(Long rutadetid) {
        this.rutadetid = rutadetid;
    }

    public Long getPlanillaid() {
        return planillaid;
    }

    public void setPlanillaid(Long planillaid) {
        this.planillaid = planillaid;
    }
}
