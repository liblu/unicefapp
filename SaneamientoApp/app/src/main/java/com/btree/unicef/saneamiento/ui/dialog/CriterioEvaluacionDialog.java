package com.btree.unicef.saneamiento.ui.dialog;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.PreguntaBl;
import com.btree.unicef.saneamiento.bussiness.RespuestaBl;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities.NameValue;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;
import com.btree.unicef.saneamiento.model.entities_generated.Respuesta;
import com.btree.unicef.saneamiento.ui.adapters.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CriterioEvaluacionDialog extends DialogFragment implements View.OnClickListener {

    private IDialogConfirmar listener;
    private static final String key_object = "key_object";
    private static final String key_evaluacion = "key_evaluacion";

    private TextView lblError;
    private EditText txtRespText;
    private RespuestaBl respuestaBl;
    private Respuesta respuesta;
    private Spinner spnCantidad;
    private PreguntaRes preguntaRes;
    private Long evaluacionid;

    public static CriterioEvaluacionDialog showDialogCriterio(Long preguntaid,Long evaluacionid,IDialogConfirmar listener) {
        CriterioEvaluacionDialog actualDialog = new CriterioEvaluacionDialog();
        actualDialog.setCancelable(false);
        actualDialog.setListener(listener);
        actualDialog.setStyle(STYLE_NO_FRAME, R.style.Dialog_No_Border);
        Bundle bundle= new Bundle();
        bundle.putLong(CriterioEvaluacionDialog.key_object,preguntaid);
        bundle.putLong(CriterioEvaluacionDialog.key_evaluacion,evaluacionid);
        actualDialog.setArguments(bundle);


        return actualDialog;
    }

    public void setListener(IDialogConfirmar listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        respuestaBl = new RespuestaBl();
        PreguntaBl preguntaBl=new PreguntaBl();

        View v = inflater.inflate(R.layout.dialog_criterio_evaluacion, container, false);
        v.findViewById(R.id.btn_cerrar).setOnClickListener(this);
        v.findViewById(R.id.btn_aceptar).setOnClickListener(this);
        lblError = (TextView) v.findViewById(R.id.lbl_error);
        TextView lblOpt=(TextView) v.findViewById(R.id.lbl_opt);

        txtRespText = (EditText) v.findViewById(R.id.txt_resp_text);
        LinearLayout layoutCant=(LinearLayout) v.findViewById(R.id.view_resp_cant);
        spnCantidad = (Spinner) v.findViewById(R.id.spn_cant);


        Long preguntaid = getArguments().getLong(key_object);
        evaluacionid=getArguments().getLong(key_evaluacion);
        List<NameValue> nameValueList=fillSppiner();
        SpinnerAdapter<NameValue> spinnerAdapter=new SpinnerAdapter<>(getContext(),nameValueList);
        spnCantidad.setAdapter(spinnerAdapter);

        preguntaRes= preguntaBl.getById(preguntaid);
        respuesta=respuestaBl.getRespuesta(evaluacionid,preguntaid);

        if (preguntaRes.getTiporespuesta().equals("N")) {
            txtRespText.setVisibility(View.GONE);
            lblOpt.setText("Ingrese cantidad");
            if(respuesta!=null && respuesta.getRespuestavalue()!=null) {
                int pos = findPosition(respuesta.getRespuestavalue(), nameValueList);
                spnCantidad.setSelection(pos);
            }
        } else {
            layoutCant.setVisibility(View.GONE);
            lblOpt.setText("Escriba su respuesta");
            if(respuesta!=null&& respuesta.getRespuesttext()!=null)
                txtRespText.setText(respuesta.getRespuesttext());

        }

        return v;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cerrar:
                dismiss();
                break;
            case R.id.btn_aceptar:
                if (txtRespText.getVisibility() == View.VISIBLE) {

                    if (txtRespText.getText()!=null && !txtRespText.getText().toString().trim().isEmpty()) {
                        if(respuesta==null){
                            respuesta.setPreguntaid(preguntaRes.getPreguntaid());
                            respuesta.setEvaluacionid(evaluacionid);
                            respuesta.setRespuesttext(txtRespText.getText().toString().trim());
                            respuestaBl.save(respuesta);
                        }else{
                            respuesta.setRespuesttext(txtRespText.getText().toString().trim());
                            respuestaBl.update(respuesta);
                        }
                        listener.aceptar("");
                        dismiss();
                    } else {
                        showError("Debe ingresar una respuesta");
                        txtRespText.requestFocus();
                    }
                } else {
                    NameValue item=(NameValue) spnCantidad.getSelectedItem();
                        if (respuesta == null) {
                            respuesta=new Respuesta();
                            respuesta.setPreguntaid(preguntaRes.getPreguntaid());
                            respuesta.setEvaluacionid(evaluacionid);
                             item=(NameValue) spnCantidad.getSelectedItem();
                            respuesta.setRespuestavalue(item.getItemid());
                            respuestaBl.save(respuesta);
                        } else {
                            respuesta.setRespuestavalue(item.getItemid());
                            respuestaBl.update(respuesta);
                        }
                    listener.aceptar("");
                    dismiss();

                }
                break;
        }
    }

    private void showError(String error) {
        lblError.setText(error);
        lblError.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.GONE);
            }
        }, 3000);
    }

    private List<NameValue> fillSppiner(){
        List<NameValue> nameValueList= new ArrayList<>();
        nameValueList.add(new NameValue(10,"10 %"));
        nameValueList.add(new NameValue(30,"30 %"));
        nameValueList.add(new NameValue(50,"50 %"));
        nameValueList.add(new NameValue(80,"80 %"));
        nameValueList.add(new NameValue(100,"100 %"));
        return  nameValueList;
    }

    private int findPosition(Integer respuestavalue, List<NameValue> nameValueList) {
        for(int i=0;i<=nameValueList.size();i++){
            NameValue item=nameValueList.get(i);
            if(item.getItemid()==respuestavalue){
                return i;
            }
        }
        return 0;
    }
}

