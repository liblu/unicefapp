package com.btree.unicef.saneamiento.bussiness;


import android.util.Log;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.DatosSesionDao;
import com.btree.unicef.saneamiento.model.daos.Singleton;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;

/**
 * Created by Diana Mejia .
 */

public class ParametersBl {
    private DaoSession daoSession;

    public ParametersBl() {
        this.daoSession =  Singleton.getctsBDInstancia(GlobalApp.getInstance().getBaseContext()).getDefaultSession();
    }



    public void insertDatosSesion(DatosSesion datosSesion) {
        this.daoSession.getDatosSesionDao().insertOrReplace(datosSesion);
    }

    public DatosSesion getDatosSession(){
        try {
            String user = SessionManager.getLoginUser();
            return this.daoSession.getDatosSesionDao().queryBuilder().where(DatosSesionDao.Properties.User.eq(user)).unique();
        }catch (Exception e){
            Log.e("login",e.getMessage());
        }
        return null;
    }



    public void updateDatosSession(DatosSesion datosSesion) {

        this.daoSession.getDatosSesionDao().update(datosSesion);
    }



}
