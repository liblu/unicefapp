package com.btree.unicef.saneamiento.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.btree.unicef.saneamiento.model.enums.Fecha;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.core.app.ActivityCompat;

@SuppressLint("SimpleDateFormat")
public class Utils {

    public static final String HABILITADO = "H";
    public static final String INHABILITADO = "I";
    public static final String PENDIENTE = "P";
    public static final String REALIZADO = "R";
    public static final String COMPLETADO = "C";
    public static final String AUTORIZADO = "A";
    public static final String COMERCIAL = "M";

    public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

        if (activeNetworkInfo != null) { // connected to the internet

            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        }
        return false;
    }

    public static String getImageUrl(Integer carpetaId, String itemId) {

        return "";
    }

    public static String formatPrice(Double value) {

        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", decimalFormatSymbols);
        return decimalFormat.format(value);

    }

    public static Double stringToDouble(String value) {
        try {
            String newValue = value.replace(",", "");
            return Double.parseDouble(newValue);
        } catch (Exception e) {
            Log.e("Parseo string a double", e.getMessage());
        }
        return null;
    }


    public static <VHOLDER> void asignarXMLResourceToHolderView(Context context, VHOLDER holder, View view) {
        Class<?> baseAClass = holder.getClass();
        do {
            Field[] fields = baseAClass.getDeclaredFields();
            for (Field field : fields) {
                try {
                    field.getType().asSubclass(View.class);
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(XMLReference.class)) {
                        XMLReference annIGU = field.getAnnotation(XMLReference.class);
                        try {
                            field.set(holder, view.findViewById(annIGU.idRes()));

                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
            }
            baseAClass = baseAClass.getSuperclass();
        } while (baseAClass != null
                && baseAClass.getPackage().getName()
                .contains(context.getPackageName()));
    }


    public static String getDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        TimeZone tz = TimeZone.getTimeZone("America/La_Paz");
        dateFormat.setTimeZone(tz);
        return dateFormat.format(date);
    }

    public static Date getDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getTime(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date);
    }

    public static Date getTime(String time) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.parse(time);
    }

    public static String getDateTime(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String getDateTimeMs(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    public static String getDatePic() {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");

        return dateFormat.format(getCalendar().getTime());
    }

    public static Calendar getCalendar() {
        return Calendar.getInstance(TimeZone.getTimeZone("America/La_Paz"));
    }

    /**
     * Compara dos fechas
     *
     * @param actual
     * @param antigua si la actual es mayor que antigua devuelve 1 si la actual es
     *                menor que la antigua devuelve 2 si la actual es igual que la
     *                antigua devuelve 3
     * @return int
     */
    public static Fecha comparar_fechas(Date actual, Date antigua) {

        Calendar cActual = getCalendar();
        cActual.setTime(actual);

        Calendar cAntigua = getCalendar();
        cAntigua.setTime(antigua);


        if (cActual.after(cAntigua)) {
            return Fecha.MAYOR;

        }
        if (cActual.before(cAntigua)) {
            return Fecha.MENOR;
        }
        if (cActual.equals(cAntigua)) {
            return Fecha.IGUAL;
        }

        return Fecha.NONE;
    }



    public static void llamarTelefono(Context context, String telefono) {
        String number = "tel:" + telefono;
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            context.startActivity(callIntent);
        }

    }


    /*********************************************************************************/

    public static byte[] BitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, blob);
        return blob.toByteArray();
    }

    public static byte[] stringToByteArray(String imagen) {
        // try {

       return Base64.decode(imagen.getBytes(), Base64.DEFAULT);

    }


    public static String encodeFileToBase64(File file){
        String encodedfile = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = Base64.encodeToString(bytes,Base64.DEFAULT);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return encodedfile;
    }

    public static String byteArrayToString(byte[] bytes) {
        return Base64.encodeToString(bytes,Base64.DEFAULT);
    }

    public static boolean IsBase64(String base64String) {
        if ((base64String.replace(" ", "")).length() % 4 != 0) {
            return false;
        } else {
            return true;
        }
    }

    /***
     * Guarda un String como un Arreglo de bytes
     *
     * @param context
     * @param base64ImageData
     * @param nombreArchivo
     * @param extencionArchivo
     * @return
     */
    public static String saveImage(Context context, String base64ImageData,
                                   String nombreArchivo, String extencionArchivo) {
        String fullPath = nombreArchivo + extencionArchivo;
        FileOutputStream fos = null;
        try {
            if (base64ImageData != null) {
                fos = context.openFileOutput(fullPath, Context.MODE_PRIVATE);
                byte[] decodedString = Base64.decode(
                        base64ImageData, Base64.DEFAULT);
                fos.write(decodedString);
                fos.flush();
                fos.close();
            }
        } catch (Exception e) {

        } finally {
            if (fos != null) {
                fos = null;
            }
        }
        return fullPath;
    }



    /**
     * @param context
     * @param nombre
     * @return
     */
    public static Bitmap openImage(Context context, String nombre) {
        FileInputStream fis = null;
        Bitmap bitmap = null;
        try {
            fis = context.openFileInput(nombre);
            bitmap = BitmapFactory.decodeStream(fis);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return bitmap;
    }





    public static Bitmap arrayToBitmap(String picture1) {
        Bitmap bitmap=null;
        try {
            byte[] bitmpadata= stringToByteArray(picture1);
            bitmap = BitmapFactory.decodeByteArray(bitmpadata,0,bitmpadata.length);

        }catch (Exception e){

        }

        return bitmap;
    }


    public static String userAuth() {
        String credential="clerk:clerk";

        return "Basic "+Base64.encodeToString(credential.getBytes(),Base64.NO_WRAP);
    }
}
