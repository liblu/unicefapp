package com.btree.unicef.saneamiento.service.rest.request;

import com.btree.unicef.saneamiento.model.entities_generated.Serviciopeticion;

/**
 * Created by Diana Mejia
 */
public class PeticionRes {


    private Integer cantidad;
    private Long evaluacionid;
    private Long servicioid;

    public PeticionRes(Serviciopeticion item){

        this.cantidad=item.getCantidad();
        this.servicioid=item.getServicioid();
        this.evaluacionid=item.getEvaluacionid();

    }


    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Long getEvaluacionid() {
        return evaluacionid;
    }

    public void setEvaluacionid(Long evaluacionid) {
        this.evaluacionid = evaluacionid;
    }

    public Long getServicioid() {
        return servicioid;
    }

    public void setServicioid(Long servicioid) {
        this.servicioid = servicioid;
    }
}
