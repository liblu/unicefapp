package com.btree.unicef.saneamiento;

import android.app.Application;
import android.content.Context;


/**
 * Created by Diana Mejia
 */
public class GlobalApp extends Application {


    private static GlobalApp myApp;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;

    }


    public static GlobalApp getInstance() {
        return myApp;
    }



}
