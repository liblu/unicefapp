package com.btree.unicef.saneamiento.model.entities;

/**
 * Created by Diana Mejia
 */

public class NameValue {
    public  int itemid;
    public String itemname;

    public NameValue(int id,String name){
        this.itemid=id;
        this.itemname=name;
    }

    public String getItemname() {
        return itemname;
    }

    public int getItemid() {
        return itemid;
    }

    @Override
    public String toString() {
        return getItemname();
    }
}
