package com.btree.unicef.saneamiento.model.enums;

/**
 * Created by Diana Mejia on 20/05/2016.
 */
public enum EstadoComp {
    DONE(0),MIN(-1),MAX(1), NONE(-2);
    private final Integer id;

    EstadoComp(Integer cod) {
        this.id=cod;
    }

    public Integer id() {
        return id;
    }


}
