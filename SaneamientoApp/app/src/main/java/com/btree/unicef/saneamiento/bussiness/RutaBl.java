package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.RutaResDao;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.utils.EvaluacionStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diana Mejia
 */

public class RutaBl extends ObjetoBLBase<RutaResDao> {
    private ParametersBl parametersBl;
    public RutaBl() {
        super(GlobalApp.getInstance().getBaseContext());
        parametersBl=new ParametersBl();
    }

    @Override
    protected RutaResDao getDao(DaoSession daoSession) {
        return daoSession.getRutaResDao();
    }


    public RutaRes getRuta(Long idRutadet) {
      return   getDefaultDao().queryBuilder().where(RutaResDao.Properties.Rutadetid.eq(idRutadet)).unique();
    }

    public RutaRes getRuta(Long rutaid,Integer orden) {
        return   getDefaultDao().queryBuilder().where(RutaResDao.Properties.Rutaid.eq(rutaid),RutaResDao.Properties.Orden.eq(orden)).unique();
    }

    public void enableNextRuta(Long rutadetid){
        RutaRes rutaRes=getRuta(rutadetid);
        rutaRes.setEstado(EvaluacionStatus.SUCCESS.name());
        getDefaultDao().update(rutaRes);
        if(rutaRes.getItemsCount()>rutaRes.getOrden()){
            RutaRes next=getDefaultDao().queryBuilder().where(RutaResDao.Properties.Rutaid.eq(rutaRes.getRutaid()),RutaResDao.Properties.Orden.eq(rutaRes.getOrden()+1)).unique();
            next.setEstado(EvaluacionStatus.ENABLE.name());
            getDefaultDao().update(next);
        }
    }

    public List<RutaRes> getOrdenesVisita() {
        Long monitorid = parametersBl.getDatosSession().getMonitorId();
        if (monitorid != null) {
            return getDefaultDao().queryBuilder().where(RutaResDao.Properties.Monitorid.eq(monitorid)).orderAsc(RutaResDao.Properties.Orden).list();
        }
        return new ArrayList<>();
    }

}
