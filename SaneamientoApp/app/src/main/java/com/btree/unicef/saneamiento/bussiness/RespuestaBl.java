package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.RespuestaDao;
import com.btree.unicef.saneamiento.model.entities_generated.Respuesta;

import java.util.List;

/**
 * Created by Diana Mejia.
 */

public class RespuestaBl extends ObjetoBLBase<RespuestaDao> {

    public RespuestaBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected RespuestaDao getDao(DaoSession daoSession) {
        return daoSession.getRespuestaDao();
    }

    public Respuesta getRespuesta(Long evaluacionId, Long preguntaId){
        return getDefaultDao().queryBuilder().where(RespuestaDao.Properties.Evaluacionid.eq(evaluacionId),RespuestaDao.Properties.Preguntaid.eq(preguntaId) ).unique();
    }

    public void save(Respuesta item){
        getDefaultDao().insert(item);
    }

    public void update(Respuesta item){
        getDefaultDao().update(item);
    }

    public void updateBoolResp(Long respuestaid, boolean b) {
        Respuesta respuesta=findById(respuestaid);
        if(respuesta!=null){
            respuesta.setRespuesta(b);
            getDefaultDao().update(respuesta);
        }
    }

    private Respuesta findById(Long respuestaid) {
        return getDefaultDao().queryBuilder().where(RespuestaDao.Properties.Id.eq(respuestaid)).unique();
    }

    public Integer countRespuesta(Long evaluacionid) {
        return getDefaultDao().queryBuilder().where(RespuestaDao.Properties.Evaluacionid.eq(evaluacionid)).list().size();
    }

    public List<Respuesta> getByEvaluacion(Long evaluacionid) {
        return getDefaultDao().queryBuilder().where(RespuestaDao.Properties.Evaluacionid.eq(evaluacionid)).list();
    }
}
