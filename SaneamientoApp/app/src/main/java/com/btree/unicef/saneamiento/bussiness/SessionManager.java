package com.btree.unicef.saneamiento.bussiness;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.btree.unicef.saneamiento.AppConfig;
import com.btree.unicef.saneamiento.GlobalApp;

public class SessionManager {
    private static final String key_app = "key_app";
    private static final String key_url_config = "url_config";
    private static final String key_login_user = "login_user";
    private static String key_recordar_user = "recordar_user";
    private static final String preference_name = "UserPreferences";
    private static final String key_new_config = "key_new_config";
    private static final String key_device_token = "key_device_token";

    public static final String DATE_TIME_DEFAULT = "01/01/1900 00:00:00";
    public static final String DATE_DEFAULT = "01/01/1900";



    public static SharedPreferences getPreferences() {
        Context context = GlobalApp.getInstance().getBaseContext();
        return context.getSharedPreferences(preference_name,
                Context.MODE_PRIVATE);
    }

    public static Editor getPreferencesEditor() {
        Context context = GlobalApp.getInstance().getBaseContext();
        return context.getSharedPreferences(preference_name,
                Context.MODE_PRIVATE).edit();
    }

    public static void setDeviceToken(String deviceToken) {
        getPreferencesEditor().putString(key_device_token,deviceToken).commit();
    }

    public static String getDeviceToken() {
        return getPreferences().getString(key_device_token, "");

    }



    public static String getCompanyIpPort() {
        return getPreferences().getString(key_url_config, AppConfig.COMPANY_IP_PORT);
    }

    public static void setCompanyIpPort(String url) {
        getPreferencesEditor().putString(key_url_config, url)
                .commit();
    }



    public  static  void setUsuarioRecordar(String usuario){
        getPreferencesEditor().putString(key_recordar_user,usuario).commit();
    }

    public static String getUsuarioRecordar(){
        return getPreferences().getString(key_recordar_user,null);
    }

    public  static  void setLoginUser(String usuario){
        getPreferencesEditor().putString(key_login_user,usuario).commit();
    }

    public static String getLoginUser(){
        return getPreferences().getString(key_login_user,null);
    }


    public static void setNewConfig(boolean newConfig) {
        getPreferencesEditor().putBoolean(key_new_config, newConfig).commit();
    }

    public static Boolean isNewConfig() {
        return getPreferences().getBoolean(key_new_config, false);
    }

    public static String getAppName() {
        return getPreferences().getString(key_app, AppConfig.APP_NAME);
    }

    public static void setAppName(String name) {
        getPreferencesEditor().putString(key_app,name).commit();
    }




}
