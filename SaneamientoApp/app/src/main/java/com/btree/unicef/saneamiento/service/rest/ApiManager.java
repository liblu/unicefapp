package com.btree.unicef.saneamiento.service.rest;


import android.content.Context;
import android.util.Log;

import com.btree.unicef.saneamiento.bussiness.BeneficiarioBl;
import com.btree.unicef.saneamiento.bussiness.GrupoBl;
import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.bussiness.PreguntaBl;
import com.btree.unicef.saneamiento.bussiness.RespuestaBl;
import com.btree.unicef.saneamiento.bussiness.RutaBl;
import com.btree.unicef.saneamiento.bussiness.ServicioBl;
import com.btree.unicef.saneamiento.bussiness.ServiciopeticionBl;
import com.btree.unicef.saneamiento.interfaces.IBaseRest;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.model.entities_generated.Respuesta;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.model.entities_generated.Serviciopeticion;
import com.btree.unicef.saneamiento.model.enums.ResponseStatus;
import com.btree.unicef.saneamiento.service.rest.request.EvaluacionRes;
import com.btree.unicef.saneamiento.service.rest.request.PeticionRes;
import com.btree.unicef.saneamiento.service.rest.request.RegisterRequest;
import com.btree.unicef.saneamiento.service.rest.request.RespuestaRes;
import com.btree.unicef.saneamiento.service.rest.request.SincronizacionRequest;
import com.btree.unicef.saneamiento.service.rest.response.ResponseListener;
import com.btree.unicef.saneamiento.service.rest.response.RutaResponse;
import com.btree.unicef.saneamiento.service.rest.response.SincronizacionResponse;
import com.btree.unicef.saneamiento.service.restbase.MResponse;
import com.btree.unicef.saneamiento.service.restbase.RestBuilder;
import com.btree.unicef.saneamiento.utils.EvaluacionStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Diana Mejia
 */

public class ApiManager {

    private IBaseRest iBaseRest;
    private IDialogConfirmar iDialogoConfirmarFailure;
    private RespuestaBl respuestaBl;
    private ServiciopeticionBl serviciopeticionBl;
    private  RutaBl rutaBl;

    private ApiManager(Context context) {
        this.iBaseRest = (IBaseRest) context;
        respuestaBl=new RespuestaBl();
        serviciopeticionBl=new ServiciopeticionBl();
        rutaBl = new RutaBl();
    }

    public static ApiManager getInstance(Context context) {
        return new ApiManager(context);
    }





    public void dowloadData(final ResponseListener listener){
        final ParametersBl parametersBl=new ParametersBl();
        final DatosSesion datosSesion=parametersBl.getDatosSession();
        SincronizacionRequest request= new SincronizacionRequest();
       // request.setDtLastSync(datosSesion.getLastUpdate());
        request.setMonitorId(datosSesion.getMonitorId());
        request.setUser(datosSesion.getUser());

        final Call<SincronizacionResponse> call = RestBuilder.getDataService().downloadData(request);

        iBaseRest.showProgressDialog(call,"Sincronizando datos...");
        call.enqueue(new Callback<SincronizacionResponse>() {
            @Override
            public void onResponse(Call<SincronizacionResponse> call, Response<SincronizacionResponse> response) {
                if(response.isSuccessful()) {
                    SincronizacionResponse dtResponse = response.body();
                    if (dtResponse.getStatus() == ResponseStatus.SUCCESSFULL.id()) {
                        BeneficiarioBl beneficiarioBl = new BeneficiarioBl();
                        beneficiarioBl.getDefaultDao().deleteAll();
                        beneficiarioBl.getDefaultDao().insertInTx(dtResponse.getBeneficiarios());
                        Log.i("Beneficiarios ", "Succefull");

                        PreguntaBl preguntaBl = new PreguntaBl();
                        preguntaBl.getDefaultDao().deleteAll();
                        preguntaBl.getDefaultDao().insertInTx(dtResponse.getPreguntas());
                        Log.i("Preguntas ", "Succefull");


                        rutaBl.getDefaultDao().deleteAll();
                        int count=dtResponse.getRutas().size();
                        boolean enable=false;
                        for (RutaRes item:dtResponse.getRutas()) {

                            item.setItemsCount(count);
                            if(item.getEstado().equals("P") ){
                                if(!enable) {
                                    item.setEstado(EvaluacionStatus.ENABLE.name());
                                    enable=true;
                                }else{
                                    item.setEstado(EvaluacionStatus.QUEUE.name());
                                }
                            }else if(item.getEstado().equals("C")){

                                item.setEstado(EvaluacionStatus.SUCCESS.name());
                            }
                            rutaBl.getDefaultDao().insert(item);
                        }
                       // rutaBl.getDefaultDao().insertInTx(dtResponse.getRutas());
                        Log.i("Rutas ", "Succefull");

                        GrupoBl grupoBl = new GrupoBl();
                        grupoBl.getDefaultDao().deleteAll();

                        grupoBl.getDefaultDao().insertInTx(dtResponse.getGrupos());

                        Log.i("Grupos ", "Succefull");

                        ServicioBl servicioBl=new ServicioBl();
                        servicioBl.getDefaultDao().deleteAll();
                        servicioBl.getDefaultDao().insertInTx(dtResponse.getServicios());
                        Log.i("Servicios ", "Succefull");

                       datosSesion.setLastUpdate(new Date());
                       parametersBl.updateDatosSession(datosSesion);

                        iBaseRest.onFinish(call);
                        listener.onSuccess("Sincronizacion exitosa");
                    }else if(dtResponse.getStatus()==ResponseStatus.ERROR_LOGICO.id()){
                        failureResponse(true,call,dtResponse.getMessage());
                    }else{
                        failureResponse(false,call,"Error inesperado");
                    }
                }

            }

            @Override
            public void onFailure(Call<SincronizacionResponse> call, Throwable t) {
                failureResponse(false,call,t.getMessage());
            }
        });


    }


    public void downloadRuta(final ResponseListener listener){
        final ParametersBl parametersBl=new ParametersBl();
        final DatosSesion datosSesion=parametersBl.getDatosSession();
        SincronizacionRequest request= new SincronizacionRequest();
      //  request.setDtLastSync(datosSesion.getLastUpdate());
        request.setMonitorId(datosSesion.getMonitorId());
        request.setUser(datosSesion.getUser());

        final Call<RutaResponse> call = RestBuilder.getDataService().downloadRuta(request);

        iBaseRest.showProgressDialog(call,"Sincronizando datos...");
        call.enqueue(new Callback<RutaResponse>() {
            @Override
            public void onResponse(Call<RutaResponse> call, Response<RutaResponse> response) {
                if(response.isSuccessful()) {
                    RutaResponse dtResponse = response.body();
                    Log.i("Status: ",dtResponse.getStatus()+" Mensaje: "+dtResponse.getMessage());
                    if (dtResponse.getStatus() == ResponseStatus.SUCCESSFULL.id()) {
                        RutaBl rutaBl = new RutaBl();
                        rutaBl.getDefaultDao().deleteAll();
                        rutaBl.getDefaultDao().insertInTx(dtResponse.getRutas());
                        Log.i("Rutas ", "Succefull");
                        iBaseRest.onFinish(call);
                        listener.onSuccess("");
                    }else if(dtResponse.getStatus()==ResponseStatus.ERROR_LOGICO.id()){
                        failureResponse(false,call,dtResponse.getMessage());
                    }else{
                        failureResponse(false,call,"Error inesperado");
                    }
                }
            }

            @Override
            public void onFailure(Call<RutaResponse> call, Throwable t) {

                failureResponse(false,call,t.getMessage());
            }
        });
    }

    private void failureResponse(boolean logic,Object call, String mensaje){
        iBaseRest.onFinish(call);
        if(logic) {
            iBaseRest.onErrorLogico(iDialogoConfirmarFailure, mensaje);
        }else{
            iBaseRest.onFailure(iDialogoConfirmarFailure, mensaje);
        }
    }

    public void evaluacionRegister(final RutaRes rutadetalle, final ResponseListener listener) {
        RegisterRequest request=new RegisterRequest();

        final List<Respuesta> respuestas= respuestaBl.getByEvaluacion(rutadetalle.getEvaluacionid());
        List<RespuestaRes> respuestaResList=new ArrayList<>();
        for(Respuesta item:respuestas){
            respuestaResList.add(new RespuestaRes(item));
        }

        final List<Serviciopeticion> serviciopeticionList=serviciopeticionBl.getByEvaluacion(rutadetalle.getEvaluacionid());
        List<PeticionRes> peticionList=new ArrayList<>();
        for(Serviciopeticion item:serviciopeticionList){
            peticionList.add(new PeticionRes(item));
        }
        rutadetalle.setUlastupdate(new ParametersBl().getDatosSession().getUser());
        EvaluacionRes evaluacionRes=new EvaluacionRes(rutadetalle);
        request.setEvaluacion(evaluacionRes);
        request.setRespuestas(respuestaResList);
        request.setPeticiones(peticionList);

        final Call<MResponse> call = RestBuilder.getDataService().evaluacionRegister(request);

        iBaseRest.showProgressDialog(call,"Registrando evaluacion...");
        call.enqueue(new Callback<MResponse>() {
            @Override
            public void onResponse(Call<MResponse> call, Response<MResponse> response) {
                MResponse dtResponse = response.body();
                if (dtResponse.getStatus() == ResponseStatus.SUCCESSFULL.id()) {

                    rutaBl.enableNextRuta(rutadetalle.getRutadetid());
                    serviciopeticionBl.getDefaultDao().deleteInTx(serviciopeticionList);
                    respuestaBl.getDefaultDao().deleteInTx(respuestas);
                    listener.onSuccess("Registro realizado exitosamente!!!");

                }else if(dtResponse.getStatus()==ResponseStatus.ERROR_LOGICO.id()){
                    failureResponse(true,call,dtResponse.getMessage());
                }else{
                    failureResponse(false,call,"Error inesperado");
                }
            }

            @Override
            public void onFailure(Call<MResponse> call, Throwable t) {
                failureResponse(false,call,t.getMessage());
            }
        });

    }
}

