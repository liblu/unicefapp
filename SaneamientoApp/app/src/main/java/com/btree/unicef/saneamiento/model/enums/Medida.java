package com.btree.unicef.saneamiento.model.enums;

/**
 * Created by Diana Mejia on 16/07/2018.
 */

public enum Medida {
    CAUDAL(2),NIVEL(1), PRESION(3),CORRIENTE(4);
    Integer id;
    Medida(int cod) {
        this.id=cod;
    }

    public Integer id() {
        return id;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public String Name(int id){
        switch (id){
            case 1:
                return "Nivel";

            case 2:
                return "Caudal";

            case 3:
                return  "Presion";

            case 4:
                return "Corriente";

            default:
                return "";

        }

    }

}
