package com.btree.unicef.saneamiento.model.daos;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


public class Singleton {

	public static DNetPreventaDB instancia;
	public static final String NOMBRE_BD = "dNetPreventa.sqlite";

	public static DNetPreventaDB getctsBDInstancia(Context context) {
		if (instancia == null) {
			instancia = new DNetPreventaDB(context, NOMBRE_BD);
		}
		return instancia;
	}

	public static class DNetPreventaDB {
		private SQLiteDatabase db;
		private DaoMaster daoMaster;
		private DaoSession defaultDaoSession;
		private OpenHelper helper;

		public DNetPreventaDB(Context context, String databaseName) {
			helper = new OpenHelper(context, databaseName, null);
			db = helper.getWritableDatabase();
			daoMaster = new DaoMaster(db);
			defaultDaoSession = daoMaster.newSession();

		}

		public DaoSession recrearDefaultDaoSession() {
			defaultDaoSession = daoMaster.newSession();
			return defaultDaoSession;
		}

		public DaoSession getDefaultSession() {
			return defaultDaoSession;
		}
	}

}
