package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.RespuestaBl;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;
import com.btree.unicef.saneamiento.model.entities_generated.Respuesta;

import java.util.List;


/**
 * Created by Diana Mejia
 */
public class EvaluacionAdapter extends BaseExpandableListAdapter {
   private List<GrupoRes> grupoResList;
    private final LayoutInflater inflater;
    private Long evaluacion;
    private View.OnClickListener onClickListener;
    private Switch.OnCheckedChangeListener checkedListener;
    private RespuestaBl respuestaBl;
    private Respuesta current;
    private boolean isNew;
    public EvaluacionAdapter(Context context, List<GrupoRes> list, Long evaluacion) {
        this.inflater = LayoutInflater.from(context);
        this.grupoResList =list;
        this.evaluacion=evaluacion;
        this.respuestaBl=new RespuestaBl();
    }

    public void setListener(View.OnClickListener listener){
        onClickListener=listener;
    }
    public void setCheckedListener(Switch.OnCheckedChangeListener listener){
        this.checkedListener=listener;
    }

    @Override
    public int getGroupCount() {
        return grupoResList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return grupoResList.get(i).getPreguntaResList().size();
    }

    @Override
    public Object getGroup(int i) {
        return grupoResList.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return grupoResList.get(groupPosition).getPreguntaResList().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        View resultView = view;
        ViewHolder holder;

        if (resultView == null) {
            resultView = inflater.inflate(R.layout.card_grupo, null);
            holder = new ViewHolder();
            holder.lblNombre = (TextView) resultView.findViewById(R.id.lblNombreGrupo);
            resultView.setTag(holder);
        } else {
            holder = (ViewHolder) resultView.getTag();
        }

        final GrupoRes item = (GrupoRes) getGroup(i);

        holder.lblNombre.setText(item.getDescripcion());
        return resultView;

    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        View resultView = view;
        final ViewChildHolder holder;

        if (resultView == null) {
            resultView = inflater.inflate(R.layout.card_evaluacion, null);
            holder = new ViewChildHolder();
            holder.lblpregunta = (TextView) resultView.findViewById(R.id.lbl_pregunta);
            holder.lblRespCant=(TextView) resultView.findViewById(R.id.lbl_resp_cant);
            holder.lblRespText=(TextView) resultView.findViewById(R.id.lbl_resp_text);
            holder.btnAdd=(ImageButton) resultView.findViewById(R.id.btn_add_text);
            holder.swRespuesta=(Switch) resultView.findViewById(R.id.switch_respuesta);
            holder.viewBool=(LinearLayout) resultView.findViewById(R.id.view_boolean);
            holder.viewText=(LinearLayout) resultView.findViewById(R.id.view_text);
            resultView.setTag(holder);
        } else {
            holder = (ViewChildHolder) resultView.getTag();
        }

        PreguntaRes item = (PreguntaRes) getChild(i,i1);
        current=respuestaBl.getRespuesta(evaluacion,item.getPreguntaid());
        if(current==null) {
            current = new Respuesta();
            current.setEvaluacionid(evaluacion);
            current.setPreguntaid(item.getPreguntaid());
            if(item.getTiporespuesta().equals("B")){
                current.setRespuesta(false);
            }
            respuestaBl.save(current);
        }

        holder.lblpregunta.setText(item.getDescripcion());
        holder.btnAdd.setOnClickListener(onClickListener);
        holder.btnAdd.setTag(item.getPreguntaid());
        holder.swRespuesta.setOnCheckedChangeListener(checkedListener);
        holder.swRespuesta.setTag(current.getId());




            if(item.getTiporespuesta().equals("B")){
                holder.swRespuesta.setChecked(current.getRespuesta()!=null?current.getRespuesta():false);
                holder.viewBool.setVisibility(View.VISIBLE);
                holder.viewText.setVisibility(View.GONE);

            }else if(item.getTiporespuesta().equals("N")){
                holder.lblRespText.setVisibility(View.GONE);
                holder.lblRespCant.setText(current.getRespuestavalue()!=null?String.valueOf(current.getRespuestavalue()):"-");
                holder.viewBool.setVisibility(View.GONE);
                holder.viewText.setVisibility(View.VISIBLE);
            }else if(item.getTiporespuesta().equals("T")) {
                holder.lblRespText.setText("OK");
                holder.lblRespCant.setVisibility(View.GONE);
                holder.viewBool.setVisibility(View.GONE);
                holder.viewText.setVisibility(View.VISIBLE);
            }


        return resultView;
    }



    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {

        TextView lblNombre;


    }

    private class ViewChildHolder{
        TextView lblpregunta;

        LinearLayout viewBool;
        Switch swRespuesta;

        LinearLayout viewText;
        ImageButton btnAdd;
        TextView lblRespText;
        TextView lblRespCant;

    }

}
