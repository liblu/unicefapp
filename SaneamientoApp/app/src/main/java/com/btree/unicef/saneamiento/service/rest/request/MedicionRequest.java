package com.btree.unicef.saneamiento.service.rest.request;

/**
 * Created by Diana Mejia on 16/07/2018.
 */

public class MedicionRequest {
    private String codModem;
    private int tipoComponente;
    private int componenteId;
    private String fechaInicial;
    private String fechaFinal;
    private int medida;

    public String getCodModem() {
        return codModem;
    }

    public void setCodModem(String codModem) {
        this.codModem = codModem;
    }

    public int getTipoComponente() {
        return tipoComponente;
    }

    public void setTipoComponente(int TipoComponente) {
        this.tipoComponente = TipoComponente;
    }

    public int getComponenteId() {
        return componenteId;
    }

    public void setComponenteId(int ComponenteId) {
        this.componenteId = ComponenteId;
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }
}
