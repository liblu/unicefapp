package com.btree.unicef.saneamiento.service.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.btree.unicef.saneamiento.service.rest.SendPuntoControlTask;

/**
 * Created by Diana Mejia on 24/05/2016.
 */
public class ConnectionReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      //  new SendMedicionTask().execute();
        new SendPuntoControlTask().execute();
    }
}
