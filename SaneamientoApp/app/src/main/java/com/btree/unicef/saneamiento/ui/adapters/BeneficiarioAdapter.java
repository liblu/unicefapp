package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.model.entities_generated.BeneficiarioRes;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class BeneficiarioAdapter extends RecyclerView.Adapter<BeneficiarioAdapter.VHIComponente> implements Filterable{
    private List<BeneficiarioRes> beneficiarioList;

    private Context context;
    private TextView cantClientesText;
    private View.OnClickListener onClickListener;

    public BeneficiarioAdapter(Context context, List<BeneficiarioRes> componenteList, TextView cantClientesText) {

        this.context = context;
        this.cantClientesText=cantClientesText;

            this.cantClientesText.setText(String.valueOf(componenteList.size()));


        this.beneficiarioList = componenteList;
    }

    public void setListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public VHIComponente onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_beneficiario, parent, false);
        return new VHIComponente(v);
    }

    @Override
    public void onBindViewHolder(final VHIComponente holder, int position) {
        BeneficiarioRes item = beneficiarioList.get(position);
        holder.lblNombre.setText(item.getNombre());
        holder.lblCodEpsa.setText(String.valueOf(item.getCodigoepsa()));
        holder.lblDireccion.setText(item.getDireccion());
        holder.lblDistrito.setText(String.valueOf(item.getDistrito()));
        holder.card.setTag(item.getBeneficiarioid());
        holder.card.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return beneficiarioList.size();
    }

    private List<BeneficiarioRes> componentes;
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults onReturn= new FilterResults();
                final List<BeneficiarioRes> results= new ArrayList<>();
                if(componentes ==null){
                    componentes = beneficiarioList;
                }

                if(componentes !=null && !componentes.isEmpty()&& constraint!=null){
                    for(BeneficiarioRes item: componentes){
                        if (item.getNombre().toLowerCase().contains(constraint.toString().toLowerCase())){
                            results.add(item);
                        }
                    }
                    onReturn.values=results;
                }
                return onReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                beneficiarioList = (List<BeneficiarioRes>) results.values;

                myNotifyDataSetChanged();
            }
        };
    }

    public void myNotifyDataSetChanged(){
        notifyDataSetChanged();
        if(cantClientesText!=null) {
            cantClientesText.setText(String.valueOf(beneficiarioList.size()));
        }
    }


    public static class VHIComponente extends RecyclerView.ViewHolder  {
        TextView lblNombre;
        TextView lblCodEpsa;
        TextView lblDireccion;
        TextView lblDistrito;
        View  card;




        public VHIComponente(View itemView) {
            super(itemView);

            lblNombre = (TextView) itemView.findViewById(R.id.lbl_nombre);
            lblCodEpsa = (TextView) itemView.findViewById(R.id.lbl_codigo_epsa);
            lblDireccion = (TextView) itemView.findViewById(R.id.lbl_direccion);
            lblDistrito = (TextView) itemView.findViewById(R.id.lbl_distrito);
            card= itemView.findViewById(R.id.card_view);

        }


    }
}
