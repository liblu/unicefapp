package com.btree.unicef.saneamiento.ui.adapters;

import com.btree.unicef.saneamiento.ui.fragment.EvaluacionFragment;
import com.btree.unicef.saneamiento.ui.fragment.ServicioFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

/**
 * Created by Diana Mejia on 03/09/2020.
 */
public class EvaluacionPageAdapter extends FragmentStateAdapter {

    private Long evaluacionid;

    public EvaluacionPageAdapter(@NonNull FragmentActivity fragmentActivity, Long evaluacionid) {
        super(fragmentActivity);
        this.evaluacionid=evaluacionid;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return  EvaluacionFragment.newInstance(evaluacionid);
                default:
                return ServicioFragment.newInstance(evaluacionid);

        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
