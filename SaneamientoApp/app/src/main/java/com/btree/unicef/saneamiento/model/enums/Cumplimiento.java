package com.btree.unicef.saneamiento.model.enums;

/**
 * Created by Diana Mejia on 23/05/2016.
 */
public enum Cumplimiento {

    LLEGUE(1) ,CANCELADO(3),REALIZADO(2),VOLVER(4);

    private final Integer id;

    Cumplimiento(Integer id) {
        this.id=id;
    }

    public Integer getId() {
        return id;
    }
}
