package com.btree.unicef.saneamiento.interfaces;

/**
 * Created by Diana Mejia on 19/05/2016.
 */
public interface IChecked {
    Boolean getChecked();
    void setChecked(Boolean isChecked);
    String getName();
    String getInfoExtra();

}
