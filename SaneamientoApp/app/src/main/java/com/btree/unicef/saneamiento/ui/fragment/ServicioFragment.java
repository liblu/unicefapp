package com.btree.unicef.saneamiento.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.ServicioBl;
import com.btree.unicef.saneamiento.bussiness.ServiciopeticionBl;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities_generated.ServicioRes;
import com.btree.unicef.saneamiento.ui.adapters.ServicioAdapter;
import com.btree.unicef.saneamiento.ui.dialog.CantidadDialog;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A fragment representing a list of Items.
 */
public class ServicioFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private ServicioAdapter servicioAdapter;
    private List<ServicioRes> servicioList;
    private ServiciopeticionBl serviciopeticionBl;
    private Long evaluacionid;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ServicioFragment() {
        serviciopeticionBl=new ServiciopeticionBl();
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ServicioFragment newInstance(Long evaluacionid) {
        ServicioFragment fragment = new ServicioFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, evaluacionid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            evaluacionid = getArguments().getLong(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_servicio, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        servicioList=new ServicioBl().getDefaultDao().loadAll();
        servicioAdapter=new ServicioAdapter(getContext(),evaluacionid,servicioList);
        servicioAdapter.setCheckedListener(ServicioFragment.this);
        servicioAdapter.setListener(ServicioFragment.this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView rvServicio= (RecyclerView)view.findViewById(R.id.rvServicio);
        rvServicio.setAdapter(servicioAdapter);
        rvServicio.setLayoutManager(layoutManager);
        servicioAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.rb_resp:
                if(compoundButton.getTag()!=null) {
                    Long servicioid = (Long) compoundButton.getTag();
                    serviciopeticionBl.updateResp(evaluacionid,servicioid,b);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_cantidad:
                Long servicioid=(Long)view.getTag();
                CantidadDialog.showDialogCantidad(servicioid, evaluacionid, new IDialogConfirmar() {
                    @Override
                    public void aceptar(String object) {
                        servicioAdapter.notifyDataSetChanged();
                    }
                }).show(getChildFragmentManager(),"");

                break;
        }
    }
}