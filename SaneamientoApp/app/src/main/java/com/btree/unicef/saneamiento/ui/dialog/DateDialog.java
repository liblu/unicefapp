package com.btree.unicef.saneamiento.ui.dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia
 */
@SuppressLint("ValidFragment")
public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    EditText txtDate;

    public DateDialog(View view){
        txtDate= (EditText) view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       //return super.onCreateDialog(savedInstanceState);

        final Calendar calendar= Calendar.getInstance();
        int year= calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day= calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),this,year,month,day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String day, month;
        day= String.valueOf(dayOfMonth);
        month= String.valueOf(monthOfYear+1);
        if(dayOfMonth<10){
            day= "0"+dayOfMonth;
        }

        if(monthOfYear<9){
            month= "0"+(monthOfYear+1);
        }
        String date= day+"/"+month+"/"+year;
        txtDate.setText(date);
    }
}
