package com.btree.unicef.saneamiento;

/**
 * Created by Diana Mejia on 26/01/2018.
 */

public class AppConfig {


    public static final String COMPANY_IP_PORT = "190.181.61.130:8181";
    public static final String COMPANY_NAME = "B-Tree";
    public static final String APP_NAME = "SicmoPeriagua-war";


}
