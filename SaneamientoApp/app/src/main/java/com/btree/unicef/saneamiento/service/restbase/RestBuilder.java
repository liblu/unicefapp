package com.btree.unicef.saneamiento.service.restbase;


import android.util.Log;

import com.btree.unicef.saneamiento.bussiness.SessionManager;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Diana Mejia
 */

public class RestBuilder {

    private  static RequestMethods restServiceMethods;

    public static RequestMethods getDataService() {
        if( restServiceMethods==null|| SessionManager.isNewConfig()) {

          //  String urlBase= "http://192.168.0.12:8080/SaneamientoBO-war/webresources/"; //local
           // String urlBase= "http://sdbes.ddns.net:8080/SaneamientoBO-war/webresources/";//desarrollo
            String urlBase="http://3.19.26.119:8080/SaneamientoBO-war/webresources/";//produccion

            Log.d("URL Conexion",urlBase);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor("clerk","clerk"))
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            Retrofit builder = new Retrofit.Builder().baseUrl(urlBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            restServiceMethods = builder.create(RequestMethods.class);
            SessionManager.setNewConfig(false);
        }
        return restServiceMethods;
    }
}
