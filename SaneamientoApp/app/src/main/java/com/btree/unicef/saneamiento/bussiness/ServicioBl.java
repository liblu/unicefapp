package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.ServicioResDao;
import com.btree.unicef.saneamiento.model.entities_generated.ServicioRes;

import java.util.List;

/**
 * Created by Diana Mejia
 */

public class ServicioBl extends ObjetoBLBase<ServicioResDao> {

    public ServicioBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected ServicioResDao getDao(DaoSession daoSession) {
        return daoSession.getServicioResDao();
    }

    public List<ServicioRes> getServicios() {
        return getDefaultDao().loadAll();
    }


    public ServicioRes getById(Long servicioid) {
        return getDefaultDao().queryBuilder().where(ServicioResDao.Properties.Servicioid.eq(servicioid)).unique();
    }
}
