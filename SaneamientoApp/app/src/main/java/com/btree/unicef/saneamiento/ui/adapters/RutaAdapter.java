package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.utils.EvaluacionStatus;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Diana Mejia
 */
public class RutaAdapter extends RecyclerView.Adapter<RutaAdapter.VHIComponente> implements Filterable{
    private List<RutaRes> rutaList;

    private Context context;
    private TextView cantText;
    private View.OnClickListener onClickListener;

    public RutaAdapter(Context context, List<RutaRes> rutaList, TextView cantText) {

        this.context = context;
        this.cantText =cantText;

            this.cantText.setText(String.valueOf(rutaList.size()));


        this.rutaList = rutaList;
    }

    public void setListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public VHIComponente onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ruta, parent, false);
        return new VHIComponente(v);
    }

    @Override
    public void onBindViewHolder(final VHIComponente holder, int position) {
        RutaRes item = rutaList.get(position);
        holder.lblBeneficiario.setText(item.getBeneficiarioDes());
        holder.lblOrdenAtencion.setText(String.valueOf(item.getOrden()));
        holder.optUbicacion.setTag(item.getRutadetid());
        holder.optPlanilla.setTag(item.getRutadetid());
        holder.viewStatus.setBackgroundColor(item.getEstado().equals(EvaluacionStatus.ENABLE.name())?context.getColor(R.color.colorAccent):item.getEstado().equals(EvaluacionStatus.SUCCESS.name())?context.getColor(R.color.success_color):context.getColor(R.color.btn_disable));
        if(item.getEstado().equals(EvaluacionStatus.ENABLE.name())) {
            holder.optPlanilla.setOnClickListener(onClickListener);
            holder.optUbicacion.setOnClickListener(onClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return rutaList.size();
    }

    private List<RutaRes> rutas;
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults onReturn= new FilterResults();
                final List<RutaRes> results= new ArrayList<>();
                if(rutas ==null){
                    rutas = rutaList;
                }

                if(rutas !=null && !rutas.isEmpty()&& constraint!=null){
                    for(RutaRes item: rutas){
                        if (item.getBeneficiarioDes().toLowerCase().contains(constraint.toString().toLowerCase())){
                            results.add(item);
                        }
                    }
                    onReturn.values=results;
                }
                return onReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                rutaList = (List<RutaRes>) results.values;

                myNotifyDataSetChanged();
            }
        };
    }

    public void myNotifyDataSetChanged(){
        notifyDataSetChanged();
        if(cantText !=null) {
            cantText.setText(String.valueOf(rutaList.size()));
        }
    }


    public static class VHIComponente extends RecyclerView.ViewHolder  {
        TextView lblBeneficiario;
        TextView lblOrdenAtencion;
        ImageView optUbicacion;
        ImageView optPlanilla;
        View  viewStatus;




        public VHIComponente(View itemView) {
            super(itemView);

            lblBeneficiario = (TextView) itemView.findViewById(R.id.lbl_nombre);
            lblOrdenAtencion = (TextView) itemView.findViewById(R.id.lbl_nro);
            optPlanilla=(ImageView) itemView.findViewById(R.id.btn_opt_planilla);
            optUbicacion=itemView.findViewById(R.id.btn_opt_ubicacion);

            viewStatus= itemView.findViewById(R.id.view_status);

        }


    }
}
