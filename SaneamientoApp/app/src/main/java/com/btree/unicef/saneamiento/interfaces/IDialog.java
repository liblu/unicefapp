package com.btree.unicef.saneamiento.interfaces;

/**
 * Created by Diana Mejia on 25/05/2016.
 */
public interface IDialog {
    public abstract void aceptar(Object nameOject);
}
