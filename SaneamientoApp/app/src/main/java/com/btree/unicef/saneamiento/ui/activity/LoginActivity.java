package com.btree.unicef.saneamiento.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.bussiness.SessionManager;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.model.enums.Fecha;
import com.btree.unicef.saneamiento.service.rest.LoginTask;
import com.btree.unicef.saneamiento.service.rest.response.ResponseListener;
import com.btree.unicef.saneamiento.utils.Utils;
import com.btree.unicef.saneamiento.utils.XMLReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.btree.unicef.saneamiento.utils.KeyUtils.REQUEST_ID_MULTIPLE_PERMISSIONS;

/**
 * Created by Diana Mejia
 */

public class LoginActivity extends BaseActivity implements OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {


    @XMLReference(idRes = R.id.txt_login)
    private EditText txtLogin;
    @XMLReference(idRes = R.id.txt_pasword)
    private EditText txtPassword;
    @XMLReference(idRes = R.id.btn_ingresar)
    private Button btnIniciarSesion;
   // @XMLReference(idRes = R.id.img_logo)
    //private ImageView imgLogo;
    @XMLReference(idRes = R.id.cbx_recordar_user)
    private CheckBox cbxRecordarUsuario;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void inicializarVariables() {

    }

    @Override
    public void instanciarIGU() {
        btnIniciarSesion.setOnClickListener(this);

        cbxRecordarUsuario.setChecked(SessionManager.getUsuarioRecordar() != null);
        if (SessionManager.getUsuarioRecordar() != null && cbxRecordarUsuario.isChecked()) {
            txtLogin.setText(SessionManager.getUsuarioRecordar());
            txtPassword.requestFocus();
        }


    }

    @Override
    public String getToolbarTitle() {
        return null;
    }


    private boolean validarDatos() {
        String password = txtPassword.getText().toString();

        if (txtLogin.getText().toString().isEmpty()) {
            txtLogin.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            txtPassword.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        if (checkAndRequestPermissions()) {
            if (validarDatos() ) {
                SessionManager.setLoginUser(txtLogin.getText().toString().trim());
                if(Utils.checkConnection(LoginActivity.this)){
                    iniciarSesion();
                }else{
                    DatosSesion datosSesion= new ParametersBl().getDatosSession();
                    if(datosSesion!=null && datosSesion.getUser()==txtLogin.getText().toString().trim() && Utils.comparar_fechas(Utils.getCalendar().getTime(),datosSesion.getLastLogin())== Fecha.IGUAL){
                        if(datosSesion.getUser()==txtLogin.getText().toString().trim() && datosSesion.getPassword()==txtPassword.getText().toString().trim()){
                            startActivity(new Intent(LoginActivity.this,MenuPrincipalActivity.class));
                        }else{
                            showAlertWaring("usuario y/o contraseña incorrectos..");
                        }
                    }else{
                        showAlertWaring("Sus datos no estan actualizados, conectese a una red wifi para sincronizar..");
                    }
                }
            } else {
                showAlertWaring(getString(R.string.alert_campos_obligatorios));
            }
        }
    }

    private void iniciarSesion() {

        final String login = txtLogin.getText().toString().trim();
        String passw = txtPassword.getText().toString().trim();

        new LoginTask(this,login,passw, new ResponseListener() {
            @Override
            public void onSuccess(Object o) {
                if (cbxRecordarUsuario.isChecked()) {
                    SessionManager.setUsuarioRecordar(login);
                } else {
                    SessionManager.setUsuarioRecordar(null);
                }
                startActivity(new Intent(LoginActivity.this, MenuPrincipalActivity.class));
            }


        }).execute();

    }






    private boolean checkAndRequestPermissions() {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            List<String> listPermissionsNeeded = new ArrayList<>();
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE,PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if ( perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permission", " location services permission granted");
                        SessionManager.setLoginUser(txtLogin.getText().toString().trim());
                        iniciarSesion();
                    } else {
                        Log.d("Permission", "Some permissions are not granted ask again ");
                        if ( ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("El permiso Ubicacion es necesaria para esta aplicacion",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        } else {
                            Toast.makeText(this, "Ir a ajustes -> aplicaciones-> saneamiento -> permisos y habilitar los permisos  Ubicación", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
}

