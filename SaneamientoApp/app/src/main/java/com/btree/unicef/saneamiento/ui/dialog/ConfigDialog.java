package com.btree.unicef.saneamiento.ui.dialog;

import android.annotation.TargetApi;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.SessionManager;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia on 15/04/2016.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ConfigDialog extends DialogFragment implements View.OnClickListener {
    private final static String key_object = "key_object";
    private IDialogConfirmar listener;
    private EditText txtIP;
    private TextView lblError;
    private TextView txtPuerto;
    private TextView lblServiceFolder;
    private TextView txtApp;

    public static ConfigDialog showConfigDialog(IDialogConfirmar listener) {
        ConfigDialog actualDialog = new ConfigDialog();
        actualDialog.setCancelable(false);
        actualDialog.setListener(listener);
        actualDialog.setStyle(STYLE_NO_FRAME, R.style.Dialog_No_Border);

        return actualDialog;
    }

    public void setListener(IDialogConfirmar listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        View v = inflater.inflate(R.layout.dialog_config, container, false);
        v.findViewById(R.id.btn_aceptar).setOnClickListener(this);
        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        TextView lblIpAnterior = (TextView) v.findViewById(R.id.lbl_ip_anterior);
        TextView lblAppAnterior = (TextView) v.findViewById(R.id.lbl_app_anterior);
        txtIP = (EditText) v.findViewById(R.id.txt_ip);
        txtPuerto = (TextView) v.findViewById(R.id.txt_puerto);
        txtApp = (TextView) v.findViewById(R.id.txt_app);
        lblError = (TextView) v.findViewById(R.id.lbl_error);
        lblServiceFolder = (TextView) v.findViewById(R.id.lbl_service_folder);

        TextView lblApp = (TextView) v.findViewById(R.id.lbl_app);

        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(),0);
            lblApp.setText(getString(R.string.app_name)+" v "+packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("IP_PUERTO",SessionManager.getCompanyIpPort());
        lblIpAnterior.setText(SessionManager.getCompanyIpPort());
        lblAppAnterior.setText(SessionManager.getAppName());
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_aceptar:

                if (camposValidos()) {
                    String url = txtIP.getText().toString().trim() + ":" + txtPuerto.getText().toString().trim();
                    SessionManager.setCompanyIpPort(url);
                    String appName= txtApp.getText().toString().trim();
//                    if(appName.compareTo(SessionManager.getAppName())!=0){
//                        FirebaseMessaging.getInstance().unsubscribeFromTopic(SessionManager.getAppName());
//                        Log.d("UnsubscribeTopic",SessionManager.getAppName());
//                    }

                    SessionManager.setAppName(appName);

                    SessionManager.setNewConfig(true);
                    listener.aceptar(null);
                    dismiss();
                }

                break;

            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    private Boolean camposValidos() {
        if (txtIP.getText().toString().trim().isEmpty()) {
            showError("Ingrese una ip");
            return false;
        }

        if (txtPuerto.getText().toString().trim().isEmpty()) {
            showError("Ingrese un puerto");
            return false;
        }

        if (txtApp.getText().toString().trim().isEmpty()) {
            showError("Ingrese nombre de la aplicacion");
            return false;
        }

        return true;
    }

    private void showError(String error) {
        lblError.setText(error);
        lblError.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.GONE);
            }
        }, 3000);
    }


}



