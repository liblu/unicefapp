package com.btree.unicef.saneamiento.service.rest.response;

import com.btree.unicef.saneamiento.model.entities_generated.BeneficiarioRes;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.model.entities_generated.ServicioRes;
import com.btree.unicef.saneamiento.service.restbase.MResponse;

import java.util.List;

/**
 * Created by Diana Mejia
 */
public class SincronizacionResponse extends MResponse {
    List<BeneficiarioRes> beneficiarios;
    List<RutaRes> rutas;
    List<PreguntaRes> preguntas;
    List<GrupoRes> grupos;
    List<ServicioRes> servicios;

    public List<BeneficiarioRes> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<BeneficiarioRes> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    public List<RutaRes> getRutas() {
        return rutas;
    }

    public void setRutas(List<RutaRes> rutas) {
        this.rutas = rutas;
    }

    public List<PreguntaRes> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<PreguntaRes> preguntas) {
        this.preguntas = preguntas;
    }

    public List<GrupoRes> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoRes> grupos) {
        this.grupos = grupos;
    }

    public List<ServicioRes> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioRes> servicios) {
        this.servicios = servicios;
    }
}
