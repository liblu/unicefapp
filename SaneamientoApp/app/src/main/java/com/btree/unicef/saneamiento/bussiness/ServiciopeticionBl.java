package com.btree.unicef.saneamiento.bussiness;

import com.btree.unicef.saneamiento.GlobalApp;
import com.btree.unicef.saneamiento.model.daos.DaoSession;
import com.btree.unicef.saneamiento.model.daos.ObjetoBLBase;
import com.btree.unicef.saneamiento.model.daos.ServiciopeticionDao;
import com.btree.unicef.saneamiento.model.entities_generated.Serviciopeticion;

import java.util.List;

/**
 * Created by Diana Mejia
 */

public class ServiciopeticionBl extends ObjetoBLBase<ServiciopeticionDao> {

    public ServiciopeticionBl() {
        super(GlobalApp.getInstance().getBaseContext());
    }

    @Override
    protected ServiciopeticionDao getDao(DaoSession daoSession) {
        return daoSession.getServiciopeticionDao();
    }



    public Serviciopeticion getByEvaluacionServicio(Long evaluacion,Long servicioid) {
        return getDefaultDao().queryBuilder().where(ServiciopeticionDao.Properties.Servicioid.eq(servicioid), ServiciopeticionDao.Properties.Evaluacionid.eq(evaluacion)).unique();
    }

    public void update(Serviciopeticion entity){
        getDefaultDao().update(entity);

    }

    public void create(Serviciopeticion entity) {
        getDefaultDao().insert(entity);
    }

    public void updateResp(Long evaluacionid, Long servicioid, boolean val) {
        Serviciopeticion item=getByEvaluacionServicio(evaluacionid,servicioid);
        if(item!=null && !val){
            getDefaultDao().delete(item);
        }else if(item==null && val){
            item=new Serviciopeticion();
            item.setServicioid(servicioid);
            item.setEvaluacionid(evaluacionid);
            getDefaultDao().insert(item);
        }

    }

    public List<Serviciopeticion> getByEvaluacion(Long evaluacionid) {

        return getDefaultDao().queryBuilder().where(ServiciopeticionDao.Properties.Evaluacionid.eq(evaluacionid)).list();
    }
}
