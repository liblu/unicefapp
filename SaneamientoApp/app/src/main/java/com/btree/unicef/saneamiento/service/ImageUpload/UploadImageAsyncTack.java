package com.btree.unicef.saneamiento.service.ImageUpload;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.btree.unicef.saneamiento.model.enums.ResponseStatus;
import com.btree.unicef.saneamiento.service.rest.response.ObjResponse;
import com.btree.unicef.saneamiento.service.restbase.RestBuilder;
import com.btree.unicef.saneamiento.utils.Utils;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;

/**
 * Created by Diana Mejia on 01/08/2017.
 */

public class UploadImageAsyncTack extends AsyncTask<String,Long,Long> {
    private Context ctx;
    public UploadImageAsyncTack(Context ctx) {
        this.ctx=ctx;
    }

    @Override
    protected Long doInBackground(String... params) {

        try {

            for (String fileName: params) {

                File file = new File(fileName);

               String image = Utils.encodeFileToBase64(file);


                String fil=file.getName();

                Call<ObjResponse> req= RestBuilder.getDataService().uploadImage(fil,image);

                ObjResponse responseBody= req.execute().body();
                if(responseBody.getStatus()== ResponseStatus.SUCCESSFULL.id()){
                    Log.i("upload Image", "Response Status: " + responseBody.getStatus() + " - " + responseBody.getMessage());
                }else{
                    Log.e("upload Image", "Response Status: " + responseBody.getStatus() + " - " + responseBody.getMessage());
                }
            }

            return 1L;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);

    }
}
