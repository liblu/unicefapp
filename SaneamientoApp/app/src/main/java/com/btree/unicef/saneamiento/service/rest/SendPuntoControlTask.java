package com.btree.unicef.saneamiento.service.rest;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by Diana Mejia
 */

public class SendPuntoControlTask extends AsyncTask<Void, Long, Long> {

    @Override
    protected Long doInBackground(Void... params) {
        try {

           /* PuntoControlBl puntoControlBl= new PuntoControlBl();
            List<PuntoControl> puntoControlList=puntoControlBl.getDefaultDao().loadAll();

            Call<List<MovilId>> ptoControlCall = RestBuilder.getDataService().registrarPuntoControl(puntoControlList);

             Response<List<MovilId>> mResponse= ptoControlCall.execute();
            if( mResponse.isSuccessful()) {
                List<MovilId> movilIdList=mResponse.body();
                if (movilIdList!=null ) {

                    for (MovilId oMovilId: movilIdList ) {
                        if(oMovilId.getStatus()== ResponseStatus.SUCCESSFULL.id() && oMovilId.getMovilId()!=null) {
                            PuntoControl puntoControl = puntoControlBl.getPuntoControl(oMovilId.getMovilId());
                            puntoControlBl.getDefaultDao().delete(puntoControl);
                        }
                    }
                }
            }*/


            return 1L;

        } catch (Exception e) {
            Log.e("Envio PtoControl Error", e.getMessage());
        }
        return -1L;
    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
        if (aLong == 1L) {
            Log.i("Envio Punto Control", "descarga exitosa----------------------");
        } else {
            Log.e("Envio Punto Control", "descarga finalizada con errores----------------------");
        }
    }
}
