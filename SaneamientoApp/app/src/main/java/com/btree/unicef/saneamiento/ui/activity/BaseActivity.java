package com.btree.unicef.saneamiento.ui.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.interfaces.IBaseRest;
import com.btree.unicef.saneamiento.interfaces.IBusqueda;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.enums.StateAlert;
import com.btree.unicef.saneamiento.ui.dialog.AppDialog;
import com.btree.unicef.saneamiento.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia on 31/03/2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements IBaseRest, SearchView.OnQueryTextListener {
    protected ProgressDialog progressDialog;
    protected ProgressDialog progressDialogR;

    protected List<Object> pendientes = new ArrayList<Object>();
    private Queue<Pair<DialogFragment, String>> espera = new LinkedList<Pair<DialogFragment, String>>();
    protected boolean isResumed;
    private TextView conuterTv;
   // LocationManager locManager;
    private boolean impresionCorrecta = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransitionStart();
        super.onCreate(savedInstanceState);


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutId());
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        progressDialogR = new ProgressDialog(this);

//        locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            locManager.addGpsStatusListener(this);
//        }
        inicializarVariables();
        Utils.asignarXMLResourceToHolderView(this, this, getWindow().getDecorView());
        ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(true);
            if (getToolbarTitle() != null) {
                toolbar.setTitle(getToolbarTitle());
            }
        }
        instanciarIGU();
    }

    public abstract int getLayoutId();

    public abstract void inicializarVariables();

    public abstract void instanciarIGU();

    public abstract String getToolbarTitle();


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!(this instanceof LoginActivity)) {
            getMenuInflater().inflate(R.menu.main, menu);
            if ( (this instanceof BeneficiariosActivity)) {
                MenuItem item = menu.findItem(R.id.action_search);
                item.setVisible(true);
                invalidateOptionsMenu();
            }

            if (this instanceof MenuPrincipalActivity) {
                MenuItem item = menu.findItem(R.id.action_menu_principal);
                item.setVisible(false);
            }

        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

        } else if (item.getItemId() == R.id.action_menu_principal) {
            if (impresionCorrecta) {
                Intent intent = new Intent(this, MenuPrincipalActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

        } else if (item.getItemId() == R.id.action_search) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                searchView = (SearchView) item.getActionView();
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setOnQueryTextListener(this);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void showProgressDialog(Object clases, CharSequence mensaje) {
        if (progressDialog == null || pendientes.isEmpty()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        if (mensaje != null && !mensaje.equals("")) {
            progressDialog.setMessage(mensaje);
        }
        if (clases != null) {
            pendientes.add(clases);
        }
    }

    public void dissmisProgress(Object clases) {
        if (clases != null) {
            synchronized (pendientes) {
                pendientes.remove(clases);
                if (pendientes.isEmpty()) {
                    progressDialog.dismiss();
                }
            }
        }
    }


    public void showAlert(IDialogConfirmar listener, StateAlert state, CharSequence title, CharSequence mensaje) {
        Dialog dialog = AppDialog.showAlert(this, listener, state, title, mensaje);
        dialog.show();
    }

    public void showAlertError(String mensaje) {
        showAlert(null, StateAlert.ERROR, "Error", mensaje);
    }


    public void showAlertWaring(CharSequence mensaje) {
        showAlert(null, StateAlert.WARNING, "Atención", mensaje);
    }

    public void showAlertSuccessful(String mensaje) {
        showAlert(null, StateAlert.SUCCESSFUL, "Aviso", mensaje);
    }

    public void showAlert(String title, CharSequence mensaje) {
        showAlert(null, StateAlert.WARNING, title, mensaje);
    }

    @Override
    public void onFinish(Object rest) {
        dissmisProgress(rest);
    }

    @Override
    public void onFailure(IDialogConfirmar listener, String mensaje) {

        showAlert(listener, StateAlert.ERROR, null, mensaje);
    }

    @Override
    public void onErrorLogico( IDialogConfirmar listener, String mensaje) {
        showAlert(listener, StateAlert.WARNING, null, mensaje);
    }

    protected void overridePendingTransitionStart() {
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    protected void overridePendingTransitionEnd() {
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
        while (!espera.isEmpty()) {
            Pair<DialogFragment, String> pair = espera.poll();
            pair.first.show(getSupportFragmentManager(), pair.second);
        }
    }



    public void showDialogFragment(DialogFragment dialogo, String TAG) {
        if (isResumed) {
            dialogo.show(getSupportFragmentManager(), TAG);
        } else {
            espera.add(new Pair<DialogFragment, String>(dialogo, TAG));
        }
    }

    @Override
    protected void onPause() {
        isResumed = false;
        super.onPause();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionEnd();
    }

    protected IBusqueda listener;

    protected void setSearchListener(IBusqueda listener) {
        this.listener = listener;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (listener != null) {
            listener.newQuery(newText);
        }
        return true;
    }


}
