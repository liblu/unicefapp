package com.btree.unicef.saneamiento.ui.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.ServiciopeticionBl;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities_generated.Serviciopeticion;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * Created by Diana Mejia
 */
public class CantidadDialog extends DialogFragment implements View.OnClickListener {

    private IDialogConfirmar listener;
    private static final String key_object = "key_object";
    private static final String key_evaluacion = "key_evaluacion";

    private TextView lblError;

    private ServiciopeticionBl serviciopeticionBl;
    private Serviciopeticion serviciopeticion;
    private NumberPicker npRespCantidad;
    private Long evaluacionid;
    private Long servicioid;

    public static CantidadDialog showDialogCantidad(Long servicioid, Long evaluacionid, IDialogConfirmar listener) {
        CantidadDialog actualDialog = new CantidadDialog();
        actualDialog.setCancelable(false);
        actualDialog.setListener(listener);
        actualDialog.setStyle(STYLE_NO_FRAME, R.style.Dialog_No_Border);
        Bundle bundle= new Bundle();
        bundle.putLong(CantidadDialog.key_object,servicioid);
        bundle.putLong(CantidadDialog.key_evaluacion,evaluacionid);
        actualDialog.setArguments(bundle);


        return actualDialog;
    }

    public void setListener(IDialogConfirmar listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        serviciopeticionBl = new ServiciopeticionBl();


        View v = inflater.inflate(R.layout.dialog_cantidad, container, false);
        v.findViewById(R.id.btn_cerrar).setOnClickListener(this);
        v.findViewById(R.id.btn_aceptar).setOnClickListener(this);
        lblError = (TextView) v.findViewById(R.id.lbl_error);

        npRespCantidad = (NumberPicker) v.findViewById(R.id.spn_cant);
        npRespCantidad.setMinValue(1);
        npRespCantidad.setMaxValue(50);

        servicioid = getArguments().getLong(key_object);
        evaluacionid=getArguments().getLong(key_evaluacion);

        serviciopeticion = serviciopeticionBl.getByEvaluacionServicio(evaluacionid,servicioid);
        if(serviciopeticion!=null){
            npRespCantidad.setValue(serviciopeticion.getCantidad());
        }


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cerrar:
                dismiss();
                break;
            case R.id.btn_aceptar:
                if(npRespCantidad.getValue()>0){
                    if(serviciopeticion==null){
                        serviciopeticion=new Serviciopeticion();
                        serviciopeticion.setEvaluacionid(evaluacionid);
                        serviciopeticion.setServicioid(servicioid);
                        serviciopeticion.setCantidad(npRespCantidad.getValue());
                        serviciopeticionBl.create(serviciopeticion);
                    }else{
                        serviciopeticion.setCantidad(npRespCantidad.getValue());
                        serviciopeticionBl.update(serviciopeticion);
                    }
                    listener.aceptar("");
                    dismiss();
                }else{
                    showError("la cantidad debe ser mayor a cero");
                }
                break;


        }
    }

    private void showError(String error) {
        lblError.setText(error);
        lblError.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lblError.setVisibility(View.GONE);
            }
        }, 3000);
    }
}

