package com.btree.unicef.saneamiento.service.location;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diana Mejia on 01/11/2016.
 */

public class TrazarRutaAsyncTask extends AsyncTask<Void, Void, String> {
    private ProgressDialog progressDialog;
    private String url;
    private Location locationOrigen;
    private Location locationDestino;
    private Context context;
    private TrazarRutaListener listener;

    public TrazarRutaAsyncTask(Context context, TrazarRutaListener listener, Location origen, Location destino) {
        this.context = context;
        this.locationOrigen = origen;
        this.locationDestino = destino;
        this.listener = listener;
        this.url = makeURL(locationOrigen.getLatitude(), locationOrigen.getLongitude(), locationDestino.getLatitude(), locationDestino.getLongitude());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Obteniendo ruta, porfavor espere...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
      //  String json = JSONParser.getJSONFromUrl(url);
        // return json;
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progressDialog.hide();
        List<LatLng> pointList = new ArrayList<>();
        if (result != null && !result.equals("")) {
            try {
                final JSONObject json = new JSONObject(result);
                JSONArray routeArray = json.getJSONArray("routes");
                JSONObject routes = routeArray.getJSONObject(0);
                JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                String encodedString = overviewPolylines.getString("points");
                pointList = decodePoly(encodedString);
            } catch (JSONException e) {

            }
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(locationOrigen.getLatitude(), locationOrigen.getLongitude()));
            builder.include(new LatLng(locationDestino.getLatitude(), locationDestino.getLongitude()));

            LatLngBounds bounds = builder.build();
            int padding = 100; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);


            Float distance = locationOrigen.distanceTo(locationDestino);
            String dist;
            if (distance < 1000) {
                dist = String.format("%.02f", distance) + " Metros";
            } else {
                dist = String.format("%.02f", (distance / 1000)) + " Kilometros";
            }
            String distanciaMsg = "Usted se encuentra a una distancia de " + dist;
            listener.trazarRutaSuccess(pointList, cu, distanciaMsg);
        } else {
            listener.trazarRutaFailure("Ruta no disponible para el punto seleccionado..");
        }
    }

    private String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}
