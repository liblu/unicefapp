package com.btree.unicef.saneamiento.interfaces;

/**
 * Created by Diana Mejia on 06/05/2016.
 */
public interface IBusqueda {
    void newQuery(String criterio);
}
