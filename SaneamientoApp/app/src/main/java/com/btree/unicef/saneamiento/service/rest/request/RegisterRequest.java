package com.btree.unicef.saneamiento.service.rest.request;

import java.util.List;

/**
 * Created by Diana Mejia
 */
public class RegisterRequest {
    private EvaluacionRes evaluacion;
    private List<RespuestaRes> respuestas;
    private List<PeticionRes> servicios;


    public List<PeticionRes> getPeticiones() {
        return servicios;
    }

    public void setPeticiones(List<PeticionRes> peticiones) {
        this.servicios = peticiones;
    }

    public EvaluacionRes getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(EvaluacionRes evaluacion) {
        this.evaluacion = evaluacion;
    }

    public List<RespuestaRes> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(List<RespuestaRes> respuestas) {
        this.respuestas = respuestas;
    }
}
