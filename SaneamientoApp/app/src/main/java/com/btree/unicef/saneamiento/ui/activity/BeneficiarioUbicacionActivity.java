package com.btree.unicef.saneamiento.ui.activity;

import android.location.Location;
import android.util.LongSparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.BeneficiarioBl;
import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.model.entities_generated.BeneficiarioRes;
import com.btree.unicef.saneamiento.service.location.LocalizacionBL;
import com.btree.unicef.saneamiento.service.location.TrazarRutaListener;
import com.btree.unicef.saneamiento.utils.XMLReference;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.List;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BeneficiarioUbicacionActivity extends BaseActivity implements
        OnMapReadyCallback,
        View.OnClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, LocalizacionBL.TrackingBLListener, TrazarRutaListener {


    private GoogleMap mMap;

    private LatLng SCZ_VIEW = new LatLng(-17.783848596044916, -63.1809747558593);
    private static final int ZOOM = 11;
    private static final int ZOOM_CLIENT = 15;

    private static HashMap<Marker, BeneficiarioRes> puntoMarker = new HashMap<Marker, BeneficiarioRes>();
    private static LongSparseArray<Marker> markersPuntos = new LongSparseArray<Marker>();
    private static LongSparseArray<BitmapDescriptor> mapMarkerSparserArray = new LongSparseArray<>();

    @XMLReference(idRes = R.id.btn_llegue)
    private Button btnLlegue;
    @XMLReference(idRes = R.id.btn_finalizar)
    private Button btnFinalizar;
//    @XMLReference(idRes = R.id.btn_cancelar)
//    private Button btnCancelar;
    @XMLReference(idRes = R.id.cordinatorLayout)
    private CoordinatorLayout coordinatorLayout;


   // private List<Componente> lstPuntos;

    private BeneficiarioBl beneficiarioBl;

    private Location myLocation;


    ParametersBl parametersBl;

    @Override
    public int getLayoutId() {
        return R.layout.activity_ruta_map;
    }

    @Override
    public void inicializarVariables() {

        beneficiarioBl = new BeneficiarioBl();



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_ruta);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void instanciarIGU() {
        btnLlegue.setOnClickListener(this);
        btnFinalizar.setOnClickListener(this);
     //   btnCancelar.setOnClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }



    @Override
    public String getToolbarTitle() {
        return "Ubicación Beneficiario";
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
      //  LocalizacionBL.getInstance(this).startListening(this);

       // if (lstPuntos != null && !lstPuntos.isEmpty()) {
            actualizarPuntos();
        //}

            situarMapa(SCZ_VIEW, ZOOM);

    }

    private void actualizarPuntos() {
        if (mMap != null) {
            mMap.clear();
            aderirMarcador();
//            for (Componente punto : lstPuntos) {
//                Marker m = aderirMarcador(punto);
//                if (m != null) {
//                    puntoMarker.put(m, punto);
//                    markersPuntos.put(punto.getId(), m);
//                }
//
//            }
        }
    }

    public Marker aderirMarcador() {
        String med="Nro: 1 Beneficiaro: Nombre";
      LatLng pointstatic=  new LatLng( -17.771688, -63.175779);
        MarkerOptions markerOptions = new MarkerOptions()
                .position(pointstatic)
                .title("Beneficiario ")
                .snippet(med);
        return mMap.addMarker(markerOptions);
    }




    private void situarMapa(LatLng place, int zoom) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place).zoom(zoom).bearing(0).tilt(0).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_llegue:
               // hideMenu();


                break;
            case R.id.btn_finalizar:
             //   hideMenu();


                break;
            case R.id.btn_cancelar:
             //   hideMenu();

                break;



        }
    }




    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    public void onInfoWindowClick(Marker marker) {
     //   hideMenu();

         BeneficiarioRes componente = puntoMarker.get(marker);

    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    public void onMapClick(LatLng latLng) {
        actualizarPuntos();
        situarMapa(latLng, 15);
       // hideMenu();
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
    }

    @Override
    public void trazarRutaSuccess(List<LatLng> pointList, CameraUpdate cu, String distanciaMsg) {
        for (int z = 0; z < pointList.size() - 1; z++) {
            LatLng src = pointList.get(z);
            LatLng dest = pointList.get(z + 1);
            mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude))
                    .width(4).color(getResources().getColor(R.color.ruta_trazada))
                    .geodesic(true));
        }
        mMap.moveCamera(cu);
        Snackbar.make(coordinatorLayout, distanciaMsg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void trazarRutaFailure(String mensaje) {
        Toast.makeText(BeneficiarioUbicacionActivity.this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalizacionBL.getInstance(this).stopLocationUpdates();
    }

}
