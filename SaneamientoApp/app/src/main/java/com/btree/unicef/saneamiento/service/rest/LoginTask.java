package com.btree.unicef.saneamiento.service.rest;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.interfaces.IBaseRest;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.model.enums.ResponseStatus;
import com.btree.unicef.saneamiento.service.rest.response.ResponseListener;
import com.btree.unicef.saneamiento.service.restbase.RestBuilder;

import java.net.SocketTimeoutException;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Diana Mejia on 03/08/2017.
 */

public class LoginTask extends AsyncTask<Void, Long, Boolean> {
    private IBaseRest iBaseRest;
    private ResponseListener listener;
    private Context ctx;
    private String user;
    private String pasw;
    private String responsemsg;

    private IDialogConfirmar iDialogoConfirmarFailure;
    public LoginTask(Context ctx,String user, String pasw,ResponseListener listener) {
        this.listener=listener;
        this.ctx=ctx;
        this.user=user;
        this.pasw=pasw;
        if (ctx instanceof IBaseRest) {
            iBaseRest = (IBaseRest) ctx;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        iBaseRest.showProgressDialog(ctx,"Iniciando sesion");
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {

            DatosSesion datosSesion = new DatosSesion();
            datosSesion.setUser(user);
            datosSesion.setPassword(pasw);


            Call<DatosSesion> loginCall = RestBuilder.getDataService().iniciarSesion(datosSesion);
            Response<DatosSesion> loginResponse = loginCall.execute();
            if(loginResponse.isSuccessful()){
                DatosSesion dtResponse= loginResponse.body();
                if(dtResponse.getStatus()==ResponseStatus.SUCCESSFULL.id()){
                    ParametersBl parametersBl = new ParametersBl();
                    DatosSesion dt=parametersBl.getDatosSession();

                    if(dt==null) {
                        dtResponse.setLastLogin(new Date());
                        dtResponse.setUser(user);
                        dtResponse.setPassword(pasw);
                        parametersBl.insertDatosSesion(dtResponse);
                    }else {
                        dt.setLastLogin(new Date());
                        dt.setPassword(pasw);
                        dt.setMonitorId(dtResponse.getMonitorId());
                        dt.setNombre(dtResponse.getNombre());
                        parametersBl.updateDatosSession(dt);
                    }

                }else{
                    responsemsg= "lng: "+dtResponse.getMessage();
                    return false;
                }
            }else{
                responsemsg="lng: "+loginResponse.message();
                return false;
            }

            Log.i("Login","Inicio de session ok-------------------->>>>");





            return true;

        } catch (Exception e) {
            Log.e("Error en descarga", e.getMessage());
            if(e instanceof SocketTimeoutException){
                responsemsg="Tiempo de espera agotado, intente nuevamente";
            }else if(e instanceof java.net.ConnectException){
                responsemsg=e.getMessage();
            }else{
                responsemsg=e.getMessage();
            }
        }
        return false;
    }



    @Override
    protected void onPostExecute(Boolean aLong) {
        super.onPostExecute(aLong);
        iBaseRest.onFinish(ctx);
        if(aLong) {
            listener.onSuccess(responsemsg);
        }else{
            iBaseRest.onFailure(iDialogoConfirmarFailure,responsemsg);
        }
    }
}
