package com.btree.unicef.saneamiento.model.daos;

import android.content.Context;

import org.greenrobot.greendao.AbstractDao;


public abstract class ObjetoBLBase<T extends AbstractDao<?,?>>  {

	protected Context context;

	protected abstract T getDao(DaoSession daoSession);

	public ObjetoBLBase(Context context) {
		this.context = context;
	}

	public  DaoSession getDefaultDaoSession() {
		return Singleton.getctsBDInstancia(context).getDefaultSession();
	}

	public DaoSession getNewDaoSession() {
		return Singleton.getctsBDInstancia(context).recrearDefaultDaoSession();
	}

	public  T getDefaultDao() {
		return getDao(getDefaultDaoSession());
	}

	public T getNewDao() {
		return getDao(getNewDaoSession());
	}
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		context = null;
	}



}