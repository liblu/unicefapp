package com.btree.unicef.saneamiento.ui.activity;

import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.ParametersBl;
import com.btree.unicef.saneamiento.bussiness.RutaBl;
import com.btree.unicef.saneamiento.bussiness.SessionManager;
import com.btree.unicef.saneamiento.model.entities_generated.DatosSesion;
import com.btree.unicef.saneamiento.model.entities_generated.RutaRes;
import com.btree.unicef.saneamiento.model.enums.Fecha;
import com.btree.unicef.saneamiento.service.rest.ApiManager;
import com.btree.unicef.saneamiento.service.rest.response.ResponseListener;
import com.btree.unicef.saneamiento.utils.Utils;
import com.btree.unicef.saneamiento.utils.XMLReference;
import com.google.android.material.navigation.NavigationView;

import java.util.Date;
import java.util.List;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


/**
 * Created by Diana Mejia on 31/03/2016.
 */

public class MenuPrincipalActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    @XMLReference(idRes = R.id.toolbar)
    private Toolbar toolbar;
    @XMLReference(idRes = R.id.lblUser)
    private TextView lblUser;
    private TextView lblMail;
    private ImageView ivUser;

    @XMLReference(idRes = R.id.men_beneficiario)
    private View imgBeneficiario;

    @XMLReference(idRes = R.id.men_rutas)
    private View imgRuta;
    @XMLReference(idRes = R.id.men_plla_model)
    private View imgPllaModel;

    private DatosSesion datosSesion;
    private RutaBl rutaBl;
    private List<RutaRes> rutas;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void inicializarVariables() {
       datosSesion=new ParametersBl().getDatosSession();
       rutaBl=new RutaBl();
    }

    @Override
    public void instanciarIGU() {

        setSupportActionBar(toolbar);

        imgRuta.setOnClickListener(this);
        imgBeneficiario.setOnClickListener(this);
        //imgNuevoPedido.setOnClickListener(this);
        imgPllaModel.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View navHeader = navigationView.getHeaderView(0);
        lblUser = (TextView) navHeader.findViewById(R.id.lblUser);
        lblMail = (TextView) navHeader.findViewById(R.id.lblMail);
        ivUser = (ImageView) navHeader.findViewById(R.id.ivUser);

        DatosSesion datosSesion = new ParametersBl().getDatosSession();

        lblUser.setText(datosSesion.getNombre());
        //lblMail.setText("me@mail.com");
//        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), Utils.openImage(this, datosSesion.getVenImage()));
//        drawable.setCircular(true);
//        ivUser.setImageDrawable(drawable);


    }

    @Override
    public String getToolbarTitle() {
        return "Menu principal";
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.info) {
            // Handle the camera action
        }

        if (id == R.id.nav_cerrar_sesion) {

            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onClick(View v) {
        String today=Utils.getDate(new Date());
        if(datosSesion.getLastUpdate()==null){
            Date date=Utils.getDate(SessionManager.DATE_DEFAULT);
            datosSesion.setLastUpdate(date);
        }
        try {

            if (Utils.comparar_fechas(datosSesion.getLastUpdate(),datosSesion.getLastLogin())== Fecha.MENOR) {
                ApiManager.getInstance(MenuPrincipalActivity.this).dowloadData(new ResponseListener() {
                    @Override
                    public void onSuccess(Object o) {
                        String mensaje = (String) o;
                        showAlertSuccessful(mensaje);
                    }
                });
                return;
            }
        }catch (Exception e){
            showAlertError("Date parse exception");
            return;
        }
        switch (v.getId()) {



            case R.id.men_beneficiario:

                startActivity(new Intent(MenuPrincipalActivity.this, BeneficiariosActivity.class));
                break;

            case R.id.men_rutas:
                rutas= rutaBl.getDefaultDao().loadAll();
               if(rutas.size()>0) {
                   startActivity(new Intent(MenuPrincipalActivity.this, RutaActivity.class));

               }else{

                   ApiManager.getInstance(MenuPrincipalActivity.this).downloadRuta(new ResponseListener() {
                       @Override
                       public void onSuccess(Object o) {
                           rutas= rutaBl.getDefaultDao().loadAll();

                           if(rutas.size()>0) {
                               startActivity(new Intent(MenuPrincipalActivity.this, RutaActivity.class));
                           }else{
                               showAlertWaring("No existen rutas para hoy");
                           }
                       }
                   });
               }
                break;

            case R.id.men_plla_model:
                 startActivity(new Intent(this, PlanillaModelActivity.class));
                break;
        }
    }

}
