package com.btree.unicef.saneamiento.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;

import com.btree.unicef.saneamiento.R;
import com.btree.unicef.saneamiento.bussiness.GrupoBl;
import com.btree.unicef.saneamiento.bussiness.RespuestaBl;
import com.btree.unicef.saneamiento.interfaces.IDialogConfirmar;
import com.btree.unicef.saneamiento.model.entities_generated.GrupoRes;
import com.btree.unicef.saneamiento.model.entities_generated.PreguntaRes;
import com.btree.unicef.saneamiento.ui.adapters.EvaluacionAdapter;
import com.btree.unicef.saneamiento.ui.dialog.CriterioEvaluacionDialog;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class EvaluacionFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Long evaluacionid;
    private String mParam2;
    private ExpandableListView lvPregunta;
    private EvaluacionAdapter evaluacionAdapter;
    private RespuestaBl respuestaBl;
    public EvaluacionFragment() {
        respuestaBl=new RespuestaBl();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param evaluacionid Parameter 1.

     * @return A new instance of fragment EvaluacionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EvaluacionFragment newInstance(Long evaluacionid) {
        EvaluacionFragment fragment = new EvaluacionFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, evaluacionid);
        // args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            evaluacionid = getArguments().getLong(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_evaluacion, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvPregunta=view.findViewById(R.id.lvPlanilla);
        List<GrupoRes>  grupoResList=new GrupoBl().getDefaultDao().loadAll();
        for(GrupoRes item:grupoResList){
            List<PreguntaRes> preguntas=item.getPreguntaResList();
            String cant="";
        }
        evaluacionAdapter= new EvaluacionAdapter(getContext(),grupoResList,evaluacionid);
        evaluacionAdapter.setListener(EvaluacionFragment.this);
        evaluacionAdapter.setCheckedListener(EvaluacionFragment.this);
        lvPregunta.setAdapter(evaluacionAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_text:
                Long preguntaid=(Long)view.getTag();
                CriterioEvaluacionDialog.showDialogCriterio(preguntaid, evaluacionid, new IDialogConfirmar() {
                    @Override
                    public void aceptar(String object) {
                        evaluacionAdapter.notifyDataSetChanged();
                    }
                }).show(getChildFragmentManager(),"");

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.switch_respuesta:
                if(compoundButton.getTag()!=null) {
                    Long respuestaid = (Long) compoundButton.getTag();
                    respuestaBl.updateBoolResp(respuestaid,b);
                }
                break;
        }
    }
}