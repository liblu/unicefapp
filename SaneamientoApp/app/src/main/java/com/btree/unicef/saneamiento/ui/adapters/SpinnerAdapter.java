package com.btree.unicef.saneamiento.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.btree.unicef.saneamiento.R;

import java.util.List;


public class SpinnerAdapter<T> extends ArrayAdapter<T> {

    public SpinnerAdapter(Context context, List<T> objects) {
        super(context, 0, objects);
    }

    class ViewHolder {
        TextView lblFiltro;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, R.layout.item_filtro_spinner);
    }


    public View getView(int position, View convertView, int resourceLayout) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(resourceLayout, null);
            holder.lblFiltro = (TextView) convertView.findViewById(R.id.lbl_filtro_spinner);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        T item = getItem(position);
        holder.lblFiltro.setText(item.toString());

        return convertView;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, R.layout.item_filtro_spinner_dropdown);
    }
}
