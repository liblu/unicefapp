--Actualizacion al 26/112020
--nuevas columnas 

ALTER TABLE evaluacion
ADD COLUMN orden integer ;

ALTER TABLE evaluacion
ADD COLUMN beneficiarioid bigint ;

ALTER TABLE evaluacion
ADD COLUMN monitorid bigint ;

ALTER TABLE evaluacion
ADD FOREIGN KEY (beneficiarioid) REFERENCES beneficiario(beneficiarioid);

ALTER TABLE evaluacion
ADD FOREIGN KEY (monitorid) REFERENCES monitor(monitorid);

-- adicionar datos
CREATE OR REPLACE FUNCTION cursor_for_update() RETURNS VOID AS
$BODY$

DECLARE 
	reg	RECORD;
	cur_update_eval CURSOR FOR SELECT e.evaluacionid, rd.orden, rd.beneficiarioid, r.monitorid 
									FROM evaluacion as e INNER JOIN rutadetalle as rd 
										ON e.rutadetid=rd.rutadetid INNER JOIN ruta as r 
											ON rd.rutaid=r.rutaid ;

BEGIN
	FOR reg IN cur_update_eval LOOP
	
    RAISE NOTICE 'EVALUACION %',reg.evaluacionid;

    update evaluacion 
    	set orden=reg.orden,
    	 beneficiarioid=reg.beneficiarioid,
    	 monitorid=reg.monitorid
    WHERE evaluacionid=reg.evaluacionid;

 	END LOOP;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

-- Ejecutar funcion 

SELECT cursor_for_update();